# Solidity-Safety to Model Checking

## What is Solidity-Safety (Ethereum Smart Contract Safety)

## Converstion to Model Checking

## Current Limitations

- Local Variables in a function cannot share the same name as other functions within the same contract, unless they share the same date type

---

# How To Use This Tool

## Running the Regression Tests

## Converting Your Solidity Contracts

---

# Developer Notes

## Technology Stack

```mermaid
graph LR;
  A[Parse Solidity File];
  B[Create Finite State Machine];
  C[Write to SMV File];
  D[NuSMV];
  A-->B;
  B-->C;
  C-->D;
```

### Parse Solidity File

Uses ANTLR to parse solidity file.
Started from a solidity parser file and has been heavily modified.
Added the Query special comment.
Removed features (commented out) that won't be supported until far in the future.

1. Documnetation https://github.com/antlr/antlr4/blob/master/doc/index.md
2. Original G4 file: https://raw.githubusercontent.com/antlr/grammars-v4/master/solidity/Solidity.g4

### Finite State Machine (FSM)

We are converting a subset of the procedural programming language Solidity into
a finite state machine. This allows us to have an accurate representation of the
program and makes converting to SMV easy, as it has native support for FSM.

### Converting to SMV

Converting to SMV language (specifically the NuSMV variant) utilizes
Modules for each contract. This allows for variable scope to be pushed to NuSMV
and removed from us. We are keeping track of varaible scope, but we are not
representing expressions within Java, thus are unable to easily update
variable names within an expression (unless variable names are unique and
don't interfere with anything within the expressions).

1. NuSMV Docs: http://nusmv.fbk.eu/NuSMV/userman/v26/nusmv.pdf
2. nuXmv Docs: https://es-static.fbk.eu/tools/nuxmv/index.php?n=Documentation.Home

### Running NuSMV

It is simple to run NuSMV, just start the program and have the first
commandline option be the path to the SMV file.

**Note for MAC OS users**: Each NuSMV file must be manually opened. MAC OS will not allow the execution of the NuSMV files unless they are opened first.

1. NuSMV: http://nusmv.fbk.eu/
2. NuSMV Docs: http://nusmv.fbk.eu/NuSMV/userman/v26/nusmv.pdf
3. nuXmv: https://es-static.fbk.eu/tools/nuxmv/index.php
4. nuXmv Docs: https://es-static.fbk.eu/tools/nuxmv/index.php?n=Documentation.Home

## Simplified UML

```mermaid
classDiagram
    DApp *-- SmartContract
    DApp *-- QueryState
    SmartContract *-- QueryI
    SmartContract *-- QueryState
    SmartContract *-- Function
    SmartContract *-- Variable
    Function *-- Variable
    QueryI <|-- ValueReachabilityQuery
    class DApp{
        +ArrayList<SmartContract> contracts
        +ArrayList<QueryState> queries
        +StateI initState
        +StateI loopState
        +StateI errorState
    }
    class SmartContract{
        +HashMap<String, Variable> state_variables
        +HashMap<String, Function> functions
        +HashMap<String, ModifierFunction> modifiers
        +ConstructorFunction constructor
        +ArrayList<QueryState> stateQueries
        +ArrayList<QueryI> contractQueries
    }
    class QueryState{
        +Variable queryVar
        +String goalStateCondition
    }
    class QueryI {
        <<interface>>
        +String toSMV()
    }
    class ValueReachabilityQuery {
        +String goalCondition
    }
    class Function {
        +String identifier
        +HashMap<String, Variable> parameters
        +HashMap<String, ModifierFunction> require_modifiers
        +HashMap<String, Variable> return_parameters
        +ArrayList<StateI> states
        +StateI start_state
        +StateI last_state
        +HashMap<String, Variable> internal_variables
        +void startDefinition()
        +void endDefinition()
        +void addState(StateTransition state)
    }

    class Variable {
        +String typeName
        +String identifier
        +VariableScope scope
        +SmartContract parentContract
        +Function parentFunction
        +String scopeName
        +DataI value
    }
```
