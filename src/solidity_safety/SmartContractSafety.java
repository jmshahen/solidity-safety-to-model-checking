package solidity_safety;

import java.util.logging.Logger;

public class SmartContractSafety {
    public static final Logger logger = Logger.getLogger("shahen");

    public static void main(String[] args) {
        SmartContractSafetyInstance inst = new SmartContractSafetyInstance();

        inst.run(args);
    }

}
