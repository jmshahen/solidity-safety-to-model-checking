/**
 * Holds classes that deal directly with calling NuSMV style applications and with handling their output.
 * @see solidity_safety.nusmv.CounterExample
 * @see solidity_safety.nusmv.CounterExampleStep 
 */
package solidity_safety.nusmv;
