package solidity_safety.nusmv;

import java.util.ArrayList;

public class CounterExampleStep {
    public static boolean useShortString = true;
    public static int shortStringVersion = 2;

    public ArrayList<String> input;
    public String state;
    public ArrayList<String> changes;

    public CounterExampleStep(ArrayList<String> input, String state, ArrayList<String> changes) {
        this.input = input;
        this.state = state;
        this.changes = changes;
    }

    public String getLongString() {
        if (input.isEmpty() && changes.isEmpty()) return "{State: " + state + "}";
        else if (input.isEmpty() && !changes.isEmpty()) return "{State: " + state + ", changes:" + changes + "}";
        else if (!input.isEmpty() && changes.isEmpty()) return "{Input: " + input + ", State: " + state + "}";
        return "{Input: " + input + ", State: " + state + ", changes:" + changes + "}";
    }

    public String getShortString() {
        if (input.isEmpty() && changes.isEmpty()) return "{" + state + "}";
        else if (input.isEmpty() && !changes.isEmpty()) return "{" + state + " -> " + changes + "}";
        else if (!input.isEmpty() && changes.isEmpty()) return "{" + input + " -> " + state + "}";
        return "{" + input + " -> " + state + " -> " + changes + "}";
    }

    @Override
    public String toString() {
        if (useShortString) { return getShortString(); }
        return getLongString();
    }
}
