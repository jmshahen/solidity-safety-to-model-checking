package solidity_safety.nusmv;

import java.util.ArrayList;

import solidity_safety.constants.SMVConstants;

public class CounterExample {
    public String specification;
    public boolean goalReached;
    public String queryName = "No_Name";

    public ArrayList<CounterExampleStep> steps = new ArrayList<>();
    public ArrayList<String> details = new ArrayList<>();
    public StringBuilder ruleProgression = new StringBuilder();

    public CounterExample(String specification) {
        this.specification = specification;
        if (specification.endsWith(" is false")) {
            goalReached = true;
        } else {
            goalReached = false;
        }

        String[] matches = specification.split(SMVConstants.QueryName + " = ");

        if (matches.length == 2) {
            queryName = matches[1].split(" & ")[0];
        }
    }

    public String queryName() {
        return queryName;
    }

    public CounterExample(boolean goalReached) {
        this.goalReached = goalReached;
    }

    public void addStep(CounterExampleStep step) {
        steps.add(step);
    }

    public int numberOfSteps() {
        return steps.size();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Initial State -> ");

        for (CounterExampleStep ces : steps) {
            sb.append(ces.toString()).append(" -> ");
        }

        if (goalReached) {
            sb.append("Goal State");
        } else {
            sb.append("Goal State is Unreachable");
        }
        return sb.toString();
    }

    /**
     * If the last step was added in error, then you can remove it with this line
     */
    public void removeLastStep() {
        steps.remove(steps.size() - 1);
    }
    public void finish() {
        if (ruleProgression.length() > 0) {
            ruleProgression.append("GOAL_STATE");
            goalReached = true;
        } else {
            goalReached = false;
            ruleProgression.append("GOAL_STATE_UNREACHABLE");
        }
    }

    public String getReachabilityString() {
        if (goalReached) return "REACHABLE";
        return "UNREACHABLE";
    }
}
