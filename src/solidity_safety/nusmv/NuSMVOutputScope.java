package solidity_safety.nusmv;

public enum NuSMVOutputScope {
    IN_STATE, IN_INPUT, IN_SPECIFICATION, UNKOWN
}
