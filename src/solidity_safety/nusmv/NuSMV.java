/**
 * The MIT License
 *
 * Copyright (c) 2010 Karthick Jayaraman
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

package solidity_safety.nusmv;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;

import solidity_safety.constants.SMVConstants;

/** @author Jonathan Shahen; kjayaram */
public class NuSMV {
    public final static Logger logger = Logger.getLogger("shahen");

    public Process execProcess = null;
    /** Exit value returned from the last execution of {@link NuSMV#RunNuSMV(List)}; NULL if not run yet. */
    public Integer exitValue = null;
    public String NuSMVProcessName = "NuSMV";
    /** Holds the path to the file where the program's console output is stored */
    public String programOutput = "logs/latestNuSMVOutput.txt";
    /** Internal reference to the current bound */
    public Integer bound = null;

    /** When TRUE it will output the NuSMV output as it comes in */
    public boolean writeNuSMVOutput = false;

    public StringBuilder _lastOutput = new StringBuilder();
    public ArrayList<CounterExample> counterExamples = new ArrayList<CounterExample>();

    /** Empty constructor, uses the default values */
    public NuSMV() {}

    public NuSMV(String nusmvProcessName) {
        NuSMVProcessName = nusmvProcessName;
    }

    /** Function to call the symbolic model checker.
     *
     * @param filename path to the SMV file
     * @throws IOException
     * @throws InterruptedException */
    public void SMCFile(String filename) throws IOException, InterruptedException {
        this.bound = null;
        List<String> commands = Arrays.asList(NuSMVProcessName, filename);
        RunNuSMV(commands);
    }

    /** Call the NuSMV symbolic model checker. The bound and SMV model filename are passed as inputs
     *
     * @param bound
     * @param filename
     * @throws IOException
     * @throws InterruptedException */
    public void BMCFile(Integer bound, String filename) throws IOException, InterruptedException {
        this.bound = bound;
        List<String> commands = Arrays.asList(NuSMVProcessName, "-bmc", "-bmc_length", bound.toString(), filename);
        RunNuSMV(commands);
    }

    /** Run the NuSMV engine using the specified command and input.<br>
     * Returns TRUE is the QUERY is reachable, FALSE if it is unreachable
     *
     * @param commands List&lt;String&gt; = [program,command param 1, command param 2, ...]
     * @return TRUE is the QUERY is reachable, FALSE if it is unreachable
     * @throws IOException
     * @throws InterruptedException
     */
    private void RunNuSMV(List<String> commands) throws IOException, InterruptedException {
        logger.entering("NuSMV", "RunNuSMV(commands)");
        logger.info("[NuSMV] Running NuSMV with the commands: " + commands);

        // Run the NuSMV engine
        ProcessBuilder builder = new ProcessBuilder(commands);
        builder.redirectErrorStream(true); // combine the Error into the normal stream
        execProcess = builder.start();

        // Reading Output from process
        BufferedReader bufread = new BufferedReader(new InputStreamReader(execProcess.getInputStream()));
        String strLine = null;
        _lastOutput.setLength(0);// clear any old data

        CounterExample counterExample = null;
        ArrayList<String> input = new ArrayList<>();
        String state = "";
        ArrayList<String> changes = new ArrayList<>();
        NuSMVOutputScope currentScope = NuSMVOutputScope.UNKOWN;

        appendNewLineAndWrite(_lastOutput, "################### START: NuSMV Output ###################");
        while ((strLine = bufread.readLine()) != null) {
            // Add line to log and STDOUT
            appendNewLineAndWrite(_lastOutput, strLine);

            // Skip Empty Lines
            if (strLine.isEmpty()) continue;
            // Skip Comments
            if (strLine.startsWith("***")) continue;

            // Find new specification
            if (strLine.startsWith("-- specification ")) {
                if (counterExample != null) counterExample.finish();

                currentScope = NuSMVOutputScope.IN_SPECIFICATION;
                counterExample = new CounterExample(strLine);
                counterExamples.add(counterExample);

                input = new ArrayList<>();
                state = "";
                changes = new ArrayList<>();

                continue;
            }

            if (counterExample != null) {
                if (strLine.contains("-> Input: ")) {
                    counterExample.addStep(new CounterExampleStep(input, state, changes));

                    currentScope = NuSMVOutputScope.IN_INPUT;
                    input = new ArrayList<>();
                    continue;
                }
                if (strLine.contains("  -> State: ")) {
                    currentScope = NuSMVOutputScope.IN_STATE;
                    state = strLine.trim().replace("-> State: ", "").replace(" <-", "");
                    changes = new ArrayList<>();
                    continue;
                }

                switch (currentScope) {
                    case IN_INPUT :
                        input.add(strLine.trim());
                        break;
                    case IN_STATE : {
                        changes.add(strLine.trim());

                        int index = strLine.indexOf(SMVConstants.State + " = ");
                        if (index != -1 && (strLine.endsWith("_ln0") || !strLine.contains("_ln"))) {
                            counterExample.ruleProgression
                                    .append(strLine.replace(SMVConstants.State + " = ", "").trim()).append(" -> ");
                        }
                        break;
                    }
                    case IN_SPECIFICATION :
                        if (strLine.startsWith("--")) continue;
                        counterExample.details.add(strLine.trim());
                        break;
                    default :
                        logger.warning("[NuSMV] Unknown Line in NuSMV Output: " + strLine);
                }
            }
        }
        if (counterExamples.size() > 0) {
            counterExamples.get(counterExamples.size() - 1).addStep(new CounterExampleStep(input, state, changes));
            counterExamples.get(counterExamples.size() - 1).finish();
        }
        appendNewLineAndWrite(_lastOutput, "################### END: NuSMV Output ###################");

        // MUST BE HERE, DO NOT MOVE ABOVE READ LOOP
        execProcess.waitFor();
        exitValue = execProcess.exitValue();
        appendNewLineAndWrite(_lastOutput, "################### NuSMV Exit Value: " + exitValue);
        if (exitValue != 0) {
            logger.severe("An error was detected in the NuSMV output:\n" + _lastOutput.toString());
        }

        try {
            FileWriter fw = new FileWriter(programOutput, false);
            fw.write("Generated on: " + Calendar.getInstance().getTime() + "\n");
            for (CounterExample ce : counterExamples) {
                fw.write("Rule Progression:\n");
                fw.write(ce.ruleProgression.toString());
                fw.write("\n\nCounter Example (" + ce.numberOfSteps() + " steps):\n");
                fw.write(ce.toString());
                fw.write("\n\n");
            }
            fw.write(_lastOutput.toString());
            fw.close();
        } catch (Exception e) {
            // do not send this error up
            System.out.println("[ERROR] Failed to write out the NuSMV log");
        }

        logger.exiting("NuSMV", "RunNuSMV(commands)");
    }

    private void appendNewLineAndWrite(StringBuilder sb, String line) {
        sb.append(line + "\n");
        if (writeNuSMVOutput) System.out.println(line);
    }
}
