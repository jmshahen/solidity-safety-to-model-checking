/**
 * Solidity Safety: Given a solidity file, with safety queries encoded as comments,
 * answer if each of the safety queries are REACHABLE (providing a counter example) or if they are UNREACHABLE.  
 */
package solidity_safety;
