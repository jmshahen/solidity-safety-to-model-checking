package solidity_safety;

import java.io.*;
import java.time.Duration;
import java.util.*;
import java.util.logging.Logger;

import org.apache.commons.cli.Options;

public class SmartContractSafetyCUI {
    public static final Logger logger = Logger.getLogger("shahen");
    public static String previousCommandFilename = "SmartContractSafetyCUIPreviousCommand.txt";
    public static String previousCmd;

    public static void main(String[] args) {
        SmartContractSafetyInstance inst = new SmartContractSafetyInstance();
        ArrayList<String> argv = new ArrayList<String>();
        ArrayList<String[]> cmds = new ArrayList<String[]>();
        String arg = "";
        Scanner user_input = new Scanner(System.in);

        Options options = new Options();
        inst.setupOptions(options);
        inst.printHelp(options, 120);

        printCommonCommands();

        System.out.println("HELP: To solve all test cases: !run_all");
        System.out.println("HELP: Use '!e' to separate commands, and '!x' to run the previous commands");
        System.out.print("Enter Commandline Argument: ");
        String quotedStr = "";
        StringBuilder fullCommand = new StringBuilder();
        while (true) {
            arg = user_input.next();

            if (quotedStr.isEmpty() && arg.startsWith("\"")) {
                System.out.println("Starting: " + arg);
                quotedStr = arg.substring(1);
                continue;
            }
            if (!quotedStr.isEmpty() && arg.endsWith("\"")) {
                System.out.println("Ending: " + arg);
                argv.add(quotedStr + " " + arg.substring(0, arg.length() - 1));
                quotedStr = "";
                continue;
            }

            if (!quotedStr.isEmpty()) {
                quotedStr = quotedStr + " " + arg;
                continue;
            }

            // !e indicates that a command is finished and that a new command is wanted
            if (arg.equals("!e") || arg.equals("!E")) {
                if (argv.size() > 0) {
                    cmds.add(argv.toArray(new String[1]));
                    argv.clear();
                }
                continue;
            }

            if (arg.equals("!p!x") || arg.equals("!P!X")) {
                Collections.addAll(argv, previousCmd.split(" "));
                fullCommand.append(previousCmd + " ");
                if (argv.size() > 0) {
                    cmds.add(argv.toArray(new String[1]));
                    argv.clear();
                }
                break;
            }

            // !x indicates that no other input should be read, and that the commands should be executed
            if (arg.equals("!x") || arg.equals("!X")) {
                if (argv.size() > 0) {
                    cmds.add(argv.toArray(new String[1]));
                    argv.clear();
                }
                break;
            }

            if (arg.equals("!p") || arg.equals("!P")) {
                Collections.addAll(argv, previousCmd.split(" "));
                fullCommand.append(previousCmd + " ");
                continue;
            }

            // if (arg.equals("!run_all")) {
            // argv.clear();
            // cmds = allStats();
            // break;
            // }

            fullCommand.append(arg + " ");
            argv.add(arg);
        }
        user_input.close();

        System.out.println("Commands: " + argv);

        if (!arg.equals("!run_all")) {
            try {
                FileWriter fw;
                fw = new FileWriter(previousCommandFilename, false);
                fw.write(fullCommand.toString().replaceAll("!p", previousCmd));
                fw.close();
            } catch (IOException e) {
                System.out.println("[ERROR] Unable to write out previous command to: " + previousCommandFilename);
            }
        }

        long fullstart = System.currentTimeMillis();
        for (String[] c : cmds) {
            System.out.println("==============================================================");
            System.out.println("== CMD: " + Arrays.toString(c));
            System.out.println("==================START OF OUTPUT=============================");
            long start = System.currentTimeMillis();
            inst = new SmartContractSafetyInstance();
            inst.run(c);
            Duration d = Duration.ofMillis(System.currentTimeMillis() - start);
            System.out.println("====================END OF OUTPUT============================");
            System.out.println("Done [" + humanReadableFormat(d) + "]");
            System.out.println("");
        }

        if (cmds.size() > 1) {
            Duration d = Duration.ofMillis(System.currentTimeMillis() - fullstart);
            System.out.println("\n");
            System.out.println("===========================");
            System.out.println("===========================");
            System.out.println("Done all Tests [" + humanReadableFormat(d) + "]");
        }
    }

    public static String humanReadableFormat(Duration duration) {
        return duration.toString().substring(2).replaceAll("(\\d[HMS])(?!$)", "$1 ").toLowerCase();
    }

    public static void printCommonCommands() {
        String sup = "data/regression/supported_features";
        String allcmd = "-loglevel warning -run regression -bulk ";
        System.out.println("\n\n--- Run Regression Tests ---");
        System.out.println(allcmd + "-input data/regression/reachable/ !x");
        System.out.println(allcmd + "-input data/regression/unreachable/ !x");
        System.out.println(allcmd + "-input data/regression/supported_features/ !x");
        System.out.println(allcmd + "-input data/regression/fixed_bugs/ !x");
        System.out.println("-----------------------------------------------");

        System.out.println("\n-----------------------------------------------");
        String a = "-debug -loglevel finest -run solve ";
        System.out.println(a + "-input " + sup + "/EnumDataType.sol !x");
        System.out.println(a + "-input " + sup + "/IfStatements1.sol !x");
        System.out.println(a + "-input " + sup + "/IfStatements2.sol !x");
        System.out.println(a + "-input " + sup + "/MultipleContracts1.sol !x");
        System.out.println(a + "-input " + sup + "/RequireStatement1.sol !x");
        System.out.println(a + "-input " + sup + "/RequireStatement2.sol !x");
        System.out.println(a + "-input " + sup + "/RequireStatement3.sol !x");
        System.out.println(a + "-input " + sup + "/SimpleContract1.sol !x");
        System.out.println(a + "-input " + sup + "/SimpleStateVariables.sol !x");
        System.out.println(a + "-input " + sup + "/SimpleFunction1.sol !x");
        System.out.println(a + "-input " + sup + "/SimpleFunction2.sol !x");
        System.out.println(a + "-input " + sup + "/SimpleStateMachine1.sol !x");
        System.out.println(a + "-input " + sup + "/SimpleStateMachine2.sol !x");
        System.out.println(a + "-input " + sup + "/ModifierFunction1.sol !x");
        System.out.println(a + "-input " + sup + "/ModifierFunction2.sol !x");
        System.out.println(a + "-input " + sup + "/ModifierFunction3.sol !x");
        System.out.println(a + "-input " + sup + "/ModifierFunction4.sol !x");
        System.out.println(a + "-input " + sup + "/ModifierFunction5.sol !x");
        System.out.println(a + "-input " + sup + "/ModifierFunction6.sol !x");
        System.out.println(a + "-input " + sup + "/ModifierFunction7.sol !x");
        System.out.println(a + "-input " + sup + "/ModifierFunction8.sol !x");
        System.out.println(a + "-input " + sup + "/OutOfGas.sol !x");
        System.out.println("-----------------------------------------------");

        System.out.println("\n-----------------------------------------------");
        System.out.println("-noRollback -run dotHTML -input " + sup + "/RequireStatement1.sol !x");
        System.out.println("-noRollback -run dotHTML -input " + sup + "/RequireStatement2.sol !x");
        System.out.println("-noRollback -run dotHTML -input " + sup + "/RequireStatement3.sol !x");
        System.out.println("-----------------------------------------------");

        System.out.println("\n-----------------------------------------------");
        a = "-debug -loglevel finest -run solve ";
        System.out.println(a + "-input data/regression/reachable/reachable01.sol !x");
        System.out.println(a + "-input data/regression/reachable/reachable02.sol !x");
        System.out.println(a + "-input data/thesis/purchase-converted.sol !x");
        System.out.println("-----------------------------------------------");

        System.out.println("\n-----------------------------------------------");
        System.out.println("-debug -loglevel finest -run solve -input data/last_bug.sol !x");
        System.out.println("-----------------------------------------------");
        System.out.println("\n-----------------------------------------------");
        try {
            BufferedReader bfr = new BufferedReader(new FileReader(previousCommandFilename));
            previousCmd = bfr.readLine();
            bfr.close();
            System.out.println("Previous Command ('!p' expands to this): " + previousCmd);
        } catch (IOException e) {
            System.out.println("[ERROR] Unable to load previous command!");
        }
    }
}
