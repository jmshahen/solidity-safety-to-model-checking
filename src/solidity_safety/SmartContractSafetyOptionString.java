package solidity_safety;

/**
 * Contains the supported Command Line Options.
 * @author Jonathan Shahen
 *
 */
public enum SmartContractSafetyOptionString {
    /** Displays the help message in the command prompt */
    HELP("help"),
    /** Displays a list of authors who have contributed to this project */
    AUTHORS("authors"),
    /** Displays the current version of this program */
    VERSION("version"),
    /** Include more debugging information in the output files.
     * Example: State names are used in full, instead of a short abbreviation. */
    DEBUG("debug"),
    /** Extra state variables removed.
     * Logic for backing-up and rolling-back is removed
     * Failed require state lead to an error state, where there exists no exit branches for the statemachine (State Called __Failed_Required)*/
    NOROLLBACK("noRollback"),
    /** Sets the logging level for the console and the log file */
    LOGLEVEL("loglevel"),
    /** Indicates which file to store the log file in */
    LOGFILE("logfile"),
    /**  */
    NOHEADER("noheader"),
    /** Sets the maximum width of the console log. Use 0 for no word-wrapping. */
    MAXW("maxw"),
    /** Allow to overwrite the newline string */
    LINESTR("linstr"),
    /** The input file/folder to try and solve */
    SPECFILE("input"),
    /** When using the bulk option, this allows to change the file extension to search for. */
    SPECEXT("ext"),
    /** Treats the {@link SmartContractSafetyOptionString#SPECFILE} as a
     * folder and uses {@link SmartContractSafetyOptionString#SPECEXT} for the file extension to search for */
    BULK("bulk"),
    /** This automatically will pick the correct version if running on WINDOWS or LINUX.
     * <ul>
     * <li>"nusmv" - NuSMV v2.6.0 64bit
     * <li>"zchaff" - NuSMV v2.6.0 64bit with ZCHAFF (no commercial use)
     * <li>"nuxmv" - nuXmv v2.0.0 64bit
     * </ul>
     */
    PROGRAM("program"),
    /** Sets the actions that will be performed on the input policies. The following options are available:
     * <ul>
     * <li>"list" - List the contracts that are contained within the solidity file
     * <li>"dot" - Outputs a DOT graph to STDOUT
     * <li>"dotHTML" - Outputs a DOT graph to an HTML file
     * <li>"smv" - Outputs a SMV model to STDOUT
     * <li>"smvFile" - Outputs a SMV model to a file
     * <li>"solve" - Converts the solidity file to an SMV model and then runs it using NuSMV or nuXmv
     * </ul>
     */
    RUN("run");

    private String _str;

    private SmartContractSafetyOptionString(String s) {
        _str = s;
    }

    @Override
    public String toString() {
        return _str;
    }

    /** Shortcut of calling toString(). Use this when querying the CommandLine object. AUTHORS =&gt; "authors" */
    public String s() {
        return _str;
    }

    /** Returns the commandline equivalent with the hyphen and a space following: AUTHORS -&gt; "-authors " */
    public String c() {
        return "-" + _str + " ";
    }

    /** Returns the commandline equivalent with the hyphen and a space following:
     * LOGLEVEL("debug") -&gt; "-loglevel debug"*/
    public String c(String param) {
        return "-" + _str + " " + param + " ";
    }
}