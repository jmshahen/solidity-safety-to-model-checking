package solidity_safety.helper;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

import org.antlr.v4.runtime.*;

import solidity_safety.ethereum.SmartContract;
import solidity_safety.parser.SolidityLexer;
import solidity_safety.parser.SolidityParser;

public class ParserHelper {
    public final static Logger logger = Logger.getLogger("mohawk");

    public BooleanErrorListener error = new BooleanErrorListener();

    public SolidityParser parseSolidityString(String str, boolean noRollback) throws IOException {
        InputStream stream = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));

        error.errorFound = false; // reset the error listener
        SolidityParser parser = ParserHelper.runParser(stream, error, false, noRollback);

        if (error.errorFound) {
            logger.warning("Unable to parse the string: \n" + str);
        }

        return parser;
    }

    public SolidityParser parseSolidityFile(File file, boolean noRollback) throws IOException {
        FileInputStream fis = new FileInputStream(file);

        error.errorFound = false; // reset the error listener
        SolidityParser parser = ParserHelper.runParser(fis, error, false, noRollback);

        if (error.errorFound) {
            logger.warning("Unable to parse the file: " + file.getAbsolutePath());
        }

        return parser;
    }

    public static SolidityParser runParser(InputStream is, BaseErrorListener errorListener, Boolean displayStats,
            boolean noRollback) throws IOException {
        ANTLRInputStream input = new ANTLRInputStream(is);
        SolidityLexer lexer = new SolidityLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        SolidityParser parser = new SolidityParser(tokens);

        parser.dapp.noRollback = noRollback;

        parser.removeErrorListeners();
        parser.addErrorListener(errorListener);
        parser.sourceUnit();

        if (displayStats) {
            for (SmartContract contract : parser.dapp.contracts) {
                System.out.println(contract.toString());
            }
        }

        return parser;
    }
}
