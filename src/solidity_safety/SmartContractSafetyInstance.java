package solidity_safety;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.*;

import org.apache.commons.cli.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;

import shahen.file.FileHelper;
import shahen.formatter.ShahenCSVFileFormatter;
import shahen.formatter.ShahenConsoleFormatter;
import shahen.timing.ShahenTiming;
import solidity_safety.ethereum.DApp;
import solidity_safety.ethereum.SmartContract;
import solidity_safety.ethereum.query.QueryI;
import solidity_safety.helper.ParserHelper;
import solidity_safety.nusmv.CounterExample;
import solidity_safety.nusmv.NuSMV;
import solidity_safety.parser.SolidityParser;

public class SmartContractSafetyInstance {
    private final String VERSION = "v1.0.0";
    private static final String AUTHORS = "Jonathan Shahen <jmshahen [AT] uwaterloo [DOT] ca>";

    // Logger Fields
    public static final Logger logger = Logger.getLogger("shahen");
    public Level defaultLogLevel = Level.INFO;
    private String Logger_filepath = "logs/SmartContractSafety-Log.csv";
    private ConsoleHandler consoleHandler = new ConsoleHandler();
    private FileHandler fileHandler;
    private Boolean WriteCSVFileHeader = true;
    private String timingFile = "logs/SmartContractSafety-Timing.csv";

    // Helpers
    public ShahenTiming timing;
    public ParserHelper parserHelper = new ParserHelper();
    public FileHelper fileHelper = new FileHelper();

    // Programs
    public File nusmv_win = new File("programs/NuSMV-2.6.0-win64/bin/NuSMV.exe");
    public File nusmv_linux = new File("programs/NuSMV-2.6.0-Linux/bin/NuSMV");
    public File nusmv_mac = new File("programs/NuSMV-2.6.0-Darwin/bin/NuSMV");
    public File nusmv_zchaff_win = new File("programs/NuSMV-ZCHAFF-2.6.0-win64/bin/NuSMV.exe");
    public File nusmv_zchaff_linux = new File("programs/NuSMV-ZCHAFF-2.6.0-Linux/bin/NuSMV");
    public File nusmv_zchaff_mac = new File("programs/NuSMV-ZCHAFF-2.6.0-Darwin/bin/NuSMV");
    public File nuxmv_win = new File("programs/nuXmv-2.0.0-win64/bin/nuXmv.exe");
    public File nuxmv_linux = new File("programs/nuXmv-2.0.0-Linux/bin/nuXmv");
    public File nuxmv_mac = new File("programs/nuXmv-2.0.0-Darwin/bin/nuXmv");

    // Settings
    /**
     *
     */
    public boolean debug = false;
    /**
     *
     */
    public boolean noRollback = false;
    /**
     * Location where the SMV file will be written to when using the RUN option equal to "solve".
     */
    public File tempSMVFile = new File("logs/lastestSMVFile.smv");
    public File selectedSolver;

    public int run(String[] args) {
        try {
            ////////////////////////////////////////////////////////////////////////////////
            // LOCAL VARIABLES
            timing = new ShahenTiming();
            fileHelper.fileExt = ".sol";
            // LOCAL VARIABLES
            ////////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////////
            // COMMANDLINE OPTIONS
            CommandLine cmd = init(args);
            if (cmd == null) { return 0; }

            // Only set tests.debug when equal to TRUE (this allows for default to be changed to TRUE)
            if (debug) {
                timing.printOnStop = true;
            }
            // COMMANDLINE OPTIONS
            ////////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////////
            // SETUP
            if (SystemUtils.IS_OS_WINDOWS) {
                logger.fine("[ENVIRONMENT] Operating System: Windows");
            } else if (SystemUtils.IS_OS_LINUX) {
                logger.fine("[ENVIRONMENT] Operating System: Linux");
            } else {
                logger.severe("[ENVIRONMENT] Operating System Not Supported: " + SystemUtils.OS_NAME);
            }

            if (!tempSMVFile.exists()) {
                tempSMVFile.createNewFile();
            }
            ////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////
            // LOADING ALL FILES
            /* Timing */ timing.startTimer("loadFile");
            fileHelper.loadFiles(cmd.getOptionValue(SmartContractSafetyOptionString.SPECFILE.s()));
            /* Timing */ timing.stopTimer("loadFile");
            // LOADING ALL FILES
            ////////////////////////////////////////////////////////////////////////////////

            // Execute the test cases
            if (cmd.hasOption(SmartContractSafetyOptionString.RUN.toString())) {
                String runVal = cmd.getOptionValue(SmartContractSafetyOptionString.RUN.toString());
                Integer numFiles = fileHelper.files.size();

                ////////////////////////////////////////////////////////////////////////////////
                // INDIVIDUAL FILE LOOP
                logger.config(fileHelper.size() + " Files to Convert: " + fileHelper.printFileNames(300, true));
                for (Integer i = 1; i <= fileHelper.files.size(); i++) {
                    ////////////////////////////////////////////////////////////////////////////////
                    /* TIMING */ String timerName = "runLoop (" + i + "/" + numFiles + ")";
                    /* TIMING */ timing.startTimer(timerName);
                    File specFile = fileHelper.files.get(i - 1);
                    ////////////////////////////////////////////////////////////////////////////////

                    ////////////////////////////////////////////////////////////////////////////////
                    // PARSING MOHAWK+T FILE
                    /* Timing */ timing.startTimer(timerName, "-parseFile (" + i + ")");
                    logger.info("Processing File (" + i + "/" + numFiles + "): " + specFile.getAbsolutePath());
                    SolidityParser parser = parserHelper.parseSolidityFile(specFile, noRollback);
                    DApp dapp = parser.dapp;
                    dapp.setFilename(specFile);
                    /* Timing */ timing.stopTimer(timerName, "-parseFile (" + i + ")");

                    if (parserHelper.error.errorFound) {
                        logger.warning("[PARSING] ERROR: Skipping this file due to a parsing error");
                        continue;
                    } else {
                        logger.config("[PARSING] No errors found while parsing file, continuing on to converting");
                    }
                    // END OF PARSING MOHAWK+T FILE
                    ////////////////////////////////////////////////////////////////////////////////

                    ////////////////////////////////////////////////////////////////////////////////
                    // RUN A CERTAIN TASK ON THE MOHAWK+T FILE
                    switch (runVal) {
                        case "solve" : {
                            logger.config("[RUN ACTION] Converts the contracts into a NuSMV file format and "
                                    + "writes it to the SMV file: " + tempSMVFile.getAbsolutePath()
                                    + ". And then run NuSMV on it.");

                            StringBuilder sb = dapp.toSMVString(debug);

                            try (BufferedWriter writer = Files
                                    .newBufferedWriter(Paths.get(tempSMVFile.getAbsolutePath()))) {
                                writer.append(sb);
                            }

                            NuSMV nusmv = new NuSMV(selectedSolver.getAbsolutePath());
                            nusmv.writeNuSMVOutput = debug;
                            nusmv.SMCFile(tempSMVFile.getAbsolutePath());

                            for (CounterExample ce : nusmv.counterExamples) {
                                QueryI q = dapp.queryExpectedResult.getOrDefault(ce.queryName, null);
                                String expectedResult = (q == null) ? "No Query Found" : q.getExpectedResult();
                                if (expectedResult == null) {
                                    expectedResult = "None";
                                }

                                String s = "[RUN] Result for Query " + q.getOriginalQueryString(ce.queryName())
                                        + " -- Result/Expected: " + ce.getReachabilityString() + "/" + expectedResult
                                        + ") [" + ce.specification + "]: ";
                                if ((expectedResult.equals("REACHABLE") && !ce.goalReached)
                                        || (expectedResult.equals("UNREACHABLE") && ce.goalReached)) {
                                    logger.warning(s);
                                } else {
                                    logger.info(s);
                                }
                                if (ce.goalReached) {
                                    logger.info("[RUN] Rule Progression for Query "
                                            + q.getOriginalQueryString(ce.queryName()) + ":\n"
                                            + ce.ruleProgression.toString());
                                    if (logger.isLoggable(Level.FINE)) {
                                        logger.fine("[RUN] Counter Example (" + ce.numberOfSteps() + " steps):\n" + ce);
                                    }
                                }
                            }

                        }
                            break;
                        case "reg" :
                        case "regression" : {
                            logger.config("[REGRESSION ACTION] Converts the contracts into a NuSMV file format and "
                                    + "writes it to the SMV file: " + tempSMVFile.getAbsolutePath()
                                    + ". And then run NuSMV on it.");

                            /* Timing */ timing.startTimer(timerName, "-regression (" + i + ")");
                            System.out.println("Running Regression tests on: " + specFile.getAbsolutePath());
                            StringBuilder sb = dapp.toSMVString(debug);
                            try (BufferedWriter writer = Files
                                    .newBufferedWriter(Paths.get(tempSMVFile.getAbsolutePath()))) {
                                writer.append(sb);
                            }

                            NuSMV nusmv = new NuSMV(selectedSolver.getAbsolutePath());
                            nusmv.writeNuSMVOutput = debug;
                            nusmv.SMCFile(tempSMVFile.getAbsolutePath());

                            int correctQueries = 0, incorrectQueries = 0, ignoredQueries = 0;
                            for (CounterExample ce : nusmv.counterExamples) {
                                QueryI q = dapp.queryExpectedResult.getOrDefault(ce.queryName, null);
                                String expectedResult = (q == null) ? "No Query Found" : q.getExpectedResult();
                                if (expectedResult == null) {
                                    expectedResult = "None";
                                }

                                if ((expectedResult.equals("REACHABLE") && !ce.goalReached)
                                        || (expectedResult.equals("UNREACHABLE") && ce.goalReached)) {
                                    logger.warning("[RUN] Query " + q.getOriginalQueryString(ce.queryName()) + " ["
                                            + ce.specification + "]: " + ce.getReachabilityString()
                                            + " (Expected Result: " + expectedResult + ")");

                                    if (ce.goalReached) {
                                        logger.info("[RUN] Rule Progression for Query "
                                                + q.getOriginalQueryString(ce.queryName()) + ":\n"
                                                + ce.ruleProgression.toString());
                                        logger.fine("[RUN] Counter Example (" + ce.numberOfSteps() + " steps):\n" + ce);
                                    }

                                    incorrectQueries++;
                                } else
                                    if (!expectedResult.equals("REACHABLE") && !expectedResult.equals("UNREACHABLE")) {
                                        logger.info("[RUN] No Expected Result for Query " + ce.queryName() + " ["
                                                + ce.specification + "]: " + ce.getReachabilityString());
                                        ignoredQueries++;
                                    } else {
                                        correctQueries++;
                                    }
                            }
                            int totalQueries = correctQueries + incorrectQueries;
                            /* Timing */ timing.stopTimer(timerName, "-regression (" + i + ")");
                            String resultStr = String.format(
                                    "Results for %s:\nDuration: %s\nCorrect Queries: %d/%d\nIncorrect Queries: %d/%d\nIgnored Queries: %d",
                                    specFile.getAbsolutePath(), timing.getLastElapsedTimePretty(), correctQueries,
                                    totalQueries, incorrectQueries, totalQueries, ignoredQueries);
                            logger.info(resultStr);
                            System.out.println(resultStr);
                            System.out.println("===");
                        }
                            break;
                        case "list" : {
                            logger.info("[LIST ACTION] Will list the contracts that it found in the file.");

                            Integer count = 1;
                            for (SmartContract sc : dapp.contracts) {
                                logger.info("Contract " + count + " - " + sc.identifier + "\n" + sc.toString());
                                count++;
                            }
                        }
                            break;
                        case "smv" : {
                            logger.info("[SMV ACTION] Converts the contracts into a NuSMV file format and "
                                    + "prints the output into the log/terminal.");

                            System.out.println(dapp.toSMVString(debug));
                        }
                            break;
                        case "smvFile" : {
                            logger.info("[SMVFILE ACTION] Converts the contracts into a NuSMV file format and "
                                    + "writes it to the SMV with the same filename + '.smv'.");

                            StringBuilder sb = dapp.toSMVString(debug);

                            try (BufferedWriter writer = Files
                                    .newBufferedWriter(Paths.get(specFile.getAbsoluteFile() + ".smv"))) {
                                writer.append(sb);
                            }
                        }
                            break;
                        case "dot" : {
                            logger.info("[DOT ACTION] Converts the contracts into a DOT directed graphs and "
                                    + "prints the output into the log/terminal. "
                                    + "Use https://dreampuf.github.io/GraphvizOnline/ to view online.");

                            StringBuilder sb = new StringBuilder();
                            sb.append("digraph {\nrankdir=LR;\nnode[shape=box];\n");
                            sb.append(dapp.toDOTString(dapp.startState, true, debug));
                            sb.append("}");
                            System.out.println(sb);
                        }
                            break;
                        case "dotHTML" : {
                            logger.info("[DOTHTML ACTION] Will convert the contracts into a DOT directed graph and "
                                    + "write to an html file in the same folder as the contract");

                            StringBuilder sb = dapp.toDOTHTMLString(true, debug);

                            try (BufferedWriter writer = Files
                                    .newBufferedWriter(Paths.get(specFile.getAbsoluteFile() + ".html"))) {
                                writer.append(sb);
                            }
                        }
                            break;
                        default :
                            logger.severe("The Run Option '" + runVal + "' has not been implemented. "
                                    + "Please see use '-help' to see which Run Options have been implemented");
                            /* TIMING */ timing.cancelTimer(timerName);
                            continue;
                    }
                    // END OF RUN A CERTAIN TASK ON THE MOHAWK+T FILE
                    ////////////////////////////////////////////////////////////////////////////////

                    /* TIMING */ timing.stopTimer(timerName);
                }
                // END OF INDIVIDUAL FILE LOOP
                ////////////////////////////////////////////////////////////////////////////////
            }

            logger.info("[EOF] Solidity Safety is done running");

            logger.config("[TIMING] " + timing);
            timing.writeOut(new File(timingFile), false);
        } catch (ParseException e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            logger.severe(errors.toString());
            logger.severe(e.getMessage());

            return -2;
        } catch (Exception e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            logger.severe(errors.toString());
            logger.severe(e.getMessage());

            return -1;
        } finally {
            for (Handler h : logger.getHandlers()) {
                h.close();// must call h.close or a .LCK file will remain.
            }
        }
        return 0;
    }

    /* ********* FUNCTIONS ********* */
    public Level getLoggerLevel() {
        return logger.getLevel();
    }

    public void setLoggerLevel(Level loggerLevel) {
        logger.setLevel(loggerLevel);
        if (consoleHandler != null) consoleHandler.setLevel(loggerLevel);
        if (fileHandler != null) fileHandler.setLevel(loggerLevel);
    }

    public void printHelp(CommandLine cmd, Options options) throws NumberFormatException {
        if (cmd.hasOption(SmartContractSafetyOptionString.MAXW.toString())) {
            try {
                Integer maxw = Integer.decode(cmd.getOptionValue(SmartContractSafetyOptionString.MAXW.toString()));
                printHelp(options, maxw);
            } catch (NumberFormatException e) {
                printHelp(options, 80);

                e.printStackTrace();
                throw new NumberFormatException("An error occured when trying to print out the help options!");
            }
        } else {
            printHelp(options, 80);
        }
    }

    public void printHelp(Options options, int maxw) {
        HelpFormatter f = new HelpFormatter();
        f.printHelp(maxw, "cree",
                StringUtils.repeat("-", maxw) + "\nAuthors: " + AUTHORS + "\n" + StringUtils.repeat("-", 20), options,
                StringUtils.repeat("-", maxw), true);
    }

    public CommandLine init(String[] args) throws ParseException, SecurityException, IOException, Exception {
        Options options = new Options();
        setupOptions(options);
        CommandLineParser parser = new BasicParser();
        CommandLine cmd = parser.parse(options, args);

        setupLoggerOptions(cmd, options);
        if (setupReturnImmediatelyOptions(cmd, options)) { return null; }

        setupUserPreferenceOptions(cmd, options);
        setupFileOptions(cmd, options);
        setupControlOptions(cmd, options);

        return cmd;
    }

    /**
     * Adds all of the available options to input parameter.
     */
    @SuppressWarnings("static-access")
    public void setupOptions(Options options) {
        // Add Information Options
        options.addOption(SmartContractSafetyOptionString.HELP.toString(), false, "Print this message");
        options.addOption(SmartContractSafetyOptionString.VERSION.toString(), false,
                "Prints the version (" + VERSION + ") information");

        // Add Logging Level Options
        options.addOption(OptionBuilder.withArgName("level")
                .withDescription("Sets the logging level. Accepted Values: severe|warning|info|config|fine|finest. "
                        + "Default logging level is: " + defaultLogLevel)
                .hasArg().create(SmartContractSafetyOptionString.LOGLEVEL.toString()));

        options.addOption(OptionBuilder.withArgName("logfile|'n'|'u'")
                .withDescription("The filepath where the log file should be created; "
                        + "No file will be created when equal to 'n'; "
                        + "A unique filename will be created when equal to 'u'; " + "default it creates a log called '"
                        + Logger_filepath + "'")
                .hasArg().create(SmartContractSafetyOptionString.LOGFILE.toString()));

        options.addOption(SmartContractSafetyOptionString.NOHEADER.toString(), false,
                "Does not write the CSV file header to the output log");

        // custom Console Logging Options
        options.addOption(
                OptionBuilder.withArgName("num").withDescription("The maximum width of the console (default 120)")
                        .hasArg().create(SmartContractSafetyOptionString.MAXW.toString()));

        options.addOption(OptionBuilder.withArgName("string")
                .withDescription("The new line string when wrapping a long line (default '\\n    ')").hasArg()
                .create(SmartContractSafetyOptionString.LINESTR.toString()));

        options.addOption(SmartContractSafetyOptionString.DEBUG.toString(), false,
                "Add this flag to get more ouput about the steps that are taken (drastically slows down performance)");

        options.addOption(SmartContractSafetyOptionString.NOROLLBACK.toString(), false,
                "Add this flag to deactivate rollback feature");

        // Add File IO Options
        options.addOption(OptionBuilder.withArgName("file|folder")
                .withDescription("Path to the ATRBAC Spec file or Folder if the 'bulk' option is set").hasArg()
                .create(SmartContractSafetyOptionString.SPECFILE.toString()));

        options.addOption(OptionBuilder.withArgName("extension")
                .withDescription(
                        "File extention used when searching for SPEC files when the 'bulk' option is used. Default:'"
                                + fileHelper.fileExt + "'")
                .hasArg().create(SmartContractSafetyOptionString.SPECEXT.toString()));

        ///////////////////////////////////////////////////////////////////////////////////////////
        // Add Functional Options
        options.addOption(SmartContractSafetyOptionString.BULK.toString(), false,
                "Use the folder that rbacspec points to and run against all *.spec");
        ///////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////
        // Add Actionable Options
        options.addOption(OptionBuilder.withArgName("solve|regression|list|dot|dotHTML|smv|smvFile")
                .withDescription("solve - solve all queries found in the smart contracts; "
                        + "reg|regression - solve all queries and only show messages for queries that fail to equal expected value; "
                        + "list - shows the solidity files as it sees them; "
                        + "dot - converts to DOT graph language and prints to terminal; "
                        + "dotHTML - converts to DOT and saves to an HTML file; "
                        + "smv - converts to SMV and prints to the terminal; "
                        + "smvFile - converts to SMV and saves to a file.")
                .hasArg().create(SmartContractSafetyOptionString.RUN.toString()));
        ///////////////////////////////////////////////////////////////////////////////////////////
    }

    /**
     * Sets up the options that do not process anything in detail, but returns a specific informative result.<br/>
     * Examples include:
     * <ul>
     * <li>Help
     * <li>Version
     * <li>Check NuSMV programs
     * </ul>
     *
     * @param cmd     The CommandLine object
     * @param options The available options
     * @return TRUE if the program should exit immediately, FALSE otherwise
     * @throws NumberFormatException
     */
    private Boolean setupReturnImmediatelyOptions(CommandLine cmd, Options options) throws NumberFormatException {
        if (cmd.hasOption(SmartContractSafetyOptionString.HELP.toString()) == true || cmd.getOptions().length < 1) {
            printHelp(cmd, options);
            return true;
        }

        if (cmd.hasOption(SmartContractSafetyOptionString.VERSION.toString())) {
            // keep it as simple as possible for the version
            System.out.println(VERSION);
            return true;
        }

        return false;
    }

    private void setupLoggerOptions(CommandLine cmd, Options options) throws SecurityException, IOException {
        // Logging Level
        logger.setUseParentHandlers(false);
        consoleHandler.setFormatter(new ShahenConsoleFormatter());
        setLoggerLevel(defaultLogLevel);// Default Level
        if (cmd.hasOption(SmartContractSafetyOptionString.LOGLEVEL.toString())) {
            String loglevel = cmd.getOptionValue(SmartContractSafetyOptionString.LOGLEVEL.toString());
            if (loglevel.equalsIgnoreCase("warning")) {
                setLoggerLevel(Level.WARNING);
            } else if (loglevel.equalsIgnoreCase("finest")) {
                setLoggerLevel(Level.FINEST);
            } else if (loglevel.equalsIgnoreCase("severe")) {
                setLoggerLevel(Level.SEVERE);
            } else if (loglevel.equalsIgnoreCase("fine")) {
                setLoggerLevel(Level.FINE);
            } else if (loglevel.equalsIgnoreCase("info")) {
                setLoggerLevel(Level.INFO);
            } else if (loglevel.equalsIgnoreCase("config")) {
                setLoggerLevel(Level.CONFIG);
            }
        }

        logger.addHandler(consoleHandler);

        // Add CSV File Headers
        if (cmd.hasOption(SmartContractSafetyOptionString.NOHEADER.toString())) {
            WriteCSVFileHeader = false;
        }

        // Set File Logger
        if (cmd.hasOption(SmartContractSafetyOptionString.LOGFILE.toString())) {
            // Check if no log file was requested
            if (cmd.getOptionValue(SmartContractSafetyOptionString.LOGFILE.toString()).equals("n")) {
                // Create no log file
                Logger_filepath = "";
            } else if (cmd.getOptionValue(SmartContractSafetyOptionString.LOGFILE.toString()).equals("u")) {
                // Create a unique log file
                Logger_filepath = "cree-log.%u.%g.txt";
            } else {
                try {
                    // Create a log file with a specific name
                    File logfile = new File(cmd.getOptionValue(SmartContractSafetyOptionString.LOGFILE.toString()));

                    if (!logfile.exists()) {
                        logfile.createNewFile();
                    }
                    Logger_filepath = logfile.getAbsolutePath();

                    if (WriteCSVFileHeader) {
                        FileOutputStream writer = new FileOutputStream(logfile, true);// Always append!
                        writer.write(ShahenCSVFileFormatter.csvHeaders().getBytes());
                        writer.flush();
                        writer.close();
                    }

                } catch (IOException e) {
                    logger.severe(e.getMessage());
                    return;
                }
            }
        }

        // Add Logger File Handler
        if (!Logger_filepath.isEmpty()) {

            File logfile = new File(Logger_filepath);

            if (!logfile.exists()) {
                logfile.getParentFile().mkdirs();

                logfile.createNewFile();
                if (WriteCSVFileHeader) {
                    FileOutputStream writer = new FileOutputStream(logfile, true);// Always append!
                    writer.write(ShahenCSVFileFormatter.csvHeaders().getBytes());
                    writer.flush();
                    writer.close();
                }
            }

            fileHandler = new FileHandler(Logger_filepath, true);
            fileHandler.setLevel(getLoggerLevel());
            fileHandler.setFormatter(new ShahenCSVFileFormatter());
            logger.addHandler(fileHandler);
        }
    }

    /**
     * Looks for the following options and sets up the program accordingly:
     * <ul>
     * <li>{@link SmartContractSafetyOptionString#DEBUG}
     * </ul>
     */
    private void setupControlOptions(CommandLine cmd, Options options) {
        /////////////////////////////////////////////////////////////////////////
        // DEBUG FLAG
        if (cmd.hasOption(SmartContractSafetyOptionString.DEBUG.toString())) {
            debug = true;
            if (!cmd.hasOption(SmartContractSafetyOptionString.LOGLEVEL.toString())) {
                setLoggerLevel(Level.FINEST);
            }
        } else {
            debug = false;
        }
        logger.config("[Option] Debug Mode: " + ((debug) ? "ENABLED" : "DISABLED"));
        /////////////////////////////////////////////////////////////////////////
        // ROLLBACK FLAG
        noRollback = cmd.hasOption(SmartContractSafetyOptionString.NOROLLBACK.toString());
        /////////////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////////
        // WHICH PROCESS
        if (SystemUtils.IS_OS_WINDOWS) {
            selectedSolver = nusmv_win;
            if (cmd.hasOption(SmartContractSafetyOptionString.PROGRAM.toString())) {
                switch (cmd.getOptionValue(SmartContractSafetyOptionString.PROGRAM.toString())) {
                    case "nusmv" :
                    case "NuSMV" :
                    case "NUSMV" :
                        selectedSolver = nusmv_win;
                        break;
                    case "zchaff" :
                    case "ZChaff" :
                    case "ZCHAFF" :
                        selectedSolver = nusmv_zchaff_win;
                        break;
                    case "nuxmv" :
                    case "nuXmv" :
                    case "NUXMV" :
                        selectedSolver = nuxmv_win;
                        break;
                }
            }
        } else if (SystemUtils.IS_OS_LINUX) {
            selectedSolver = nusmv_linux;
            if (cmd.hasOption(SmartContractSafetyOptionString.PROGRAM.toString())) {
                switch (cmd.getOptionValue(SmartContractSafetyOptionString.PROGRAM.toString())) {
                    case "nusmv" :
                    case "NuSMV" :
                    case "NUSMV" :
                        selectedSolver = nusmv_linux;
                        break;
                    case "zchaff" :
                    case "ZChaff" :
                    case "ZCHAFF" :
                        selectedSolver = nusmv_zchaff_linux;
                        break;
                    case "nuxmv" :
                    case "nuXmv" :
                    case "NUXMV" :
                        selectedSolver = nuxmv_linux;
                        break;
                }
            }
        } else if (SystemUtils.IS_OS_MAC) {
            selectedSolver = nusmv_mac;
            if (cmd.hasOption(SmartContractSafetyOptionString.PROGRAM.toString())) {
                switch (cmd.getOptionValue(SmartContractSafetyOptionString.PROGRAM.toString())) {
                    case "nusmv" :
                    case "NuSMV" :
                    case "NUSMV" :
                        selectedSolver = nusmv_mac;
                        break;
                    case "zchaff" :
                    case "ZChaff" :
                    case "ZCHAFF" :
                        selectedSolver = nusmv_zchaff_mac;
                        break;
                    case "nuxmv" :
                    case "nuXmv" :
                    case "NUXMV" :
                        selectedSolver = nuxmv_mac;
                        break;
                }
            }
        }

        logger.config("[Option] NuSMV Process: " + selectedSolver.getAbsolutePath());
        /////////////////////////////////////////////////////////////////////////
    }

    private void setupFileOptions(CommandLine cmd, Options options) {
        // Grab the SPEC file
        if (cmd.hasOption(SmartContractSafetyOptionString.SPECFILE.toString())) {
            logger.config("[OPTION] Using a specific SPEC File: "
                    + cmd.getOptionValue(SmartContractSafetyOptionString.SPECFILE.toString()));
        } else {
            logger.config("[OPTION] No Spec File included");
        }

        if (cmd.hasOption(SmartContractSafetyOptionString.SPECEXT.toString())) {
            logger.config("[OPTION] Using a specific SPEC File Extension: "
                    + cmd.getOptionValue(SmartContractSafetyOptionString.SPECEXT.toString()));
            fileHelper.fileExt = cmd.getOptionValue(SmartContractSafetyOptionString.SPECEXT.toString());
        } else {
            logger.config("[OPTION] Using the default SPEC File Extension: " + fileHelper.fileExt);
        }

        // Load more than one file from the SPEC File?
        if (cmd.hasOption(SmartContractSafetyOptionString.BULK.toString())) {
            logger.config("[OPTION] Bulk SPEC File inclusion: Enabled");
            fileHelper.bulk = true;
        } else {
            logger.config("[OPTION] Bulk SPEC File inclusion: Disabled");
            fileHelper.bulk = false;
        }
    }

    private void setupUserPreferenceOptions(CommandLine cmd, Options options) {
        // Set the Console's Max Width
        if (cmd.hasOption(SmartContractSafetyOptionString.MAXW.toString())) {
            logger.config("[OPTION] Setting the console's maximum width");
            String maxw = "";
            try {
                maxw = cmd.getOptionValue(SmartContractSafetyOptionString.MAXW.toString());
                ((ShahenConsoleFormatter) consoleHandler.getFormatter()).maxWidth = Integer.decode(maxw);
            } catch (NumberFormatException e) {
                logger.severe("[ERROR] Could not decode 'maxw': " + maxw + ";\n" + e.getMessage());
            }
        } else {
            logger.config("[OPTION] No Console Maximum Width Used.");
            ((ShahenConsoleFormatter) consoleHandler.getFormatter()).maxWidth = 0;
        }

        // Set the Console's Wrap String
        if (cmd.hasOption(SmartContractSafetyOptionString.LINESTR.toString())) {
            logger.config("[OPTION] Setting the console's new line string");
            ((ShahenConsoleFormatter) consoleHandler.getFormatter()).newLineStr = cmd
                    .getOptionValue(SmartContractSafetyOptionString.LINESTR.toString());
        } else {
            logger.config("[OPTION] Default Line String Used");
        }

    }
}
