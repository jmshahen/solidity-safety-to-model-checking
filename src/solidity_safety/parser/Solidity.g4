// Just parser stuff: Copyright 2016-2017 Federico Bond <federicobond@gmail.com> Link to original
// version(2019-12-02):
// https://raw.githubusercontent.com/antlr/grammars-v4/master/solidity/Solidity.g4 Copyright 2019

// Java additions: Jonathan Shahen Copyright 2019

grammar Solidity;

@header {
package solidity_safety.parser;

import java.util.ArrayList;
import solidity_safety.constants.SMVConstants;
import solidity_safety.ethereum.*;
import solidity_safety.ethereum.data.*;
import solidity_safety.ethereum.function.*;
import solidity_safety.ethereum.statemachine.*;
import solidity_safety.ethereum.statemachine.blocks.*;
import solidity_safety.ethereum.statemachine.states.*;
import solidity_safety.ethereum.query.*;
}

@members {
  public DApp dapp = new DApp();
}

sourceUnit:
	(
		pragmaDirective
		| importDirective
		| contractDefinition
		| specialOptionalParameters
		// | interfaceDefinition | libraryDefinition
	)* EOF {
		dapp.dappLoaded();
	};

specialOptionalParameters:
	'/*!UINT_MAX:' neg='-'? value = DecimalNumber ';*/'
	{
		try {
	    	SMVConstants.NuSMV_MAX_UINT = Integer.parseInt((($neg == null)? "" : $neg.text) + $value.text);
		} catch (Exception e) {
			throw new RuntimeException("Value must be an integer: "+ $value.text);
		}
	}
	|'/*!UINT_MIN:' neg='-'? value = DecimalNumber ';*/'
	{
		try {
        SMVConstants.NuSMV_MIN_UINT = Integer.parseInt((($neg == null)? "" : $neg.text) + $value.text);
		} catch (Exception e) {
			throw new RuntimeException("Value must be an integer: "+ $value.text);
		}
    }
    |'/*!INT_MAX:' neg='-'? value = DecimalNumber ';*/'
    {
		try {
        SMVConstants.NuSMV_MAX_INT = Integer.parseInt((($neg == null)? "" : $neg.text) + $value.text);
		} catch (Exception e) {
			throw new RuntimeException("Value must be an integer: "+ $value.text);
		}
    }
    |'/*!INT_MIN:' neg='-'? value = DecimalNumber ';*/'
    {
		try {
        SMVConstants.NuSMV_MIN_INT = Integer.parseInt((($neg == null)? "" : $neg.text) + $value.text);
		} catch (Exception e) {
			throw new RuntimeException("Value must be an integer: "+ $value.text);
		}
    }
    |'/*!AttackGasLimit:' value = DecimalNumber ';*/'
    {
		try {
        dapp.attackerGasLimit = Long.parseUnsignedLong($value.text);
		} catch (Exception e) {
			throw new RuntimeException("Value must be an integer: "+ $value.text);
		}
    };

pragmaDirective: 'pragma' pragmaName pragmaValue ';';

pragmaName: identifier;

pragmaValue: version | expression;

version: versionConstraint versionConstraint?;

versionOperator: '^' | '~' | '>=' | '>' | '<' | '<=' | '=';

versionConstraint: versionOperator? VersionLiteral;

importDeclaration: identifier ('as' identifier)?;

importDirective:
	'import' StringLiteral ('as' identifier)? ';'
	| 'import' ('*' | identifier) ('as' identifier)? 'from' StringLiteral ';'
	| 'import' '{' importDeclaration (',' importDeclaration)* '}' 'from' StringLiteral ';';

// SUPPORTS SUBCLASSING contractDefinition: 'contract' identifier ( 'is' inheritanceSpecifier (','
// inheritanceSpecifier)* )? '{' contractPart* '}';

// DOES NOT SUPPORT SUBCLASSING
contractDefinition:
	{
		SmartContract sc = new SmartContract(dapp);
	} 'contract' contractName = identifier {sc.identifier = $contractName.text; } '{' contractPart[sc]
		* '}' {
	};

// interfaceDefinition: 'interface' identifier ( 'is' inheritanceSpecifier (','
// inheritanceSpecifier)* )? '{' contractPart* '}'; libraryDefinition: 'library' identifier ( 'is'
// inheritanceSpecifier (',' inheritanceSpecifier)* )? '{' contractPart* '}';

inheritanceSpecifier:
	userDefinedTypeName ('(' expression ( ',' expression)* ')')?;

contractPart[SmartContract sc]:
	stateVariableDeclaration[$sc]
	| functionDefinition[$sc]
	| constructorDefinition[$sc]
	| modifierDefinition[$sc]
	| queryInContract[$sc]
	| usingForDeclaration
	| structDefinition
	| eventDefinition
	| enumDefinition[$sc];


stateVariableDeclaration[SmartContract sc] throws InvalidValueForDateType:
	typeName (
		PublicKeyword
		| InternalKeyword
		| PrivateKeyword
		| ConstantKeyword
	)* identifier ('=' expression)? ';' {
		try {
		if(sc.isTypeSupported($typeName.text)){
		    sc.addStateVariable($typeName.text, $identifier.text, $expression.text);
		}else{
		    sc.addUserDefinedStateVariable($typeName.text, $identifier.text, $expression.text, "enum");
		}
		}catch(Exception e) {
			throw new RuntimeException("State Variable is unsupported: '"+$typeName.text+" "+
				$identifier.text+(($expression.ctx == null)? "" : $expression.text)+"'");
		}
};

usingForDeclaration:
	'using' identifier 'for' ('*' | typeName) ';';

structDefinition:
	'struct' id = identifier '{' (
		variableDeclaration[VariableScope.StructVariable, $id.text] ';' (
			variableDeclaration[VariableScope.StructVariable, $id.text] ';'
		)*
	)? '}';

constructorDefinition[SmartContract sc]:
	'constructor' pl = parameterList m = modifierList {
		ConstructorFunction constructor = new ConstructorFunction($sc, $pl.params, $m.modifiers, $m.require_modifiers);
		constructor.startDefinition();
	} block[constructor.block] {
		constructor.endDefinition();
	};

modifierDefinition[SmartContract sc]:
	'modifier' i = identifier pl = parameterList? {
		ModifierFunction modifier = new ModifierFunction($sc, $i.text, 
			($pl.ctx == null)? new ArrayList<Variable>() : $pl.params);
			modifier.startDefinition();
	} block[modifier.block] {
		modifier.endDefinition();
	};

modifierInvocation: identifier ( '(' expressionList? ')')?;

functionDefinition[SmartContract sc]:
	'function' i = identifier? pl = parameterList m = modifierList r = returnParameters? {
		Function f = new Function($sc, ($i.ctx == null)? "" : $i.text, $pl.params, $m.modifiers, 
			$m.require_modifiers, ($r.ctx == null)? new ArrayList<Variable>() : $r.params);
		f.startDefinition();
		} (';' | block[f.block]) {
			f.endDefinition();
		};

returnParameters
	returns[ArrayList<Variable> params]:
	'returns' p = parameterList {
	$params = $p.params;
 };

modifierList
	returns[ArrayList<String> modifiers, ArrayList<String> require_modifiers]:
	{
	$modifiers = new ArrayList<String>();
	$require_modifiers = new ArrayList<String>();
} (
		r = modifierInvocation {$require_modifiers.add($r.text);}
		| a = stateMutability {$modifiers.add($a.text);}
		| b = ExternalKeyword {$modifiers.add($b.text);}
		| c = PublicKeyword {$modifiers.add($c.text);}
		| d = InternalKeyword {$modifiers.add($d.text);}
		| e = PrivateKeyword {$modifiers.add($e.text);}
	)*;

eventDefinition:
	'event' identifier eventParameterList AnonymousKeyword? ';';

enumValue: identifier;

enumDefinition[SmartContract sc]:
    {
    ArrayList<String> enumValues = new ArrayList<String>();
    }
	'enum' identifier '{' (a = enumValue{enumValues.add($a.text);})? (',' b = enumValue{enumValues.add($b.text);})* '}'
	{
	sc.createNewEnum($identifier.text, enumValues);
	};

parameterList
	returns[ArrayList<Variable> params]:
	{$params = new ArrayList<Variable>();} '(' (
		a = parameter {$params.add($a.param);} (',' parameter)*
	)? ')';

parameter
	returns[Variable param]:
	typeName storageLocation? identifier? {$param = new Variable(VariableScope.ParameterVariable, "", $typeName.text, $identifier.text);
		};

eventParameterList:
	'(' (eventParameter (',' eventParameter)*)? ')';

eventParameter: typeName IndexedKeyword? identifier?;

functionTypeParameterList:
	'(' (functionTypeParameter (',' functionTypeParameter)*)? ')';

functionTypeParameter: typeName storageLocation?;

variableDeclaration[VariableScope vs, String scopeName]
	returns[Variable var]:
	typeName storageLocation? identifier {
		$var = new Variable($vs, $scopeName, $typeName.text, $identifier.text);
	};

typeName:
	elementaryTypeName
	| userDefinedTypeName
	| mapping
	| typeName '[' expression? ']'
	| functionTypeName;

userDefinedTypeName: identifier ( '.' identifier)*;

mapping: 'mapping' '(' elementaryTypeName '=>' typeName ')';

functionTypeName:
	'function' functionTypeParameterList (
		InternalKeyword
		| ExternalKeyword
		| stateMutability
	)* ('returns' functionTypeParameterList)?;

storageLocation: 'memory' | 'storage' | 'calldata';

stateMutability:
	PureKeyword
	| ConstantKeyword
	| ViewKeyword
	| PayableKeyword;

block[BlockI b]: '{' (s = statement[$b])* '}';

statement[BlockI b]:
	simpleStatement[$b]
	| modifyVariableValue[$b]
	| ifStatement[$b]
	| requireStatement[$b]
	| functionModifierSympoleStatement[$b]
	| whileStatement[$b]
	| forStatement[$b]
	| block[$b]
	| queryInBlock[$b]
/*| inlineAssemblyStatement*/
	| doWhileStatement[$b]
	| continueStatement
	| breakStatement
	| returnStatement
	| throwStatement
	| emitStatement;

// I MADE THIS
queryInBlock[BlockI b]:
	'/*!QUERY:' ltl_expr ';' (expected_result=('REACHABLE' | 'UNREACHABLE'))? '*/' {
		$b.addState(new StateTransition(
			new QueryState($b, $ltl_expr.text, 
				($expected_result == null)?null : $expected_result.text,
				_ctx.getStart().getLine()
			), Condition.getTrueCondition()));
	};
// I MADE THIS
queryInContract[SmartContract sc]:
	'/*!QUERY:' ltl_expr ';' (expected_result=('REACHABLE' | 'UNREACHABLE'))? '*/' {
		$sc.addQuery(
			new BoolExprReachabilityQuery($sc, $ltl_expr.text,
				($expected_result == null)?null : $expected_result.text,
				_ctx.getStart().getLine()
			));
	};

functionModifierSympoleStatement[BlockI b]:
    '_;'{
            $b.addState(new StateTransition(new EmptyState($b), Condition.getTrueCondition()));
            $b.addState(new StateTransition(new FunctionModifierSymbolState($b), Condition.getTrueCondition()));
            $b.addState(new StateTransition(new EmptyState($b), Condition.getTrueCondition()));
            $b.addState(new StateTransition(new EmptyState($b), Condition.getTrueCondition()));
        };

// I MADE THIS
modifyVariableValue[BlockI b]:
	id = identifier equalSign = (
		'='
		| '+='
		| '-='
		| '*='
		| '/='
		/*
		 | '|='
 | '^='
 | '&='
 | '<<='
 | '>>='
 | '%='
		 */
	) rightSide = expression ';' {
		Variable var = $b.getParent().getVariable($id.text);
		$b.addState(new StateTransition(new DataChangeState($b, var, $equalSign.text, $rightSide.text), Condition.getTrueCondition()));
	};

expressionStatement[BlockI b]: expression ';';

ifStatement[BlockI b]:
	'if' '(' con = expression ')' {
		IfBlock ifBlock = new IfBlock($b, $con.text);
		$b.addNewBlock(ifBlock);
	} statement[ifBlock]{
		ifBlock.endBlock();
		BlockI elseBlock = ifBlock.getElseBlock();
	} (
		{
		} 'else' statement[elseBlock]
	)?
	{
		elseBlock.endBlock();
	};

requireStatement[BlockI b]:
	'require' '(' expression (',' StringLiteral)? ')' ';'{
	    RequireBlock requireBlock = new RequireBlock($b, $expression.text, dapp.noRollback);
        $b.addNewBlock(requireBlock);
        requireBlock.endBlock();
    };

whileStatement[BlockI b]:
	'while' '(' expression ')' statement[$b];

simpleStatement[BlockI b]: (
		variableDeclarationStatement[$b]
		| expressionStatement[$b]
	);

forStatement[BlockI b]:
	'for' '(' (simpleStatement[$b] | ';') expression? ';' expression? ')' statement[$b];

// inlineAssemblyStatement: 'assembly' StringLiteral? assemblyBlock;

doWhileStatement[BlockI b]:
	'do' statement[$b] 'while' '(' expression ')' ';';

continueStatement: 'continue' ';';

breakStatement: 'break' ';';

returnStatement: 'return' expression? ';';

throwStatement: 'throw' ';';

emitStatement: 'emit' functionCall ';';

variableDeclarationStatement[BlockI b]: (
		// 'var' identifierList
		v = variableDeclaration[VariableScope.LocalVariable, $b.getParent().identifier]
		// | '(' variableDeclarationList ')'
	) ('=' rightSide = expression)? ';' {
		$b.getParent().addLocalVariable($v.var);
		$b.addState(new StateTransition(new DataChangeState($b, $v.var, "=", 
			($rightSide.ctx == null)?null : $rightSide.text), Condition.getTrueCondition()));
	};

// variableDeclarationList: variableDeclaration? (',' variableDeclaration?)*;

identifierList: '(' ( identifier? ',')* identifier? ')';

elementaryTypeName:
	'address'
	| 'bool'
	| 'string'
	| 'var'
	| Int
	| Uint
	| 'byte'
	| Byte
	| Fixed
	| Ufixed;

expression:
	expression ('++' | '--')
	| 'new' typeName
	| expression '[' expression ']'
	| expression '(' functionCallArguments ')'
	| expression '.' identifier
	| '(' expression ')'
	| ('++' | '--') expression
	| ('+' | '-') expression
	| ('after' | 'delete') expression
	| '!' expression
	| '~' expression
	| expression '**' expression
	| expression ('*' | '/' | '%') expression
	| expression ('+' | '-') expression
	| expression ('<<' | '>>') expression
	| expression '&' expression
	| expression '^' expression
	| expression '|' expression
	| expression ('<' | '>' | '<=' | '>=') expression
	| expression ('==' | '!=') expression
	| expression '&&' expression
	| expression '||' expression
	| expression '?' expression ':' expression
	// | expression (
	| primaryExpression;
basic_expr:
	primaryExpression
	| '(' basic_expr ')'
	| '!' basic_expr // logical or bitwise NOT
	| 'abs(' basic_expr ')' // absolute value
	| 'max(' basic_expr ',' basic_expr ')' // max
	| 'min(' basic_expr ',' basic_expr ')' // min
	| basic_expr '&' basic_expr // logical or bitwise AND
	| basic_expr '|' basic_expr // logical or bitwise OR
	| basic_expr 'xor' basic_expr // logical or bitwise exclusive OR
	| basic_expr 'xnor' basic_expr // logical or bitwise NOT exclusive OR
	| basic_expr '->' basic_expr // logical or bitwise implication
	| basic_expr '<->' basic_expr // logical or bitwise equivalence
	| basic_expr '=' basic_expr // equality
	| basic_expr '!=' basic_expr // inequality
	| basic_expr '<' basic_expr // less than
	| basic_expr '>' basic_expr // greater than
	| basic_expr '<=' basic_expr // less than or equal
	| basic_expr '>=' basic_expr // greater than or equal
	| '-' basic_expr // integer unary minus
	| basic_expr '+' basic_expr // integer addition
	| basic_expr '-' basic_expr // integer subtraction
	| basic_expr '*' basic_expr // integer multiplication
	| basic_expr '/' basic_expr // integer division
	| basic_expr 'mod' basic_expr // integer remainder
	| basic_expr '>>' basic_expr // bit shift right
	| basic_expr '<<' basic_expr // bit shift left
	| basic_expr '[' numberLiteral ']' // index subscript
	| basic_expr '[' basic_expr ':' basic_expr ']'; // word bits selection

ltl_expr:
	// next_expr // a next boolean expression
	basic_expr
	| '(' ltl_expr ')'
	| '!' ltl_expr // logical not
	| ltl_expr '&' ltl_expr // logical and
	| ltl_expr '|' ltl_expr // logical or
	| ltl_expr 'xor' ltl_expr // logical exclusive or
	| ltl_expr 'xnor' ltl_expr // logical NOT exclusive or
	| ltl_expr '->' ltl_expr // logical implies
	| ltl_expr '<->' ltl_expr // logical equivalence
	// FUTURE
	| 'X' ltl_expr // next state
	| 'G' ltl_expr // globally
	// | 'G' bound ltl_expr // bounded globally
	| 'F' ltl_expr // finally
	// | 'F' bound ltl_expr // bounded finally
	| ltl_expr 'U' ltl_expr // until
	| ltl_expr 'V' ltl_expr // releases
	// PAST
	| 'Y' ltl_expr // previous state
	| 'Z' ltl_expr // not previous state not
	| 'H' ltl_expr // historically
	// | 'H' bound ltl_expr // bounded historically
	| 'O' ltl_expr // once
	// | 'O' bound ltl_expr // bounded once
	| ltl_expr 'S' ltl_expr // since
	| ltl_expr '.' ltl_expr // enum data uses dot
	| ltl_expr 'T' ltl_expr; // triggered


primaryExpression:
	BooleanLiteral
	| numberLiteral
	| HexLiteral
	| StringLiteral
	| identifier
	| tupleExpression
	| elementaryTypeNameExpression;

expressionList: expression (',' expression)*;

nameValueList: nameValue (',' nameValue)* ','?;

nameValue: identifier ':' expression;

functionCallArguments: '{' nameValueList? '}' | expressionList?;

functionCall: expression '(' functionCallArguments ')';

assemblyBlock: '{' assemblyItem* '}';

assemblyItem:
	identifier
	| assemblyBlock
	| assemblyExpression
	| assemblyLocalDefinition
	| assemblyAssignment
	| assemblyStackAssignment
	| labelDefinition
	| assemblySwitch
	| assemblyFunctionDefinition
	| assemblyFor
	| assemblyIf
	| BreakKeyword
	| ContinueKeyword
	| subAssembly
	| numberLiteral
	| StringLiteral
	| HexLiteral;

assemblyExpression: assemblyCall | assemblyLiteral;

assemblyCall: ('return' | 'address' | 'byte' | identifier) (
		'(' assemblyExpression? (',' assemblyExpression)* ')'
	)?;

assemblyLocalDefinition:
	'let' assemblyIdentifierOrList (':=' assemblyExpression)?;

assemblyAssignment:
	assemblyIdentifierOrList ':=' assemblyExpression;

assemblyIdentifierOrList:
	identifier
	| '(' assemblyIdentifierList ')';

assemblyIdentifierList: identifier ( ',' identifier)*;

assemblyStackAssignment: '=:' identifier;

labelDefinition: identifier ':';

assemblySwitch: 'switch' assemblyExpression assemblyCase*;

assemblyCase:
	'case' assemblyLiteral assemblyBlock
	| 'default' assemblyBlock;

assemblyFunctionDefinition:
	'function' identifier '(' assemblyIdentifierList? ')' assemblyFunctionReturns? assemblyBlock;

assemblyFunctionReturns: ( '->' assemblyIdentifierList);

assemblyFor:
	'for' (assemblyBlock | assemblyExpression) assemblyExpression (
		assemblyBlock
		| assemblyExpression
	) assemblyBlock;

assemblyIf: 'if' assemblyExpression assemblyBlock;

assemblyLiteral:
	StringLiteral
	| DecimalNumber
	| HexNumber
	| HexLiteral;

subAssembly: 'assembly' identifier assemblyBlock;

tupleExpression:
	'(' (expression? ( ',' expression?)*) ')'
	| '[' ( expression ( ',' expression)*)? ']';

elementaryTypeNameExpression: elementaryTypeName;

numberLiteral: (DecimalNumber | HexNumber) NumberUnit?;

identifier: ('from' | Identifier);

VersionLiteral: [0-9]+ '.' [0-9]+ '.' [0-9]+;

BooleanLiteral: 'true' | 'false';


DecimalNumber: ([0-9]+ | ([0-9]* '.' [0-9]+)) ([eE] [0-9]+)?;

HexNumber: '0x' HexCharacter+;

NumberUnit:
	'wei'
	| 'szabo'
	| 'finney'
	| 'ether'
	| 'seconds'
	| 'minutes'
	| 'hours'
	| 'days'
	| 'weeks'
	| 'years';

HexLiteral: 'hex' ('"' HexPair* '"' | '\'' HexPair* '\'');

Int:
	'int'
	| 'int8'
	| 'int16'
	| 'int24'
	| 'int32'
	| 'int40'
	| 'int48'
	| 'int56'
	| 'int64'
	| 'int72'
	| 'int80'
	| 'int88'
	| 'int96'
	| 'int104'
	| 'int112'
	| 'int120'
	| 'int128'
	| 'int136'
	| 'int144'
	| 'int152'
	| 'int160'
	| 'int168'
	| 'int176'
	| 'int184'
	| 'int192'
	| 'int200'
	| 'int208'
	| 'int216'
	| 'int224'
	| 'int232'
	| 'int240'
	| 'int248'
	| 'int256';

Uint:
	'uint'
	| 'uint8'
	| 'uint16'
	| 'uint24'
	| 'uint32'
	| 'uint40'
	| 'uint48'
	| 'uint56'
	| 'uint64'
	| 'uint72'
	| 'uint80'
	| 'uint88'
	| 'uint96'
	| 'uint104'
	| 'uint112'
	| 'uint120'
	| 'uint128'
	| 'uint136'
	| 'uint144'
	| 'uint152'
	| 'uint160'
	| 'uint168'
	| 'uint176'
	| 'uint184'
	| 'uint192'
	| 'uint200'
	| 'uint208'
	| 'uint216'
	| 'uint224'
	| 'uint232'
	| 'uint240'
	| 'uint248'
	| 'uint256';

Byte:
	'bytes'
	| 'bytes1'
	| 'bytes2'
	| 'bytes3'
	| 'bytes4'
	| 'bytes5'
	| 'bytes6'
	| 'bytes7'
	| 'bytes8'
	| 'bytes9'
	| 'bytes10'
	| 'bytes11'
	| 'bytes12'
	| 'bytes13'
	| 'bytes14'
	| 'bytes15'
	| 'bytes16'
	| 'bytes17'
	| 'bytes18'
	| 'bytes19'
	| 'bytes20'
	| 'bytes21'
	| 'bytes22'
	| 'bytes23'
	| 'bytes24'
	| 'bytes25'
	| 'bytes26'
	| 'bytes27'
	| 'bytes28'
	| 'bytes29'
	| 'bytes30'
	| 'bytes31'
	| 'bytes32';

Fixed: 'fixed' | ( 'fixed' [0-9]+ 'x' [0-9]+);

Ufixed: 'ufixed' | ( 'ufixed' [0-9]+ 'x' [0-9]+);

fragment HexPair: HexCharacter HexCharacter;

fragment HexCharacter: [0-9A-Fa-f];

ReservedKeyword:
	'abstract'
	| 'after'
	| 'case'
	| 'catch'
	| 'default'
	| 'final'
	| 'in'
	| 'inline'
	| 'let'
	| 'match'
	| 'null'
	| 'of'
	| 'relocatable'
	| 'static'
	| 'switch'
	| 'try'
	| 'type'
	| 'typeof';

AnonymousKeyword: 'anonymous';
BreakKeyword: 'break';
ConstantKeyword: 'constant';
ContinueKeyword: 'continue';
ExternalKeyword: 'external';
IndexedKeyword: 'indexed';
InternalKeyword: 'internal';
PayableKeyword: 'payable';
PrivateKeyword: 'private';
PublicKeyword: 'public';
PureKeyword: 'pure';
ViewKeyword: 'view';

Identifier: IdentifierStart IdentifierPart*;

fragment IdentifierStart: [a-zA-Z$_];

fragment IdentifierPart: [a-zA-Z0-9$_];

StringLiteral:
	'"' DoubleQuotedStringCharacter* '"'
	| '\'' SingleQuotedStringCharacter* '\'';

fragment DoubleQuotedStringCharacter: ~["\r\n\\] | ('\\' .);

fragment SingleQuotedStringCharacter: ~['\r\n\\] | ('\\' .);

WS: [ \t\r\n\u000C]+ -> skip;

COMMENT: '/*' (~'!')*? '*/' -> channel(HIDDEN);

LINE_COMMENT: '//' ~[\r\n]* -> channel(HIDDEN);