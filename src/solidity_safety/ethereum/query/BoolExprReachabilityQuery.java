package solidity_safety.ethereum.query;

import solidity_safety.constants.SMVConstants;
import solidity_safety.ethereum.SmartContract;
import solidity_safety.ethereum.statemachine.states.BaseState;

public class BoolExprReachabilityQuery extends BaseQuery implements QueryI {

    public BoolExprReachabilityQuery(SmartContract parent, String goalCondition, String expectedResult,
            Integer lineNumber) {
        super(parent, expectedResult);
        setOriginalQueryString(goalCondition, lineNumber);
        this.goalCondition = BaseState.cleanUpExpression(BaseState.removeDotPartInExpression(goalCondition));
    }

    @Override
    public String getQueryName(boolean verbose) {
        if (verbose) return "BoolExprReachabilityQuery_" + queryNumber;
        return "BERQ" + queryNumber;
    }

    @Override
    public String toSMV(boolean verbose) {
        return "CTLSPEC NAME " + getQueryName(verbose) + " := AG !(" + SMVConstants.QueryName + " = "
                + getQueryName(verbose) + " & " + SMVConstants.State + " = "
                + parent.parent.loopState.getStateName(verbose) + " & " + goalCondition + ");";
    }

}
