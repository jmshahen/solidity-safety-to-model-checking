package solidity_safety.ethereum.query;

public interface QueryI {
    /**
     * Used for debugging and referencing from internal Queries to the Original Solidity Query.
     * @param line The solidity comment line
     * @param lineNumber the line number that the comment is found on
     */
    public void setOriginalQueryString(String line, Integer lineNumber);

    public String toSMV(boolean verbose);

    /** Returns the name of the query, which is currently self generated. */
    public String getQueryName(boolean verbose);

    /**
     * If this is already set, then returns the string "[<b>line</b>](line: <b>lineNumber</b>)"
     * @param returnIfNotSet If the values <b>line</b> is NULL, then this value is returned
     */
    public String getOriginalQueryString(String returnIfNotSet);

    /** Returns the expected result: REACHABLE, UNREACHABLE.
     * ONLY IF the solidity file provides the expected result in the query. */
    public String getExpectedResult();

    /** Retrieves the condition of the Query. Used to update variables names for SMV. */
    public String getCondition();
    /** Sets teh condition of the Query. Used to update variable names for SMV. */
    public void setCondition(String condition);
    /** Returns the unique query number */
    public int getQueryNumber();
}
