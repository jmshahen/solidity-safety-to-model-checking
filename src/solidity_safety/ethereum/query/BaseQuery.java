package solidity_safety.ethereum.query;

import solidity_safety.ethereum.SmartContract;

public class BaseQuery implements QueryI {
    public static int next_query_number = 0;

    public int queryNumber;
    public SmartContract parent;
    public String expectedResult;
    /** Stores the edited Goal Condition to send to SMV Solver */
    public String goalCondition;
    /** Stores the UNEDITED query string; used for debugging and backwards referencing with Smart Contracts */
    public String originalQueryString;
    public Integer originalQueryLineNumber;

    public BaseQuery(SmartContract parent, String expectedResult) {
        this.parent = parent;
        this.expectedResult = expectedResult;
        this.queryNumber = next_query_number;
        next_query_number++;
    }

    @Override
    public String getQueryName(boolean verbose) {
        if (verbose) return "Query_" + queryNumber;
        return "Q" + queryNumber;
    }

    @Override
    public String getExpectedResult() {
        return expectedResult;
    }

    @Override
    public String toSMV(boolean verbose) {
        return null;
    }

    @Override
    public void setOriginalQueryString(String line, Integer lineNumber) {
        originalQueryString = line;
        originalQueryLineNumber = lineNumber;
    }

    @Override
    public String getOriginalQueryString(String returnIfNotSet) {
        if (originalQueryString == null) return returnIfNotSet;
        if (originalQueryLineNumber == null) return "[" + originalQueryString + "]";
        return "[" + originalQueryString + "](line:" + originalQueryLineNumber + ")";
    }

    @Override
    public String getCondition() {
        return goalCondition;
    }

    @Override
    public void setCondition(String condition) {
        goalCondition = condition;
    }

    @Override
    public int getQueryNumber() {
        return queryNumber;
    }

}
