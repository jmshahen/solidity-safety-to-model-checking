package solidity_safety.ethereum.query;

/**
 * This query allows for us to determine if an Account A can ever get the contract to reach a 
 * certain line B in the function (which is a state).
 * 
 * 
 * <p><b>NOTE:</b>
 * We require ALL <code>require()</code> statements to appear before the B line in this query in order for us to assume
 * that the function will be successfully added to the next block. If a require statement appears after and fails,
 * then the state is reverted and the ability for that account to reach that line is nullified.
 * Currently we are unable to code this condition, so the `require` statements appearing before that line will produce
 * correct results, whereas if they appear after then false positive situations can occur.   
 * @author Jonathan Shahen
 *
 */
public class AccountStateReachableQuery {// implements QueryI {
    // TODO: complete this Query type: AccountStateReachableQuery
}
