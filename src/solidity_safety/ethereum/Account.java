package solidity_safety.ethereum;

public class Account {
    public String address;
    public int balance;
    public Account(String address, int balance) {
        this.address = address;
        this.balance = balance;
    }
}
