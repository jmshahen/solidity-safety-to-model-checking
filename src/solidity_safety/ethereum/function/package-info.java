/**
 * Contains classes for different function types (Function, Constructor, Modifier) 
 */
package solidity_safety.ethereum.function;
