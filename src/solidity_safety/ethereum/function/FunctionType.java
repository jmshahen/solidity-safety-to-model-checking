package solidity_safety.ethereum.function;

public enum FunctionType {
    Function, ModifierFunction, ConstructorFunction;
}
