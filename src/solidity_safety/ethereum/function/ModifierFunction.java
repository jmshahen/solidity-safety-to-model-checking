package solidity_safety.ethereum.function;

import java.util.ArrayList;

import solidity_safety.ethereum.SmartContract;
import solidity_safety.ethereum.data.Variable;
import solidity_safety.ethereum.statemachine.StateTransition;
import solidity_safety.ethereum.statemachine.states.FunctionEntryState;
import solidity_safety.ethereum.statemachine.states.FunctionSuccessState;

public class ModifierFunction extends Function {

    public ModifierFunction(SmartContract parent, String identifier, ArrayList<Variable> parameters) {
        super(parent, identifier, parameters, new ArrayList<String>(), new ArrayList<String>(),
                new ArrayList<Variable>());
    }

    @Override
    public void startDefinition() {
        parent.addModifier(this);
        block.addState(StateTransition.newTrue(new FunctionEntryState(block)));
        parent.parent.loopState.addState(StateTransition.newTrue(block.getStartState()));
    }

    @Override
    public void endDefinition() {
        block.addState(StateTransition.newTrue(new FunctionSuccessState(block)));
        block.getLastState().addState(StateTransition.newTrue(parent.parent.loopState));
    }

    @Override
    public FunctionType getType() {
        return FunctionType.ModifierFunction;
    }

    @Override
    public String getStateName(boolean verbose) {
        String s = parent.getStateName(verbose);
        if (verbose) return s + "_" + identifier;
        return s + "mf" + number;
    }

    @Override
    public String getColour() {
        return "#B42324";
    }
}
