package solidity_safety.ethereum.function;

import java.util.*;

import solidity_safety.constants.SMVConstants;
import solidity_safety.ethereum.SmartContract;
import solidity_safety.ethereum.data.Variable;
import solidity_safety.ethereum.statemachine.StateTransition;
import solidity_safety.ethereum.statemachine.blocks.BlockI;
import solidity_safety.ethereum.statemachine.blocks.FunctionBlock;
import solidity_safety.ethereum.statemachine.states.*;

public class Function {
    public static String tabSpace = "    ";
    public SmartContract parent;
    /** A numeric identifier of the function within the SmartContract;
     * usually this number is just incremented for each new function in the smart contract. */
    public int number;
    public String identifier;
    public HashMap<String, Variable> parameters = new HashMap<>();
    public ArrayList<String> require_modifiers = new ArrayList<>();
    public HashMap<String, Variable> return_parameters = new HashMap<>();

    public boolean isPure = false;
    public boolean isConstant = false;
    public boolean isView = false;
    public boolean isPayable = false;
    public boolean isExternal = false;
    public boolean isPublic = false;
    public boolean isInternal = false;
    public boolean isPrivate = false;

    public BlockI block = new FunctionBlock(this);
    public HashMap<String, Variable> internal_variables = new HashMap<>();

    public Function() {}

    public Function(SmartContract parent, String identifier, ArrayList<Variable> parameters,
            ArrayList<String> modifiers, ArrayList<String> require_modifiers, ArrayList<Variable> return_parameters) {
        this.parent = parent;
        this.identifier = identifier;
        for (Variable v : parameters) {
            v.parentFunction = this;
            v.parentContract = parent;
            v.scopeName = identifier;
            this.parameters.put(v.identifier, v);
        }
        for (String m : require_modifiers) {
            m = m.split("\\(")[0];
            this.require_modifiers.add(m);
        }

        for (Variable v : return_parameters) {
            v.parentFunction = this;
            v.parentContract = parent;
            this.return_parameters.put(v.identifier, v);
        }

        if (modifiers != null) {
            for (String m : modifiers) {
                switch (m) {
                    case "pure" :
                        isPure = true;
                        break;
                    case "constant" :
                        isConstant = true;
                        break;
                    case "view" :
                        isView = true;
                        break;
                    case "payable" :
                        isPayable = true;
                        break;
                    case "external" :
                        isExternal = true;
                        break;
                    case "public" :
                        isPublic = true;
                        break;
                    case "internal" :
                        isInternal = true;
                        break;
                    case "private" :
                        isPrivate = true;
                        break;
                    default :

                }
            }
        }
    }

    /**
     * This function must be called before states are added to a function if using {@link #Function()}.
     */
    public void startDefinition() {
        parent.addFunction(this);
        block.addState(StateTransition.newTrue(new FunctionEntryState(block)));
        parent.parent.loopState.addState(StateTransition.newTrue(block.getStartState()));
    }
    /**
     * This function must be called when there are no more lines in the function. 
     */
    public void endDefinition() {
        block.addState(StateTransition.newTrue(new FunctionSuccessState(block)));
        block.getLastState().addState(StateTransition.newTrue(parent.parent.loopState));
    }

    public void addLocalVariable(Variable var) {
        internal_variables.put(var.identifier, var);
    }

    public String getString(String prefix) {
        StringBuilder sb = new StringBuilder();

        sb.append(getFunctionDefinition(prefix));

        sb.append("{\n");

        if (prefix != null) sb.append(prefix).append(prefix);
        sb.append("// ").append(block.numberOfStates()).append(" States\n");

        if (prefix != null) sb.append(prefix);
        sb.append("}");

        return sb.toString();
    }

    public StringBuilder getFunctionDefinition(String prefix) {
        StringBuilder sb = new StringBuilder();

        if (prefix != null) sb.append(prefix);
        sb.append("function ").append(identifier).append("(");
        boolean first = true;
        for (Variable v : parameters.values()) {
            if (first) {
                first = false;
            } else {
                sb.append(", ");
            }
            sb.append(v.typeName).append(" ").append(v.identifier);
        }
        sb.append(") ");

        if (isPublic) sb.append("public ");
        if (isPrivate) sb.append("private ");
        if (isPure) sb.append("pure ");
        if (isView) sb.append("view ");
        if (isExternal) sb.append("external ");
        if (isInternal) sb.append("internal ");
        if (isConstant) sb.append("constant ");
        if (isPayable) sb.append("payable ");

        if (return_parameters != null && return_parameters.size() > 0) {
            sb.append("returns(");
            first = true;
            for (Variable v : return_parameters.values()) {
                if (first) {
                    first = false;
                } else {
                    sb.append(", ");
                }
                sb.append(v.typeName);
                if (v.identifier != null && !v.identifier.equals("")) {
                    sb.append(" ").append(v.identifier);
                }
            }
            sb.append(") ");
        }

        return sb;
    }

    public Variable getVariable(String name) {
        if (internal_variables.containsKey(name)) return internal_variables.get(name);
        if (parameters.containsKey(name)) return parameters.get(name);
        return parent.getVariable(name);
    }

    public StringBuilder stateMachineSMV(String startStateName, boolean verbose) {
        StringBuilder sb = new StringBuilder();

        StateI state;
        HashSet<StateI> visited = new HashSet<StateI>();
        Queue<StateI> states = new LinkedList<StateI>();

        states.add(block.getStartState());

        while (states.size() > 0) {
            state = states.remove();
            visited.add(state);

            if (state.getStateTransitions().size() == 0) {
                sb.append(tabSpace).append(SMVConstants.State).append(" = ").append(state.getStateName(verbose))
                        .append(" : ").append(startStateName).append(";\n");
            } else {
                for (StateTransition st : state.getStateTransitions()) {
                    sb.append(tabSpace).append(st.condition.toSMV(state, st.nextState, parent.number, verbose));

                    if (!visited.contains(st.nextState)) {
                        states.add(st.nextState);
                    }
                }
            }
        }
        return sb;
    }

    public String getStateName(boolean verbose) {
        String s = parent.getStateName(verbose);
        if (verbose) return s + "_" + identifier;
        return s + "f" + number;
    }

    public FunctionType getType() {
        return FunctionType.Function;
    }

    public String getColour() {
        return "#A1FF51";
    }

    /** Returns the smart contract parent for this function */
    public SmartContract getParent() {
        return parent;
    }
}
