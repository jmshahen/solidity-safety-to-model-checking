package solidity_safety.ethereum.function;

import java.util.ArrayList;

import solidity_safety.ethereum.SmartContract;
import solidity_safety.ethereum.data.Variable;
import solidity_safety.ethereum.statemachine.StateTransition;
import solidity_safety.ethereum.statemachine.states.ConstructorEntryState;
import solidity_safety.ethereum.statemachine.states.FunctionSuccessState;

public class ConstructorFunction extends Function {

    public ConstructorFunction(SmartContract parent, ArrayList<Variable> parameters, ArrayList<String> modifiers,
            ArrayList<String> require_modifiers) {
        super(parent, "constructor", parameters, modifiers, require_modifiers, new ArrayList<Variable>());
    }

    @Override
    public FunctionType getType() {
        return FunctionType.ConstructorFunction;
    }

    @Override
    public void startDefinition() {
        parent.addFunction(this);
        block.addState(StateTransition.newTrue(new ConstructorEntryState(block)));
    }

    @Override
    public void endDefinition() {
        block.addState(StateTransition.newTrue(new FunctionSuccessState(block)));
    }

    @Override
    public String getColour() {
        return "#C047CF";
    }
}
