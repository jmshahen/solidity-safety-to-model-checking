/**
 * Holds our internal representation of an Ethereum Distributed Application (collection of Smart Contracts) 
 */
package solidity_safety.ethereum;
