package solidity_safety.ethereum.statemachine.blocks;

import solidity_safety.ethereum.statemachine.StateTransition;
import solidity_safety.ethereum.statemachine.states.StateI;

public class ElseBlock extends BaseBlock implements BlockI {
    IfBlock ifBlock;
    public ElseBlock(IfBlock ifBlock, StateI startState) {
        super(ifBlock);
        this.ifBlock = ifBlock;
        ifBlock.setElseClause(this);
        addState(StateTransition.newTrue(startState));
    }

    @Override
    public void endBlock() {
        addState(StateTransition.newTrue(ifBlock.end_if_state));
    }
}
