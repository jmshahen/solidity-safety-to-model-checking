package solidity_safety.ethereum.statemachine.blocks;

import solidity_safety.ethereum.statemachine.StateTransition;
import solidity_safety.ethereum.statemachine.states.EndBlockState;
import solidity_safety.ethereum.statemachine.states.RequireState;

public class RequireBlock extends BaseBlock implements BlockI {
    public RequireState require_state = null;
    public EndBlockState end_require_state = null;

    public RequireBlock(BlockI parentBlock, String condition, boolean noRollback) {
        super(parentBlock);
        require_state = new RequireState(this, condition, noRollback);
        end_require_state = new EndBlockState(this);
        addRawState(end_require_state);
    }

    @Override
    public void endBlock() {
        addState(StateTransition.newTrue(end_require_state));
        super.endBlock();
    }
}
