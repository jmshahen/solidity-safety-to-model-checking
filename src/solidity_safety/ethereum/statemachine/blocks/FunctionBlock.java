package solidity_safety.ethereum.statemachine.blocks;

import solidity_safety.ethereum.function.Function;

public class FunctionBlock extends BaseBlock implements BlockI {

    public FunctionBlock(Function parent) {
        super(parent);
    }

}
