package solidity_safety.ethereum.statemachine.blocks;

import java.util.ArrayList;

import solidity_safety.ethereum.function.Function;
import solidity_safety.ethereum.statemachine.StateTransition;
import solidity_safety.ethereum.statemachine.states.EmptyState;
import solidity_safety.ethereum.statemachine.states.StateI;

public class BaseBlock implements BlockI, Cloneable {
    public Function parent_function = null;
    public BlockI parent_block = null;
    public StateI start_state = null;
    public StateI last_state = null;

    public ArrayList<StateI> states = new ArrayList<>();
    public ArrayList<BlockI> children = new ArrayList<>();

    public BaseBlock(Function parentFunction) {
        setParent(parentFunction);
        setBufferState();
    }
    public BaseBlock(BlockI parentBlock) {
        setParentBlock(parentBlock);
        setParent(parentBlock.getParent());
        setBufferState();
    }

    private void setBufferState() {
        addState(StateTransition.newTrue(new EmptyState(this)));
    }

    @Override
    public Function getParent() {
        return parent_function;
    }

    @Override
    public void setParent(Function parent) {
        this.parent_function = parent;
    }

    @Override
    public BlockI getParentBlock() {
        return parent_block;
    }

    @Override
    public void setParentBlock(BlockI parent) {
        parent_block = parent;
    }

    @Override
    public void addState(StateTransition state) {
        if (start_state == null) {
            start_state = state.nextState;
            last_state = state.nextState;
        } else {
            last_state.addState(state);
            last_state = state.nextState;
        }

        states.add(state.nextState);
        if (parent_block != null) {
            parent_block.addRawState(state.nextState);
        }
    }

    @Override
    public void addRawState(StateI s) {
        states.add(s);
        if (parent_block != null) {
            parent_block.addRawState(s);
        }
    }
    @Override
    public void endBlock() {
        if (parent_block != null) {
            parent_block.setLastState(getLastState());
        }
    }
    @Override
    public ArrayList<StateI> getAllStates() {
        return states;
    }

    @Override
    public void updateAllStates(ArrayList<StateI> newStates) {
        states = newStates;
    }

    @Override
    public StateI getStartState() {
        return start_state;
    }

    @Override
    public StateI getLastState() {
        return last_state;
    }

    @Override
    public void setLastState(StateI state) {
        last_state = state;
        if (start_state == null) {
            start_state = state;
        }
    }
    @Override
    public void setStartState(StateI state) {
        start_state = state;
        if (last_state == null) {
            last_state = state;
        }
    }
    @Override
    public void addNewBlock(BlockI new_block) {
        if (new_block.getStartState() != null) {
            addState(StateTransition.newTrue(new_block.getStartState()));
        }
        children.add(new_block);
    }

    @Override
    public ArrayList<BlockI> getChildBlocks() {
        return children;
    }

    @Override
    public int numberOfStates() {
        int count = states.size();
        for (BlockI b : children) {
            count += b.numberOfStates();
        }
        return count;
    }

    @Override
    public int getNextNumber() {
        if (parent_block != null) return parent_block.getNextNumber();
        return states.size();
    }

    @Override
    public BlockI clone() throws CloneNotSupportedException {
        return (BlockI) super.clone();
    }

}
