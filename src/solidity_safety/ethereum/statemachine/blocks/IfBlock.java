package solidity_safety.ethereum.statemachine.blocks;

import solidity_safety.ethereum.statemachine.StateTransition;
import solidity_safety.ethereum.statemachine.states.EndBlockState;
import solidity_safety.ethereum.statemachine.states.IfState;

public class IfBlock extends BaseBlock implements BlockI {
    public IfState if_state = null;
    public ElseBlock else_clause = null;
    public EndBlockState end_if_state = null;

    public IfBlock(BlockI parentBlock, String condition) {
        super(parentBlock);
        if_state = new IfState(this, condition);
        end_if_state = new EndBlockState(this);
        addRawState(end_if_state);
    }

    public ElseBlock getElseBlock() {
        if (else_clause == null) else_clause = new ElseBlock(this, if_state.falseState);
        return else_clause;
    }

    public void setElseClause(ElseBlock elseBlock) {
        else_clause = elseBlock;
    }

    @Override
    public void endBlock() {
        addState(StateTransition.newTrue(end_if_state));
        super.endBlock();
    }
}
