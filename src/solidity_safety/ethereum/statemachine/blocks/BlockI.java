package solidity_safety.ethereum.statemachine.blocks;

import java.util.ArrayList;

import solidity_safety.ethereum.function.Function;
import solidity_safety.ethereum.statemachine.StateTransition;
import solidity_safety.ethereum.statemachine.states.StateI;

public interface BlockI {
    public Function getParent();
    public void setParent(Function parent);

    public BlockI getParentBlock();
    public void setParentBlock(BlockI parent);

    public void addState(StateTransition state);
    /** This is an internal function which just allows for adding a state to a block */
    public void addRawState(StateI s);
    /** This is called once the block is finished being read in. */
    public void endBlock();
    public ArrayList<StateI> getAllStates();

    public void updateAllStates(ArrayList<StateI> newStates);

    public StateI getStartState();
    public StateI getLastState();
    public void setLastState(StateI state);
    public void setStartState(StateI state);

    /** Only call this function after <code>new_block</code> has 1 state added to it */
    public void addNewBlock(BlockI new_block);
    public ArrayList<BlockI> getChildBlocks();

    public int numberOfStates();
    public int getNextNumber();

    public BlockI clone() throws CloneNotSupportedException;
}
