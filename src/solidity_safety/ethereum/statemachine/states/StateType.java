package solidity_safety.ethereum.statemachine.states;

/**
 * This class allows for checking which class a state is without using instanceof (which is a code smell)
 */
public enum StateType {
    BaseState("BaseState", "BS"),
    /** */
    RequireState("RequireState", "RS"),
    /** */
    QueryState("QueryState", "QS"),
    /** */
    IfState("IfState", "IF"),
    /** */
    EmptyState("EmptyState", "ES"),
    /** */
    FunctionModifierSymbolState("FunctionModifierSymbolState", "FMS"),
    /** */
    FunctionSuccessState("FunctionSuccessState", "FSS"),
    /** */
    FunctionFailedEndState("FunctionFailedEndState", "FFS"),
    /** */
    DataChangeState("DataChangeState", "DCS"),
    /** */
    FunctionEntryState("FunctionEntryState", "FES"),
    /** */
    CommitState("CommitState", "CS"),
    /** */
    ConstructorEntryState("ConstructorEntryState", "CES"),
    /** */
    EndBlockState("EndBlockState", "EDS"),
    /** */
    ModifierEntryState("ModifierEntryState", "MES"),
    /** */
    ParentlessState("ParentlessState", "PS"),
    /** */
    FailedRequiredState("FailedRequiredState", "FRS");

    private String _longName;
    private String _shortName;

    private StateType(String longName, String shortName) {
        _longName = longName;
        _shortName = shortName;
    }

    public String getLongName() {
        return _longName;
    }
    public String getShortName() {
        return _shortName;
    }
}