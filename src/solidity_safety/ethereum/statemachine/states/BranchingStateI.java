package solidity_safety.ethereum.statemachine.states;

/**
 * Interface to accommdate the following statements:
 * <ul>
 * <li>If
 * <li>Require
 * <li>For
 * <li>While
 * <ul>
 * @author Jonathan Shahen
 *
 */
public interface BranchingStateI extends StateI {
    /** Returns the condition string for the branching statement. */
    public String getCondition();

    /** Usually conditions are stored in a boolean variable, this will give you the name of that extra variable if required. */
    public String getSMVExtraVariableName(boolean verbose);

    /** If an extra variable is required, then this will return the SMV variable declaration. */
    public String getSMVExtraVariableDeclaration(boolean verbose);
}
