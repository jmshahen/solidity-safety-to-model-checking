package solidity_safety.ethereum.statemachine.states;

import solidity_safety.ethereum.statemachine.blocks.BlockI;

public class ConstructorEntryState extends FunctionEntryState implements StateI {
    public ConstructorEntryState() {
        super();
    }
    public ConstructorEntryState(BlockI parent) {
        super(parent);
    }

    @Override
    public StateType getStateType() {
        return StateType.ConstructorEntryState;
    }
    @Override
    public int getGasCost() {
        return 1;
    }
}
