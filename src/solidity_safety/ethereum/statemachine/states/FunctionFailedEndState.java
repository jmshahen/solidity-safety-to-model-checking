package solidity_safety.ethereum.statemachine.states;

import solidity_safety.ethereum.statemachine.StateTransition;
import solidity_safety.ethereum.statemachine.blocks.BlockI;

public class FunctionFailedEndState extends BaseState implements StateI {
    public FunctionFailedEndState() {
        super();
    }
    public FunctionFailedEndState(BlockI parent) {
        super(parent);
        this.addState(StateTransition.newTrue(parent.getParent().parent.parent.loopState));
    }

    @Override
    public StateType getStateType() {
        return StateType.FunctionFailedEndState;
    }
    @Override
    public String getDOTAttributes() {
        if (parent != null && parent.getParent() != null) return "shape=cds,orientation=180,fillcolor=\"#FF3300\"";
        return "shape=cds,orientation=180,fillcolor=\"#FF3300\"";
    }
}
