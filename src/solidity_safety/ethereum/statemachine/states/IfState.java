package solidity_safety.ethereum.statemachine.states;

import solidity_safety.ethereum.statemachine.Condition;
import solidity_safety.ethereum.statemachine.StateTransition;
import solidity_safety.ethereum.statemachine.blocks.BlockI;

public class IfState extends BaseState implements BranchingStateI {
    public StateI trueState;
    public StateI falseState;
    public String condition;

    /**The parent should be the IFBlock*/
    public IfState(BlockI parent, String condition) {
        super(parent);
        parent.getParent().parent.ifConditions.add(this);
        this.condition = Condition.solidityToSMVCondition(BaseState.removeDotPartInExpression(condition));
        // ORDERING MATTERS HERE
        parent.addState(StateTransition.newTrue(this));

        trueState = new EmptyState(parent);
        parent.addState(new StateTransition(trueState, new Condition(this.condition)));

        falseState = new EmptyState(parent);
        parent.addRawState(falseState);
        this.addState(new StateTransition(falseState, Condition.negateCondition(this.condition)));
    }
    @Override
    public StateType getStateType() {
        return StateType.IfState;
    }

    @Override
    public String getDOTAttributes() {
        String style = "shape=\"diamond\",penwidth=2";
        if (parent != null && parent.getParent() != null)
            return style + ",fillcolor=\"" + parent.getParent().getColour() + "\"";
        return style + ",fillcolor=\"#3F70BA\"";
    }

    @Override
    public String getSMVExtraVariableDeclaration(boolean verbose) {
        return getSMVExtraVariableName(verbose) + " : boolean;";

    }

    @Override
    public String getCondition() {
        return condition;
    }

    @Override
    public String getSMVExtraVariableName(boolean verbose) {
        return "IF_" + getStateName(verbose);
    }
    @Override
    public int getGasCost() {
        return 1;
    }
}
