package solidity_safety.ethereum.statemachine.states;

import solidity_safety.ethereum.statemachine.blocks.BlockI;

public class ParentlessState extends BaseState implements StateI {
    String stateName;
    String shortStateName;

    public ParentlessState(String stateName, String shortStateName) {
        this.stateName = stateName;
        this.shortStateName = shortStateName;
    }

    @Override
    public StateType getStateType() {
        return StateType.ParentlessState;
    }

    @Override
    public String getDOTAttributes() {
        return "shape=\"component\"";
    }

    @Override
    public String getStateName(boolean verbose) {
        if (verbose) return stateName;
        return shortStateName;
    }

    @Override
    public void setParent(BlockI parent) {}

    @Override
    public BlockI getParent() {
        return null;
    }
}
