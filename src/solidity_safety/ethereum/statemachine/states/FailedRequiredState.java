package solidity_safety.ethereum.statemachine.states;

import solidity_safety.ethereum.statemachine.blocks.BlockI;

public class FailedRequiredState extends BaseState implements StateI {
    public FailedRequiredState() {
        super();
    }
    public FailedRequiredState(BlockI parent) {
        super(parent);
    }

    @Override
    public StateType getStateType() {
        return StateType.FailedRequiredState;
    }
    @Override
    public String getDOTAttributes() {
        return "shape=cds,orientation=180,fillcolor=\"#FF3300\"";
    }
}
