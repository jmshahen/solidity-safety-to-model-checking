package solidity_safety.ethereum.statemachine.states;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import solidity_safety.ethereum.data.Variable;
import solidity_safety.ethereum.function.Function;
import solidity_safety.ethereum.function.FunctionType;
import solidity_safety.ethereum.statemachine.StateTransition;
import solidity_safety.ethereum.statemachine.blocks.BlockI;

public class BaseState implements StateI, Cloneable {
    int number;
    BlockI parent;
    ArrayList<StateTransition> transitions = new ArrayList<StateTransition>();

    public BaseState() {}

    public BaseState(BlockI parent) {
        this();
        this.parent = parent;
        number = parent.getNextNumber();
    }
    @Override
    public String getStateName(boolean verbose) {
        String s = parent.getParent().getStateName(verbose);
        if (verbose) return s + "_" + getStateTypeString() + "_ln" + number;
        return s + "_" + getShortStateTypeString() + "_ln" + number;
    }

    @Override
    public void setNumber(int stateNumber) {
        number = stateNumber;
    }

    @Override
    public int getNumber() {
        return number;
    }

    @Override
    public void setParent(BlockI parent) {
        this.parent = parent;
    }

    @Override
    public BlockI getParent() {
        return parent;
    }

    @Override
    public Function getParentFunction() {
        if (parent == null) return null;
        return parent.getParent();
    }

    @Override
    public FunctionType getParentFunctionType() {
        Function f = getParentFunction();
        if (f == null) return null;
        return f.getType();
    }

    @Override
    public ArrayList<StateI> getPreviousStates() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ArrayList<StateI> getNextStates() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ArrayList<StateTransition> getStateTransitions() {
        return transitions;
    }

    @Override
    public void updateStateTransitions(ArrayList<StateTransition> stateTransitions) {
        transitions = stateTransitions;
    }

    @Override
    public void addState(StateTransition state) {
        transitions.add(state);
    }

    @Override
    public StateType getStateType() {
        return StateType.BaseState;
    }

    @Override
    public String getStateTypeString() {
        return getStateType().getLongName();
    }

    @Override
    public String getShortStateTypeString() {
        return getStateType().getShortName();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof StateI) { return this.getStateName(true).equals(((StateI) obj).getStateName(true)); }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return getStateName(true).hashCode();
    }
    @Override
    public boolean changesVariable(Variable v) {
        return false;
    }

    @Override
    public String toNewVariableValueSMV(Variable v, boolean verbose, boolean noRollback) {
        return null;
    }
    @Override
    public int getGasCost() {
        return 0;
    }

    @Override
    public String getDOTAttributes() {
        if (parent != null && parent.getParent() != null)
            return "shape=box,style=filled,fillcolor=\"" + parent.getParent().getColour() + "\"";
        return "shape=box,style=rounded";
    }

    public static String cleanUpExpression(String str) {
        if (str != null) {
            if (str.matches("[0-9]+")) return str;
            str = str.replaceAll("([a-zA-Z0-9])([\\+\\-\\*\\/\\=\\&\\|])([a-zA-Z0-9])", "$1 $2 $3");
        }
        return str;
    }

    public static String removeDotPartInExpression(String str) {
        StringBuilder condition = new StringBuilder();
        // TODO Struct queries !!
        Pattern enumConditionRegex = Pattern.compile("(^.*)(=)(.*)(\\.)(.*)");
        Matcher enumConditionRegexMatcher = enumConditionRegex.matcher(str);
        if (enumConditionRegexMatcher.find()) {
            condition.append(enumConditionRegexMatcher.group(1));
            condition.append(enumConditionRegexMatcher.group(2));
            condition.append(enumConditionRegexMatcher.group(5));
            str = condition.toString();
        }
        return str;
    }

    @Override
    public Integer getContractNumber(Integer defaultValue) {
        if (parent != null && parent.getParent() != null && parent.getParent().parent != null) {
            return parent.getParent().parent.number;
        }
        return defaultValue;
    }

    @Override
    public StateI clone() throws CloneNotSupportedException {
        return (StateI) super.clone();
    }
}
