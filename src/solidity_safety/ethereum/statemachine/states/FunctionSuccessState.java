package solidity_safety.ethereum.statemachine.states;

import solidity_safety.ethereum.statemachine.blocks.BlockI;

public class FunctionSuccessState extends BaseState implements StateI {
    public FunctionSuccessState() {
        super();
    }
    public FunctionSuccessState(BlockI parent) {
        super(parent);
    }

    @Override
    public StateType getStateType() {
        return StateType.FunctionSuccessState;
    }

    @Override
    public String getDOTAttributes() {
        if (parent != null && parent.getParent() != null)
            return "shape=cds,orientation=180,fillcolor=\"" + parent.getParent().getColour() + "\"";
        return "shape=cds,orientation=180,fillcolor=\"#78BA3F\"";
    }
}
