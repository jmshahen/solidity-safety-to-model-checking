package solidity_safety.ethereum.statemachine.states;

import solidity_safety.ethereum.statemachine.blocks.BlockI;

public class EmptyState extends BaseState implements StateI {
    public EmptyState() {
        super();
    }
    public EmptyState(BlockI parent) {
        super(parent);
    }

    @Override
    public StateType getStateType() {
        return StateType.EmptyState;
    }

    @Override
    public String getDOTAttributes() {
        if (parent != null && parent.getParent() != null)
            return "shape=box,fillcolor=\"" + parent.getParent().getColour() + "\"";
        return "shape=box";
    }
}
