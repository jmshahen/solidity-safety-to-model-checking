package solidity_safety.ethereum.statemachine.states;

import solidity_safety.ethereum.data.Variable;
import solidity_safety.ethereum.statemachine.blocks.BlockI;

public class FunctionEntryState extends BaseState implements StateI {
    public FunctionEntryState() {
        super();
    }
    public FunctionEntryState(BlockI parent) {
        super(parent);
    }

    @Override
    public boolean changesVariable(Variable v) {
        return parent.getParent().parameters.values().contains(v);
    }

    @Override
    public String toNewVariableValueSMV(Variable v, boolean verbose, boolean noRollback) {
        return v.value.getSMVVariableDeclaration(verbose);
    }

    @Override
    public StateType getStateType() {
        return StateType.FunctionEntryState;
    }
    @Override
    public String getDOTAttributes() {
        if (parent != null && parent.getParent() != null)
            return "shape=cds,fillcolor=\"" + parent.getParent().getColour() + "\"";
        return "shape=cds";
    }
    @Override
    public int getGasCost() {
        return 1;
    }
}
