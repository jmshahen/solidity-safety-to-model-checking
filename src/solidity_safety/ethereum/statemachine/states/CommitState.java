package solidity_safety.ethereum.statemachine.states;

import solidity_safety.ethereum.statemachine.blocks.BlockI;

public class CommitState extends BaseState implements StateI {
    public CommitState() {
        super();
    }
    public CommitState(BlockI parent) {
        super(parent);
    }

    @Override
    public StateType getStateType() {
        return StateType.CommitState;
    }
    @Override
    public String getDOTAttributes() {
        if (parent != null && parent.getParent() != null)
            return "shape=cds,orientation=180,fillcolor=\"" + parent.getParent().getColour() + "\"";
        return "shape=cds,orientation=180,fillcolor=\"#78BA3F\"";
    }
}
