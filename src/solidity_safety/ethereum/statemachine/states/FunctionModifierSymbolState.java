
package solidity_safety.ethereum.statemachine.states;

import solidity_safety.ethereum.statemachine.blocks.BlockI;

public class FunctionModifierSymbolState extends BaseState implements StateI {
    public FunctionModifierSymbolState() {
        super();
    }

    public FunctionModifierSymbolState(BlockI parent) {
        super(parent);
    }

    @Override
    public StateType getStateType() {
        return StateType.FunctionModifierSymbolState;
    }

    @Override
    public String getDOTAttributes() {
        if (parent != null && parent.getParent() != null)
            return "shape=box,fillcolor=\"" + parent.getParent().getColour() + "\"";
        return "shape=box";
    }
    @Override
    public int getGasCost() {
        return 1;
    }
}
