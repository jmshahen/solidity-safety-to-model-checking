package solidity_safety.ethereum.statemachine.states;

import solidity_safety.constants.SMVConstants;
import solidity_safety.ethereum.data.*;
import solidity_safety.ethereum.query.QueryI;
import solidity_safety.ethereum.query.BoolExprReachabilityQuery;
import solidity_safety.ethereum.statemachine.blocks.BlockI;

/**
 * This state represents the QUERY comment line in the solidity file.
 * This allows for a query to have scope for variables and for it to easily reference a particular state.
 * @author Jonathan Shahen
 *
 */
public class QueryState extends BaseState implements StateI, QueryI {
    public Variable queryVar;
    // public String goalStateCondition;
    public QueryI query;
    public QueryState() {
        super();
    }
    public QueryState(BlockI parent, String goalStateCondition, String expectedResult, Integer lineNumber) {
        super(parent);
        this.query = new BoolExprReachabilityQuery(parent.getParent().parent, goalStateCondition, expectedResult,
                lineNumber);
        queryVar = new Variable(VariableScope.QueryVariable, parent.getParent().identifier, "query",
                "query_" + getStateName(false));

        parent.getParent().parent.addQuery(this);
    }

    @Override
    public String getQueryName(boolean verbose) {
        if (verbose) return getStateTypeString() + "_" + query.getQueryNumber();
        return getShortStateTypeString() + query.getQueryNumber();
    }
    public String toSMV(boolean verbose) {
        return "CTLSPEC NAME " + getQueryName(verbose) + " := AG !(" + SMVConstants.QueryName + " = "
                + getQueryName(verbose) + " & " + queryVar.getScopedName(verbose) + " = "
                + QueryStateDataValues.STATE_REACHED_CONFIRMED.val + ");";
    }

    @Override
    public StateType getStateType() {
        return StateType.QueryState;
    }

    @Override
    public String getDOTAttributes() {
        if (parent != null && parent.getParent() != null)
            return "shape=box,peripheries=2,fillcolor=\"" + parent.getParent().getColour() + "\"";
        return "shape=box,peripheries=2,fillcolor=\"#FF070D\"";
    }
    public String getExpectedResult() {
        return query.getExpectedResult();
    }
    @Override
    public void setOriginalQueryString(String line, Integer lineNumber) {
        query.setOriginalQueryString(line, lineNumber);
    }
    @Override
    public String getOriginalQueryString(String returnIfNotSet) {
        return query.getOriginalQueryString(returnIfNotSet);
    }
    @Override
    public String getCondition() {
        return query.getCondition();
    }
    @Override
    public void setCondition(String condition) {
        query.setCondition(condition);
    }
    @Override
    public int getQueryNumber() {
        return query.getQueryNumber();
    }
}
