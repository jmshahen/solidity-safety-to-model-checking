package solidity_safety.ethereum.statemachine.states;

import java.util.Arrays;

import org.apache.commons.collections4.IterableUtils;

import solidity_safety.constants.SMVConstants;
import solidity_safety.ethereum.data.Variable;
import solidity_safety.ethereum.data.VariableScope;
import solidity_safety.ethereum.statemachine.blocks.BlockI;

public class DataChangeState extends BaseState implements StateI {
    public Variable updateVar = null;
    public String equalTo = null;
    public String equalSign = "=";

    public DataChangeState() {
        super();
    }

    public DataChangeState(BlockI parent) {
        super(parent);
    }

    public DataChangeState(BlockI parent, Variable var, String equalSign, String equalTo) {
        super(parent);
        this.updateVar = var;
        this.equalTo = cleanUpExpression(equalTo);
        this.equalSign = equalSign;
    }

    public void setUpdateVar(Variable v) {
        this.updateVar = v;
    }

    @Override
    public boolean changesVariable(Variable v) {
        if (updateVar == null) return false;
        return updateVar.equals(v);
    }

    @Override
    /**
     * Ignores the parameter v
     * @param v IGNORED
     */
    public String toNewVariableValueSMV(Variable v, boolean verbose, boolean noRollback) {
        // ToDo: also for other types of variables and more complex equalTo expr.
        if (equalTo != null) {
            for (Variable var : IterableUtils.chainedIterable(parent.getParent().parent.state_variables.values(),
                    parent.getParent().internal_variables.values(), parent.getParent().parameters.values())) {
                if (Arrays.asList(equalTo.split("[ <>+!=*/-]")).contains(var.identifier)) {
                    if (!noRollback) {
                        this.equalTo = equalTo.replaceAll("\\b" + var.identifier + "\\b",
                                (var.scope == VariableScope.StateVariable)
                                        ? SMVConstants.TempVariable + var.getScopedName(verbose)
                                        : var.getScopedName(verbose));
                    } else {
                        this.equalTo = equalTo.replaceAll("\\b" + var.identifier + "\\b", var.getScopedName(verbose));
                    }
                }
            }
            this.equalTo = equalTo.replaceAll("\\b" + "true" + "\\b", "TRUE");
            this.equalTo = equalTo.replaceAll("\\b" + "false" + "\\b", "FALSE");
        }
        switch (equalSign) {
            case "+=" :
                return updateVar.value.limitValueSMV(updateVar.getScopedName(verbose) + " + " + equalTo);
            case "-=" :
                return updateVar.value.limitValueSMV(updateVar.getScopedName(verbose) + " - " + equalTo);
            case "*=" :
                return updateVar.value.limitValueSMV(updateVar.getScopedName(verbose) + " * " + equalTo);
            case "/=" :
                return updateVar.value.limitValueSMV(updateVar.getScopedName(verbose) + " / " + equalTo);
            case "=" :
            default :
                return updateVar.value.limitValueSMV(equalTo);
        }
    }

    @Override
    public StateType getStateType() {
        return StateType.DataChangeState;
    }
    @Override
    public int getGasCost() {
        return 1;
    }
}
