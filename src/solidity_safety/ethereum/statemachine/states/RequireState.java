package solidity_safety.ethereum.statemachine.states;

import solidity_safety.ethereum.function.FunctionType;
import solidity_safety.ethereum.statemachine.Condition;
import solidity_safety.ethereum.statemachine.StateTransition;
import solidity_safety.ethereum.statemachine.blocks.BlockI;

public class RequireState extends BaseState implements BranchingStateI {
    /** Which state to go to next if the require succeeds. */
    public StateI trueState;
    /** Which state to go to next if the require fails. */
    public StateI falseState;
    /** The condition to determine if the require statement succeeds or fails */
    public String condition;

    /**
     * The parent should be the IFBlock
     */
    public RequireState(BlockI parent, String condition, boolean noRollback) {
        super(parent);
        parent.getParent().parent.requireConditions.add(this);
        this.condition = Condition.solidityToSMVCondition(BaseState.removeDotPartInExpression(condition));
        parent.addState(StateTransition.newTrue(this));

        trueState = new EmptyState(parent);
        parent.addState(new StateTransition(trueState, new Condition(this.condition)));

        if (parent.getParent().getType() == FunctionType.ConstructorFunction) {
            falseState = parent.getParent().getParent().getParent().errorState;
        } else {
            if (noRollback == false) {
                falseState = new FunctionFailedEndState(parent);
            } else {
                falseState = new FailedRequiredState(parent);
            }
        }
        parent.addRawState(falseState);
        this.addState(new StateTransition(falseState, Condition.negateCondition(this.condition)));

    }

    @Override
    public StateType getStateType() {
        return StateType.RequireState;
    }

    @Override
    public String getDOTAttributes() {
        String style = "shape=house,penwidth=2";
        if (parent != null && parent.getParent() != null)
            return style + ",fillcolor=\"" + parent.getParent().getColour() + "\"";
        return style + ",fillcolor=\"#78BA3F\"";
    }

    @Override
    public String getSMVExtraVariableDeclaration(boolean verbose) {
        return getSMVExtraVariableName(verbose) + " : boolean;";

    }
    @Override
    public String getSMVExtraVariableName(boolean verbose) {
        if (verbose) return "REQUIRE_" + getStateName(verbose);
        return "RS_" + getStateName(verbose);
    }

    @Override
    public String getCondition() {
        return condition;
    }
    @Override
    public int getGasCost() {
        return 1;
    }
}
