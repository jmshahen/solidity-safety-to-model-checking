package solidity_safety.ethereum.statemachine.states;

import solidity_safety.ethereum.statemachine.blocks.BlockI;

public class ModifierEntryState extends FunctionEntryState implements StateI {
    public ModifierEntryState() {
        super();
    }
    public ModifierEntryState(BlockI parent) {
        super(parent);
    }

    @Override
    public StateType getStateType() {
        return StateType.ModifierEntryState;
    }
    @Override
    public int getGasCost() {
        return 1;
    }
}
