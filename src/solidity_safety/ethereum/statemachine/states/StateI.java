package solidity_safety.ethereum.statemachine.states;

import java.util.ArrayList;

import solidity_safety.ethereum.data.Variable;
import solidity_safety.ethereum.function.Function;
import solidity_safety.ethereum.function.FunctionType;
import solidity_safety.ethereum.statemachine.StateTransition;
import solidity_safety.ethereum.statemachine.blocks.BlockI;

public interface StateI {
    public StateType getStateType();
    public String getStateTypeString();
    public String getShortStateTypeString();

    public String getStateName(boolean verbose);

    public void setParent(BlockI parent);
    public BlockI getParent();
    /** Calls getParent().getParent() */
    public Function getParentFunction();
    /** Calls getParentFunction().getType() */
    public FunctionType getParentFunctionType();

    public ArrayList<StateI> getPreviousStates();
    public ArrayList<StateI> getNextStates();
    public ArrayList<StateTransition> getStateTransitions();
    public void updateStateTransitions(ArrayList<StateTransition> stateTransitions);
    public void addState(StateTransition state);

    public boolean changesVariable(Variable v);
    public String toNewVariableValueSMV(Variable v, boolean verbose, boolean noRollback);

    /** Returns the Estimated Gas Cost for this state. Can be positive, negative, or zero. */
    public int getGasCost();

    public void setNumber(int stateNumber);
    public int getNumber();

    /**
     * Returns the shape that should be used in the DOT diagram.
     * <br>
     * Available options: box,polygon,ellipse,oval,circle,point,egg,triangle,
     * plaintext,plain,diamond,trapezium,parallelogram,house,pentagon,hexagon,
     * septagon,octagon,doublecircle,doubleoctagon,tripleoctagon,invtriangle,invtrapezium,invhouse,
     * Mdiamond,Msquare,Mcircle,rect,rectangle,square,star,none,underline,cylinder,note,tab,
     * folder,box3d,component,promoter,cds,terminator,utr,primersite,restrictionsite,
     * fivepoverhang,threepoverhang,noverhang,assembly,signature,insulator,ribosite,
     * rnastab,proteasesite,proteinstab,rpromoter,rarrow,larrow,lpromoter.
     * <br>
     * Visual representation here: <a href="https://www.graphviz.org/doc/info/shapes.html">https://www.graphviz.org/doc/info/shapes.html</a>
     * @return
     */
    public String getDOTAttributes();
    public Integer getContractNumber(Integer defaultValue);

    public StateI clone() throws CloneNotSupportedException;
}
