package solidity_safety.ethereum.statemachine;

import solidity_safety.ethereum.statemachine.states.StateI;

public class StateTransition implements Cloneable {
    public StateI nextState;
    public Condition condition;

    public StateTransition() {}
    public StateTransition(StateI nextState, Condition condition) {
        this.nextState = nextState;
        this.condition = condition;
    }

    /**
     * Returns a new {@link StateTransition} where the condition is {@link Condition#getTrueCondition()}
     * @param nextState
     * @return
     */
    public static StateTransition newTrue(StateI nextState) {
        return new StateTransition(nextState, Condition.getTrueCondition());
    }

    @Override
    public StateTransition clone() throws CloneNotSupportedException {
        return (StateTransition) super.clone();
    }
}
