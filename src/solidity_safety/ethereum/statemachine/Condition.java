package solidity_safety.ethereum.statemachine;

import solidity_safety.constants.SMVConstants;
import solidity_safety.ethereum.statemachine.states.BranchingStateI;
import solidity_safety.ethereum.statemachine.states.StateI;

public class Condition {
    private boolean isTrueCondition = false;
    private boolean negativeCondition = false;
    public String conditionStr;

    public static Condition getTrueCondition() {
        Condition c = new Condition("TRUE");
        c.isTrueCondition = true;
        return c;
    }
    public static Condition negateCondition(String condition) {
        Condition c = new Condition(condition);
        c.negativeCondition = true;
        return c;
    }

    public Condition(String condition) {
        conditionStr = condition;
    }

    public boolean isTrueCondition() {
        return isTrueCondition;
    }

    public String getReadableString() {
        StringBuilder sb = new StringBuilder();
        if (negativeCondition) {
            sb.append("!(").append(conditionStr).append(")");
        } else {
            sb.append(conditionStr);
        }
        return sb.toString();
    }

    public String getSMVString() {
        StringBuilder sb = new StringBuilder();

        return sb.toString();
    }

    @Override
    public String toString() {
        return getReadableString();
    }

    public String toSMV(StateI prevState, StateI nextState, int contractNumber, boolean verbose) {
        if (isTrueCondition()) {
            return SMVConstants.State + " = " + prevState.getStateName(verbose) + " : "
                    + nextState.getStateName(verbose) + ";\n";
        }
        if (prevState instanceof BranchingStateI) {
            if (negativeCondition) {
                return SMVConstants.State + " = " + prevState.getStateName(verbose) + " & !contract" + contractNumber
                        + "." + ((BranchingStateI) prevState).getSMVExtraVariableName(verbose) + " : "
                        + nextState.getStateName(verbose) + ";\n";
            } else {
                return SMVConstants.State + " = " + prevState.getStateName(verbose) + " & contract" + contractNumber
                        + "." + ((BranchingStateI) prevState).getSMVExtraVariableName(verbose) + " : "
                        + nextState.getStateName(verbose) + ";\n";
            }
        }

        // TODO: might need to convert the conditionStr to be compliant to NuSMV
        return SMVConstants.State + " = " + prevState.getStateName(verbose) + " & " + conditionStr + " : "
                + nextState.getStateName(verbose) + ";\n";
    }
    public static String solidityToSMVCondition(String condition) {
        return condition.replace("==", " = ");
    }
}
