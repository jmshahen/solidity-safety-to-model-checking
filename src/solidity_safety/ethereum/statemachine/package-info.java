/**
 * Contains all that is required to setup an internal Finite State Machine.
 * @see solidity_safety.ethereum.statemachine.states.StateI
 * @see solidity_safety.ethereum.statemachine.StateTransition
 */
package solidity_safety.ethereum.statemachine;
