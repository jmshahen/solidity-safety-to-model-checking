package solidity_safety.ethereum;

import java.io.File;
import java.util.*;
import java.util.logging.Logger;

import org.apache.commons.collections4.IterableUtils;

import solidity_safety.constants.SMVConstants;
import solidity_safety.ethereum.data.*;
import solidity_safety.ethereum.function.*;
import solidity_safety.ethereum.query.QueryI;
import solidity_safety.ethereum.statemachine.StateTransition;
import solidity_safety.ethereum.statemachine.states.*;

public class SmartContract {
    public static Logger log = Logger.getLogger("shahen");
    public static String tabSpace = "    ";

    public DApp parent;
    /**
     * A numeric identifier of the SmartContract;
     * usually this number is just incremented for each new smart contract in a file.
     */
    public int number;
    public String identifier;
    public File file;
    public HashMap<String, Variable> state_variables = new HashMap<>();
    public HashMap<String, Function> functions = new HashMap<>();
    public HashMap<String, ModifierFunction> modifiers = new HashMap<>();
    public static HashMap<String, ArrayList<String>> enums = new HashMap<>();
    public ConstructorFunction constructor = null;
    public ArrayList<QueryState> stateQueries = new ArrayList<>();
    public ArrayList<QueryI> contractQueries = new ArrayList<>();
    public ArrayList<BranchingStateI> ifConditions = new ArrayList<>();
    public ArrayList<BranchingStateI> requireConditions = new ArrayList<>();

    public SmartContract(DApp parent) {
        this.parent = parent;
        parent.addContract(this);
    }

    public void addStateVariable(String typeName, String identifier, String expression) throws InvalidValueForDateType {
        Variable v = new Variable(VariableScope.StateVariable, "", typeName, identifier);

        if (expression != null) {
            v.setValue(expression);
        }

        state_variables.put(v.identifier, v);
    }

    public void addUserDefinedStateVariable(String typeName, String identifier, String expression,
            String user_defined_type) throws InvalidValueForDateType {
        if (user_defined_type.equals("enum")) {
            Variable v = new Variable(VariableScope.StateVariable, "", typeName, identifier);
            if (expression != null) {
                v.setValue(expression);
            }
            state_variables.put(v.identifier, v);
        }
    }

    /**
     * Add the function BEFORE adding states to the function
     */
    public void addFunction(Function func) {
        if (func.getType() == FunctionType.ConstructorFunction) {
            if (constructor == null) {
                constructor = (ConstructorFunction) func;
                func.number = 0;
            } else {
                log.severe("2 or more constructors found (not supported). Ignoring other constructor:\n" + func);
            }
        } else {
            functions.put(func.identifier, func);
            func.number = functions.size(); // make sure this cannot be 0
        }
    }

    public void addModifier(ModifierFunction modifier) {
        modifiers.put(modifier.identifier, modifier);
        modifier.number = modifiers.size();
    }

    public void addQuery(QueryState queryState) {
        stateQueries.add(queryState);
    }

    public void addQuery(QueryI query) {
        contractQueries.add(query);
    }

    /** Returns the Parent DApp for this smart contract. */
    public DApp getParent() {
        return parent;
    }

    public String toSolidityString() {
        StringBuilder sb = new StringBuilder();

        sb.append("contract ").append(identifier).append(" {\n");

        if (state_variables != null && state_variables.size() > 0) {
            sb.append("\t").append("// ").append(state_variables.size()).append(" State Variable(s)\n");
            for (Variable v : state_variables.values()) {
                sb.append("\t").append(v.toString()).append(";\n");
            }
        } else {
            sb.append("\t").append("// No State Variables\n");
        }
        sb.append("\n");

        if (modifiers != null && modifiers.size() > 0) {
            sb.append("\t").append("// ").append(modifiers.size()).append(" Modifier(s)\n");
            for (Function f : modifiers.values()) {
                sb.append("\n").append(f.getString("\t")).append("\n");
            }
        } else {
            sb.append("\t").append("// No Modifiers\n");
        }
        sb.append("\n");

        if (constructor != null) {
            sb.append("\t").append("// Constructor\n");
            sb.append(constructor.getString("\t")).append("\n");
        } else {
            sb.append("\t").append("// No Constructor\n");
        }
        sb.append("\n");

        if (functions != null && functions.size() > 0) {
            sb.append("\t").append("// ").append(functions.size()).append(" Function(s)\n");
            for (Function f : functions.values()) {
                sb.append("\n").append(f.getString("\t")).append("\n");
            }
        } else {
            sb.append("\t").append("// No Functions\n");
        }

        sb.append("}");

        return sb.toString();
    }

    /**
     * This returns a NuSMV styled SMV module of this Smart Contract.
     * <p>
     * <ul>
     * <li>The name of the module is: "contract_" + name
     * <li>
     * </ul>
     */
    public String toSMVModule(boolean verbose, boolean noRollback) {
        StringBuilder sb = new StringBuilder();

        sb.append("MODULE contract_").append(identifier).append("(").append(SMVConstants.State).append(",")
                .append(SMVConstants.QueryName).append(")\n");

        // COMMENTS
        if (verbose) {
            sb.append("-- Smart Contract \"").append(identifier).append("\" was generated from file: ");
            if (file != null) sb.append("\"").append(file.getAbsolutePath()).append("\"\n");
            else sb.append("No file provided\n");
            sb.append("-- Contract Number: ").append(number).append("\n");
        }

        // DEFINE Section
        sb.append("DEFINE\n");

        // QUERIES
        for (QueryI q : IterableUtils.chainedIterable(contractQueries, stateQueries)) {
            String orig_condition = q.getCondition();
            String condition = q.getCondition();
            for (Variable internalVariable : state_variables.values()) {
                if (Arrays.asList(condition.split(SMVConstants.SplitVariables)).contains(internalVariable.identifier)) {
                    condition = condition.replaceAll("\\b" + internalVariable.identifier + "\\b",
                            internalVariable.getScopedName(verbose));
                }
            }
            q.setCondition(condition);
            sb.append(q.toSMV(verbose)).append("\n");
            q.setCondition(orig_condition);
        }

        // VAR Section
        sb.append("VAR\n");
        for (QueryState qs : stateQueries) {
            sb.append(qs.queryVar.getSMVVariableDeclaration(verbose)).append("\n");
        }

        for (Variable v : state_variables.values()) {
            sb.append(v.getSMVVariableDeclaration(verbose)).append("\n");
            if (!noRollback)
                sb.append(SMVConstants.TempVariable).append(v.getSMVVariableDeclaration(verbose)).append("\n");
        }
        for (BranchingStateI bs : IterableUtils.chainedIterable(ifConditions, requireConditions)) {
            if (!(bs.getParentFunctionType() == FunctionType.ModifierFunction))
                sb.append(bs.getSMVExtraVariableDeclaration(verbose)).append("\n");
        }

        for (Function f : functions.values()) {
            if (f.internal_variables.size() + f.parameters.size() > 0) {
                if (verbose) sb.append("-- Function: ").append(f.getFunctionDefinition(null)).append("\n");
                for (Variable v : IterableUtils.chainedIterable(f.internal_variables.values(), f.parameters.values())) {
                    if (v.scope == VariableScope.ParameterVariable) {
                        v.scopeName = f.identifier;
                        v.updateScopedIdentifier();
                    }
                    sb.append(v.getSMVVariableDeclaration(verbose)).append("\n");
                }
            }
        }

        if (constructor != null) {
            if (constructor.internal_variables.size() + constructor.parameters.size() > 0) {
                if (verbose) sb.append("-- Constructor: ").append(constructor.getFunctionDefinition(null)).append("\n");
                for (Variable v : IterableUtils.chainedIterable(constructor.internal_variables.values(),
                        constructor.parameters.values())) {
                    sb.append(v.getSMVVariableDeclaration(verbose)).append("\n");
                }
            }
        }

        // ASSIGN Section
        sb.append("ASSIGN\n");

        // INIT
        for (BranchingStateI bs : IterableUtils.chainedIterable(ifConditions, requireConditions)) {
            if (!(bs.getParentFunctionType() == FunctionType.ModifierFunction))
                sb.append("init(").append(bs.getSMVExtraVariableName(verbose)).append(") :=  FALSE;\n");
        }
        for (QueryState qs : stateQueries) {
            sb.append("init(").append(qs.queryVar.getScopedName(verbose)).append(") := ")
                    .append(QueryStateDataValues.STATE_NOT_REACHED.val).append(";\n");
        }
        for (Variable v : state_variables.values()) {
            if (!noRollback) {
                sb.append("init(").append(SMVConstants.TempVariable).append(v.getScopedName(verbose)).append(") := ")
                        .append(v.value.getValue()).append(";\n");
            }
            sb.append("init(").append(v.getScopedName(verbose)).append(") := ").append(v.value.getValue())
                    .append(";\n");
        }
        for (Function f : functions.values()) {
            if (f.internal_variables.size() > 0) {
                if (verbose) sb.append("-- Function: ").append(f.getFunctionDefinition(null)).append("\n");
                for (Variable v : f.internal_variables.values()) {
                    sb.append("init(");
                    sb.append(v.getScopedName(verbose)).append(") := ").append(v.value.getValue()).append(";\n");
                }
            }
        }

        if (constructor != null) {
            if (constructor.internal_variables.size() > 0) {
                if (verbose) sb.append("-- Constructor: ").append(constructor.getFunctionDefinition(null)).append("\n");
                for (Variable v : constructor.internal_variables.values()) {
                    sb.append("init(");
                    sb.append(v.getScopedName(verbose)).append(") := ").append(v.value.getValue()).append(";\n");
                }
            }
        }

        // NEXT
        for (BranchingStateI bs : IterableUtils.chainedIterable(ifConditions, requireConditions)) {
            if (!(bs.getParentFunctionType() == FunctionType.ModifierFunction))
                nextBranchingStateCondition(verbose, sb, bs, noRollback);
        }

        for (QueryState qs : stateQueries) {
            nextQueryStateCondition(verbose, sb, qs);
        }
        for (Variable v : state_variables.values()) {
            if (!noRollback) {
                sb.append("next(").append(SMVConstants.TempVariable).append(v.getScopedName(verbose))
                        .append(") := case\n");
            } else {
                sb.append("next(").append(v.getScopedName(verbose)).append(") := case\n");
            }
            if (constructor != null) addVariableNextValue(sb, v, constructor, verbose, noRollback);
            for (Function f : functions.values()) {
                addVariableNextValue(sb, v, f, verbose, noRollback);
            }
            if (!noRollback) {
                sb.append(tabSpace).append("TRUE : ").append(SMVConstants.TempVariable).append(v.getScopedName(verbose))
                        .append(";\nesac;\n");
            } else {
                sb.append(tabSpace).append("TRUE : ").append(v.getScopedName(verbose)).append(";\nesac;\n");
            }
        }

        if (!noRollback) {
            for (Variable v : state_variables.values()) {
                sb.append("next(").append(v.getScopedName(verbose)).append(") := case\n");
                if (constructor != null) commitVariableNextValue(sb, v, constructor, verbose, noRollback);
                for (Function f : functions.values()) {
                    commitVariableNextValue(sb, v, f, verbose, noRollback);
                }
                sb.append(tabSpace).append("TRUE : ").append(v.getScopedName(verbose)).append(";\nesac;\n");
            }
        }

        for (Function f : functions.values()) {
            if (f.internal_variables.size() + f.parameters.size() > 0) {
                if (verbose) sb.append("-- Function: ").append(f.getFunctionDefinition(null)).append("\n");
                for (Variable v : IterableUtils.chainedIterable(f.internal_variables.values(), f.parameters.values())) {
                    sb.append("next(");
                    sb.append(v.getScopedName(verbose)).append(") := case\n");
                    addVariableNextValue(sb, v, f, verbose, noRollback);
                    sb.append(tabSpace).append("TRUE : ").append(v.getScopedName(verbose)).append(";\nesac;\n");
                }
            }
        }

        if (constructor != null) {
            if (constructor.internal_variables.size() + constructor.parameters.size() > 0) {
                if (verbose) sb.append("-- Constructor: ").append(constructor.getFunctionDefinition(null)).append("\n");
                for (Variable v : IterableUtils.chainedIterable(constructor.internal_variables.values(),
                        constructor.parameters.values())) {
                    sb.append("next(");
                    sb.append(v.getScopedName(verbose)).append(") := case\n");
                    addVariableNextValue(sb, v, constructor, verbose, noRollback);
                    sb.append(tabSpace).append("TRUE : ");
                    sb.append(v.getScopedName(verbose)).append(";\nesac;\n");
                }
            }
        }

        sb.append("\n");

        return sb.toString();
    }

    private void nextBranchingStateCondition(boolean verbose, StringBuilder sb, BranchingStateI bs,
            boolean noRollback) {
        String tempReplacedCondition = bs.getCondition();
        if (tempReplacedCondition != null) {
            for (Variable var : IterableUtils.chainedIterable(state_variables.values(),
                    bs.getParentFunction().internal_variables.values(), bs.getParentFunction().parameters.values())) {
                if (Arrays.asList(tempReplacedCondition.split(SMVConstants.SplitVariables)).contains(var.identifier)) {
                    if (!noRollback) {
                        tempReplacedCondition = tempReplacedCondition.replaceAll("\\b" + var.identifier + "\\b",
                                (var.scope == VariableScope.StateVariable)
                                        ? SMVConstants.TempVariable + var.getScopedName(verbose)
                                        : var.getScopedName(verbose));
                    } else {
                        tempReplacedCondition = tempReplacedCondition.replaceAll("\\b" + var.identifier + "\\b",
                                var.getScopedName(verbose));
                    }
                }
            }
            tempReplacedCondition = tempReplacedCondition.replaceAll("\\b" + "true" + "\\b", "TRUE");
            tempReplacedCondition = tempReplacedCondition.replaceAll("\\b" + "false" + "\\b", "FALSE");
            sb.append("next(").append(bs.getSMVExtraVariableName(verbose)).append(") := ").append(tempReplacedCondition)
                    .append(";\n");
        }
    }

    private void nextQueryStateCondition(boolean verbose, StringBuilder sb, QueryState qs) {
        String varName = qs.queryVar.getScopedName(verbose);
        sb.append("next(").append(varName).append(") := case\n").append(tabSpace);
        sb.append(SMVConstants.State).append(" = ").append(qs.getParent().getStartState().getStateName(verbose));
        sb.append(" : ").append(QueryStateDataValues.STATE_NOT_REACHED.val).append(";\n").append(tabSpace);
        sb.append(SMVConstants.State).append(" = ").append(qs.getStateName(verbose)).append(" : ")
                .append(QueryStateDataValues.STATE_REACHED_UNCONFIRMED.val).append(";\n").append(tabSpace);
        sb.append(SMVConstants.State).append(" = ").append(qs.getParent().getLastState().getStateName(verbose));
        sb.append(" & ").append(varName).append(" = ").append(QueryStateDataValues.STATE_REACHED_UNCONFIRMED.val);

        if (qs.getCondition() != null && !qs.getCondition().isEmpty()) {
            String t_condition = qs.getCondition();
            for (Variable var : IterableUtils.chainedIterable(state_variables.values(),
                    qs.getParentFunction().internal_variables.values(), qs.getParentFunction().parameters.values())) {
                if (Arrays.asList(t_condition.split(SMVConstants.SplitVariables)).contains(var.identifier)) {
                    t_condition = t_condition.replaceAll("\\b" + var.identifier + "\\b",
                            (var.scope == VariableScope.StateVariable)
                                    ? SMVConstants.TempVariable + var.getScopedName(verbose)
                                    : var.getScopedName(verbose));
                }
            }
            sb.append(" & ").append(t_condition);
        }
        sb.append(" : ").append(QueryStateDataValues.STATE_REACHED_CONFIRMED.val).append(";\n").append(tabSpace);
        sb.append("TRUE : ").append(varName).append(";\nesac;\n");
    }

    private void addVariableNextValue(StringBuilder sb, Variable v, Function f, boolean verbose, boolean noRollback) {
        for (StateI s : f.block.getAllStates()) {
            if (s.getStateType() == StateType.FunctionEntryState
                    || s.getStateType() == StateType.FunctionFailedEndState) {
                boolean isChanged = false;
                for (StateI st : f.block.getAllStates()) {
                    if (st.changesVariable(v)) isChanged = true;
                }
                if (isChanged) {
                    sb.append(tabSpace).append(SMVConstants.State).append(" = ").append(s.getStateName(verbose))
                            .append(" : ");
                    sb.append(v.getScopedName(verbose)).append(";\n");
                }
            } else if (s.changesVariable(v)) {
                sb.append(tabSpace).append(SMVConstants.State).append(" = ").append(s.getStateName(verbose))
                        .append(" : ");
                sb.append(s.toNewVariableValueSMV(v, verbose, noRollback)).append(";\n");
            }
        }
    }

    private void commitVariableNextValue(StringBuilder sb, Variable v, Function f, boolean verbose,
            boolean noRollback) {
        for (StateI s : f.block.getAllStates()) {
            if (s.getStateType() == StateType.FunctionSuccessState) {
                boolean isChanged = false;
                for (StateI st : f.block.getAllStates()) {
                    if (st.changesVariable(v)) isChanged = true;
                }
                if (isChanged) {
                    sb.append(tabSpace).append(SMVConstants.State).append(" = ").append(s.getStateName(verbose))
                            .append(" : ");
                    if (!noRollback) {
                        sb.append(SMVConstants.TempVariable).append(v.getScopedName(verbose)).append(";\n");
                    } else {
                        sb.append(v.getScopedName(verbose)).append(";\n");
                    }
                }
            }
        }
    }

    /**
     * @return "contract_" + identifier
     */
    public String getSMVName() {
        return "contract_" + identifier;
    }

    public Variable getVariable(String name) {
        return state_variables.getOrDefault(name, null);
    }

    public StringBuilder stateMachineSMV(String startStateName, boolean verbose) {
        StringBuilder sb = new StringBuilder();

        for (Function f : functions.values()) {
            sb.append(f.stateMachineSMV(startStateName, verbose));
        }

        return sb;
    }

    public String toDOTString(boolean includeHeader, boolean clusters, boolean verbose) {
        StringBuilder sb = new StringBuilder();
        int cluster_count = 0;

        if (includeHeader) {
            sb.append("digraph {\nrankdir=LR;\nnode[shape=box];\nlabel=\"Contract '").append(identifier)
                    .append("'\";\n");
        }

        if (modifiers != null && modifiers.size() > 0) {
            for (Function f : modifiers.values()) {
                addCluster(sb, (clusters) ? cluster_count++ : -1, f, verbose);
            }
        }

        if (constructor != null) {
            addCluster(sb, (clusters) ? cluster_count++ : -1, constructor, verbose);
        }
        sb.append("\n");

        if (functions != null && functions.size() > 0) {
            for (Function f : functions.values()) {
                addCluster(sb, (clusters) ? cluster_count++ : -1, f, verbose);
            }
        }

        if (includeHeader) {
            sb.append("}");
        }

        return sb.toString();
    }

    private void addCluster(StringBuilder sb, int cluster_count, Function f, boolean verbose) {
        StateI state;
        HashSet<StateI> visited = new HashSet<StateI>();
        Queue<StateI> states = new LinkedList<StateI>();

        states.add(f.block.getStartState());
        if (cluster_count >= 0) {
            sb.append("subgraph cluster_").append(cluster_count++).append(" {\nlabel=\"Modifier ").append(f.identifier)
                    .append("\";\n");
        }
        // sb.append(startStateName).append(" -> ").append(f.start_state.getStateName(verbose))
        // .append("[label=\"TRUE\"];\n");

        while (states.size() > 0) {
            state = states.remove();
            visited.add(state);

            for (StateTransition st : state.getStateTransitions()) {
                sb.append(state.getStateName(verbose)).append(" -> ").append(st.nextState.getStateName(verbose))
                        .append("[label=\"").append(st.condition.toString()).append("\"];\n");
                if (!visited.contains(st.nextState)) {
                    states.add(st.nextState);
                }
            }
        }
        if (cluster_count >= 0) {
            sb.append("}");
        }
        sb.append("\n\n");
    }

    @Override
    public String toString() {
        return toSolidityString();
    }

    public String getStateName(boolean verbose) {
        if (verbose) return identifier;
        return "c" + number;
    }

    /**
     * register a new enum data type.
     *
     * @param s
     * @param enumValues
     */
    public void createNewEnum(String s, ArrayList<String> enumValues) {
        // System.out.println(s);
        // for(String enumValue : enumValues){
        // System.out.println(enumValue);
        // }
        enums.put(s, enumValues);
    }

    public static boolean isTypeSupported(String typeName) {
        switch (typeName) {
            case "bool" :
            case "int" :
            case "uint" :
            case "query" :
            case "address" :
                return true;
            default :
                return false;
        }
    }
}
