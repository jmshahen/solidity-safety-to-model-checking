package solidity_safety.ethereum.data;

import solidity_safety.constants.SMVConstants;

public class UintData extends IntData {
    public int MIN_INT() {
        return SMVConstants.NuSMV_MIN_UINT;
    }
    public int MAX_INT() {
        return SMVConstants.NuSMV_MAX_UINT;
    }
    @Override
    public void setValue(String value) throws InvalidValueForDateType {
        Integer.parseUnsignedInt(value);
        super.setValue(value);
    }
}
