package solidity_safety.ethereum.data;

public enum QueryStateDataValues {
    /**
     * 
     */
    STATE_NOT_REACHED("not_reached"),
    /**
     * 
     */
    STATE_REACHED_UNCONFIRMED("condition_reached"),
    /**
     * 
     */
    STATE_REACHED_CONFIRMED("condition_confirmed");

    public String val;
    QueryStateDataValues(String val) {
        this.val = val;
    }
}
