package solidity_safety.ethereum.data;

import solidity_safety.ethereum.SmartContract;
import solidity_safety.ethereum.function.Function;

public class Variable {
    public static int _NEXT_VARIABLE_NUM = 1;

    /** Holds the unique variable number for this variable */
    public int variable_num;

    public String typeName;
    /** The identifier that is used in the SmartContract, which can conflict with other variables in different scopes*/
    public String identifier;
    /** Identifier that can be used in a SmartContract level scope*/
    public String scopedIdentifier;
    public String scopedIdentifierShort;
    /** Identifies what scope a variable is declared in */
    public VariableScope scope = VariableScope.StateVariable;
    public SmartContract parentContract;
    /** Stores the parent function only if the scope is: LocalVariable, ParameterVariable */
    public Function parentFunction;
    public String scopeName = "";
    public DataI value;

    public Variable(VariableScope scope, String scopeName, String typeName, String identifier) {
        setVariableNumber(); // MUST BE CALLED FIRST IN CONSTRUCTOR

        this.scope = scope;
        this.scopeName = scopeName;
        this.typeName = typeName;
        this.identifier = identifier;

        boolean is_enum = SmartContract.enums.get(typeName) != null;
        if (!isTypeSupported(typeName) && !is_enum) {
            throw new UnsupportedOperationException("\"" + typeName + "\" is not a supported datatype.");
        }
        value = newDataType(typeName, is_enum);

        updateScopedIdentifier();
    }

    private void setVariableNumber() {
        variable_num = _NEXT_VARIABLE_NUM;
        _NEXT_VARIABLE_NUM++;
    }

    /**
     * Updates the variable {@link Variable#scopedIdentifier} using 
     */
    public void updateScopedIdentifier() {
        // Setting the Scoped Identifier
        scopedIdentifier = "";
        if (scopeName != null && !scopeName.isEmpty()) scopedIdentifier += scopeName + "_";
        if (scope != null) scopedIdentifier += scope + "_";
        scopedIdentifier += identifier;

        scopedIdentifierShort = "v" + variable_num;
    }

    public void setValue(String value) throws InvalidValueForDateType {
        this.value.setValue(value);
    }

    @Override
    public String toString() {
        return "[Contract: " + ((parentContract == null) ? "None" : parentContract.identifier) + "; Function: "
                + ((parentFunction == null) ? "None" : parentFunction.identifier) + "; " + scope + ": \"" + scopeName
                + "\"] " + typeName + " " + identifier + " = " + value.getValue();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Variable) { return toString().equals(((Variable) obj).toString()); }
        return false;
    }

    @Override
    public int hashCode() {
        return ((parentContract == null) ? 0 : parentContract.identifier.hashCode()) + 
                + ((parentFunction == null) ? 0 : parentFunction.identifier.hashCode()) + scope.hashCode() + scopeName.hashCode()
                + typeName.hashCode() +  identifier.hashCode() + value.hashCode();
    }

    public static boolean isTypeSupported(String typeName) {
        switch (typeName) {
            case "bool" :
            case "int" :
            case "uint" :
            case "query" :
            case "address" :
                return true;
            default :
                return false;
        }
    }

    public static DataI newDataType(String typeName, Boolean is_enum) {

        if (is_enum) { return new EnumData(typeName); }
        switch (typeName) {
            case "bool" :
                return new BooleanData();
            case "int" :
                return new IntData();
            case "uint" :
                return new UintData();
            case "address" :
                return new AddressData();
            case "query" :
                return new QueryStateData();
            default :
                return null;
        }
    }

    public String getSMVVariableDeclaration(boolean verbose) {
        return getScopedName(verbose) + " : " + value.getSMVVariableDeclaration(verbose) + ";";
    }

    /**
     * Returns a variable name that does not conflict within a SmartContract scope
     * @param verbose
     * @return
     */
    public String getScopedName(boolean verbose) {
        if (verbose) return scopedIdentifier;
        return scopedIdentifierShort;
    }
}
