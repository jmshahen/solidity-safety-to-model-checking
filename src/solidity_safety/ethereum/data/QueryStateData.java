package solidity_safety.ethereum.data;

public class QueryStateData implements DataI {
    public QueryStateDataValues data = QueryStateDataValues.STATE_NOT_REACHED;

    @Override
    public void setValue(String value) throws InvalidValueForDateType {
        // DO NOTHING
    }

    @Override
    public String getValue() {
        return data.toString();
    }

    @Override
    public String getSMVVariableDeclaration(boolean verbose) {
        return "{" + QueryStateDataValues.STATE_NOT_REACHED.val + ","
                + QueryStateDataValues.STATE_REACHED_UNCONFIRMED.val + ","
                + QueryStateDataValues.STATE_REACHED_CONFIRMED.val + "}";
    }

    @Override
    public String limitValueSMV(String expression) {
        return expression;
    }

}
