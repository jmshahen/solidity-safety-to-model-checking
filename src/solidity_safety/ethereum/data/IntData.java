package solidity_safety.ethereum.data;

import solidity_safety.constants.SMVConstants;

public class IntData implements DataI {
    public Integer data = 0;

    public int MIN_INT() {
        return SMVConstants.NuSMV_MIN_INT;
    }
    public int MAX_INT() {
        return SMVConstants.NuSMV_MAX_INT;
    }

    @Override
    public void setValue(String value) throws InvalidValueForDateType {
        data = Integer.parseInt(value);
    }

    @Override
    public String getValue() {
        return data.toString();
    }

    @Override
    public String getSMVVariableDeclaration(boolean verbose) {
        return MIN_INT() + " .. " + MAX_INT();
    }

    @Override
    public String limitValueSMV(String expression) {
        if (expression == null) return getValue();
        if (expression.matches("[0-9]+")) return expression;
        return "max(" + MIN_INT() + ", (" + expression + ") mod " + MAX_INT() + ")";
    }

}
