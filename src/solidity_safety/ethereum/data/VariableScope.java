package solidity_safety.ethereum.data;

public enum VariableScope {
    StateVariable, LocalVariable, ParameterVariable, StructVariable, QueryVariable
}
