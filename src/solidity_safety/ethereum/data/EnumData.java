package solidity_safety.ethereum.data;

import java.util.ArrayList;

import solidity_safety.ethereum.SmartContract;

public class EnumData implements DataI {

    public String data;
    public String SMVDeclaration;

    public EnumData(String typeName) {
        ArrayList<String> enumDataValues = new ArrayList<>();
        enumDataValues = SmartContract.enums.get(typeName);
        if (enumDataValues != null) {
            this.data = enumDataValues.get(0);
            this.SMVDeclaration = setSMVDeclaration(enumDataValues);
        }

    }

    private String setSMVDeclaration(ArrayList<String> enumDataValues) {
        StringBuilder sb = new StringBuilder();
        sb.append('{');
        for (int i = 0; i < enumDataValues.size(); i++) {
            if (i < enumDataValues.size() - 1) {
                sb.append(enumDataValues.get(i));
                sb.append(',');
            } else {
                sb.append(enumDataValues.get(i));
            }
        }
        sb.append('}');
        return sb.toString();
    }

    @Override
    public void setValue(String value) throws InvalidValueForDateType {
        this.data = value.split("\\.")[1];
    }

    @Override
    public String getValue() {
        return data;
    }

    @Override
    public String getSMVVariableDeclaration(boolean verbose) {
        return SMVDeclaration;
    }
    @Override
    public String limitValueSMV(String expression) {
        if (expression == null) return getValue();
        String value;
        if (expression.split("\\.").length < 2) {
            value = expression.split("\\.")[0];
        } else {
            value = expression.split("\\.")[1];
        }
        return value;
    }
}
