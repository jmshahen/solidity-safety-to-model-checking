package solidity_safety.ethereum.data;

public interface DataI {
    public void setValue(String value) throws InvalidValueForDateType;
    public String getValue();
    public String getSMVVariableDeclaration(boolean verbose);
    public String limitValueSMV(String expression);
}
