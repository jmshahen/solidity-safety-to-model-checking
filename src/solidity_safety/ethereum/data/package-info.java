/**
 * Contains all Data Types which we support from Solidity.
 * <p>
 * To add a new Data Type:
 * <ol>
 * <li>Create a class in this package with <code>implements DataI</code>
 * <li>Update {@link solidity_safety.ethereum.data.Variable#isTypeSupported(String)}
 * <li>Update {@link solidity_safety.ethereum.data.Variable#newDataType(String)}
 * <li>Update parser {@link solidity_safety.parser.SolidityParser} (MUST UPDATE <b>Solidity.g4</b> and 
 *      then generate the new parsers with <code>ant parsers</code>)
 * </ol>
 */
package solidity_safety.ethereum.data;
