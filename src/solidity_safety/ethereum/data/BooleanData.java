package solidity_safety.ethereum.data;

public class BooleanData implements DataI {
    public Boolean data = false;

    @Override
    public void setValue(String value) throws InvalidValueForDateType {
        data = Boolean.parseBoolean(value);
    }

    @Override
    public String getValue() {
        return data.toString().toUpperCase();
    }

    @Override
    public String getSMVVariableDeclaration(boolean verbose) {
        return "boolean";
    }

    @Override
    public String limitValueSMV(String expression) {
        if(expression != null){
            expression = expression.replaceAll("\\b" + "false" + "\\b", "FALSE");
            expression = expression.replaceAll("\\b" + "true" + "\\b", "TRUE");
        }
        if (expression == null) return getValue();
        return expression;
    }
}
