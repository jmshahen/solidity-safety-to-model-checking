package solidity_safety.ethereum.data;

public class AddressData implements DataI {
    public String data = "0x0";
    @Override
    public void setValue(String value) throws InvalidValueForDateType {
        // System.out.println("Address: " + value);
        if (value.startsWith("address(") && value.endsWith(")")) {
            value = value.replaceFirst("address\\(", "");
            value = value.substring(0, value.length() - 1);
        }
        // System.out.println("Address: " + value);
        if (!value.startsWith("0x")) { throw new InvalidValueForDateType(); }
        data = value;
    }

    @Override
    public String getValue() {
        return data;
    }

    @Override
    public String getSMVVariableDeclaration(boolean verbose) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String limitValueSMV(String expression) {
        return expression;
    }

}
