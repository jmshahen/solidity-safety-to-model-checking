package solidity_safety.ethereum;

import java.io.File;
import java.util.*;

import org.apache.commons.collections4.IterableUtils;

import solidity_safety.constants.SMVConstants;
import solidity_safety.ethereum.data.*;
import solidity_safety.ethereum.function.*;
import solidity_safety.ethereum.query.QueryI;
import solidity_safety.ethereum.statemachine.StateTransition;
import solidity_safety.ethereum.statemachine.blocks.BlockI;
import solidity_safety.ethereum.statemachine.states.*;

/**
 * An Ethereum DApp (Distributed Application) is a collection of Smart Contracts,
 * that interact with each other to provide some service within the Ethereum network.
 * <br>
 * At minimum there must be 1 contract within a DApp.
 *
 * @author Jonathan Shahen
 */
public class DApp {
    public static String tabSpace = "    ";
    /** When FALSE, the reduction will allow for rollback simulation.
     * When TRUE, if a require statement fails, it will go to the error state. */
    public Boolean noRollback = false;
    /** The Gas Limit to apply to the Attacker. When NULL, no gas limit is applied. */
    public Long attackerGasLimit = null;
    /** A reference to the start state for the FSM */
    public StateI startState;
    /** A reference to the Loop state, where the next function can be chosen based on the model checker's heuristic. */
    public StateI loopState;
    /** A reference to the error state, where there are no exit edges. */
    public StateI errorState;
    /** A reference to the gas error state, where there are no exit edges.
     * Only enters this state if the gas limit == 0.*/
    public StateI gasErrorState;

    public ArrayList<SmartContract> contracts = new ArrayList<>();
    public ArrayList<QueryState> queries = new ArrayList<>();

    /**
     * Reset every time that {@link DApp#collectQueryNames(StringBuilder, boolean)} is called.
     * Stores: queryName -> QueryI
     */
    public HashMap<String, QueryI> queryExpectedResult = null;

    public DApp() {
        // Can't call it 'init' since that is a reserved word in NuSMV
        startState = new ParentlessState("start", "start");
        loopState = new ParentlessState("LoopState", "loop");
        errorState = new ParentlessState("ErrorState", "err");
        gasErrorState = new ParentlessState("GasErrorState", "gerr");
    }

    /**
     * Call this when the DApp has finished being added to.
     */
    public void dappLoaded() {
        StateI lastState = startState;
        for (SmartContract sc : contracts) {
            if (sc.constructor != null) {
                lastState.addState(StateTransition.newTrue(sc.constructor.block.getStartState()));
                lastState = sc.constructor.block.getLastState();
            }
        }
        lastState.addState(StateTransition.newTrue(loopState));
    }

    public void addContract(SmartContract sc) {
        contracts.add(sc);
        sc.number = contracts.size();
    }

    public void addQuery(QueryState queryState) {
        queries.add(queryState);
    }

    /**
     * Creates the self contained SMV module, which contains all queries, variables and sub-modules
     *
     * @param verbose when TRUE, then state names will be descriptive; otherwise every name with be as small as possible
     */
    public StringBuilder toSMVString(boolean verbose) throws CloneNotSupportedException {
        StringBuilder sb = new StringBuilder();

        sb.append("MODULE main\n");

        sb.append(getQueriesSMV(verbose));
        sb.append(collectVariables(verbose));
        sb.append(assignVariables(verbose));

        for (SmartContract sc : contracts) {
            sb.append("\n").append(sc.toSMVModule(verbose, noRollback));
        }

        return sb;
    }

    private StringBuilder getQueriesSMV(boolean verbose) {
        StringBuilder sb = new StringBuilder();

        for (QueryState qs : queries) {
            sb.append(qs.toSMV(verbose)).append("\n");
        }

        return sb;
    }

    /**
     * Returns the VAR and IVAR sections of the SMV file.
     * This is wear all the custom variables are defined.
     * <br>
     * The custom variables are:
     * <ul>
     * <li>(VAR) Query Variables
     * <li>(VAR) The State Machine Variable
     * <li>(VAR) Query Name variable, to locate queries from the NuSMV output
     * <li>(VAR) The contract variables
     * <li>(IVAR) The next function to call variable
     * </ul>
     * @param verbose
     * @return VAR and IVAR SMV sections
     * @throws CloneNotSupportedException
     */
    private StringBuilder collectVariables(boolean verbose) throws CloneNotSupportedException {
        StringBuilder sb = new StringBuilder();

        addModifierStates(verbose);
        StringBuilder functionStates = new StringBuilder();
        StringBuilder allStates = new StringBuilder();
        collectStates(allStates, functionStates, verbose);

        StringBuilder queryNames = new StringBuilder();
        collectQueryNames(queryNames, verbose);

        sb.append("VAR\n");
        // Adding the Query Variables (used to deem success of query)
        for (QueryState qs : queries) {
            sb.append(qs.queryVar.getSMVVariableDeclaration(verbose)).append("\n");
        }
        // Adding the State Machine Variable
        sb.append(SMVConstants.State).append(" : {");
        if (verbose) sb.append("\n").append(tabSpace);
        sb.append(allStates);
        sb.append("\n};\n");

        // Adding the Query Names Variable
        sb.append(SMVConstants.QueryName).append(" : {");
        if (queryNames.length() > 0) {
            if (verbose) sb.append("\n").append(tabSpace);
            sb.append(queryNames);
            sb.append("\n};\n");
        } else sb.append("noQueries};\n");

        // Add the AttackerGasLimit if not NULL
        if (attackerGasLimit != null) {
            sb.append(SMVConstants.AttackerGasLimit).append(" : 0 .. ").append(attackerGasLimit).append(";\n");
        }

        for (SmartContract sc : contracts) {
            sb.append("contract").append(sc.number).append(" : ").append(sc.getSMVName()).append("(")
                    .append(SMVConstants.State).append(",").append(SMVConstants.QueryName).append(");\n");
        }

        sb.append("IVAR\n");
        sb.append("nextFunc : {").append(functionStates).append("};\n");

        return sb;
    }

    /**
     * This function returns the ASSIGN section for the SMV file.
     * Within ASSIGN we need to init all variables for the DApp and provide the next value for each.
     * <br>
     * This is only for the DApp, each contract has their own sub-module with an ASSIGN section
     * @param verbose
     * @return SMV ASSIGN section
     */
    private StringBuilder assignVariables(boolean verbose) {
        StringBuilder sb = new StringBuilder();

        sb.append("ASSIGN\n");
        // INIT
        for (QueryState qs : queries) {
            sb.append("init(").append(qs.queryVar.identifier).append(") := ")
                    .append(QueryStateDataValues.STATE_NOT_REACHED.val).append(";\n");
        }
        sb.append("init(").append(SMVConstants.State).append(") := ").append(startState.getStateName(verbose))
                .append(";\n");

        // Add the AttackerGasLimit if not NULL
        if (attackerGasLimit != null) {
            sb.append("init(").append(SMVConstants.AttackerGasLimit).append(") := ").append(attackerGasLimit)
                    .append(";\n");
        }

        // NEXT
        for (QueryState qs : queries) {
            String var = qs.queryVar.identifier;
            sb.append("next(").append(var).append(") := case\n").append(tabSpace);
            // state = qs.parent.first_state.getStateName() : QueryStateDataValues.STATE_NOT_REACHED.val
            sb.append(SMVConstants.State).append(" = ").append(qs.getParent().getStartState().getStateName(verbose));
            sb.append(" : ").append(QueryStateDataValues.STATE_NOT_REACHED.val).append(";\n").append(tabSpace);
            // state = qs.getStateName() : QueryStateDataValues.STATE_REACHED_UNCONFIRMED.val
            sb.append(SMVConstants.State).append(" = ").append(qs.getStateName(verbose)).append(" : ")
                    .append(QueryStateDataValues.STATE_REACHED_UNCONFIRMED.val).append(";\n").append(tabSpace);
            // state = qs.parent.last_state.getStateName() & qs.queryVar.identifier =
            // QueryStateDataValues.STATE_REACHED_UNCONFIRMED.val &
            // qs.goalStateCondition:QueryStateDataValues.STATE_REACHED_CONFIRMED.val
            sb.append(SMVConstants.State).append(" = ").append(qs.getParent().getLastState().getStateName(verbose));
            sb.append(" & ").append(var).append(" = ").append(QueryStateDataValues.STATE_REACHED_UNCONFIRMED.val);
            if (qs.getCondition() != null && !qs.getCondition().isEmpty()) {
                sb.append(" & ").append(qs.getCondition());
            }
            sb.append(" : ").append(QueryStateDataValues.STATE_REACHED_CONFIRMED.val).append(";\n").append(tabSpace);
            sb.append("TRUE : ").append(var).append(";\nesac;\n");
        }
        sb.append(stateMachineSMV(startState, verbose));

        return sb;
    }

    public void setFilename(File file) {
        for (SmartContract sc : contracts) {
            sc.file = file;
        }
    }

    public StringBuilder toDOTHTMLString(boolean secondLoop, boolean debug) {
        StringBuilder sb = new StringBuilder();
        sb.append(HTML_TEMPLATE_PRE);
        sb.append(toDOTString(startState, secondLoop, debug));
        sb.append(HTML_TEMPLATE_SUF);
        return sb;
    }

    public StringBuilder stateMachineSMV(StateI startState, boolean verbose) {
        StringBuilder sb = new StringBuilder();
        StringBuilder sb_gas = new StringBuilder();

        StateI state;
        HashSet<StateI> visited = new HashSet<StateI>();
        Queue<StateI> states = new LinkedList<StateI>();

        states.add(startState);
        visited.add(startState);

        sb.append("next(").append(SMVConstants.State).append(") := case\n");
        if (attackerGasLimit != null) {
            sb_gas.append("next(").append(SMVConstants.AttackerGasLimit).append(") := case\n");
            sb.append(tabSpace).append(SMVConstants.AttackerGasLimit).append(" = 0 : ")
                    .append(gasErrorState.getStateName(verbose)).append(";\n");
        }
        sb.append(tabSpace).append(SMVConstants.State).append(" = ").append(loopState.getStateName(verbose))
                .append(" : nextFunc;\n");

        while (states.size() > 0) {
            state = states.remove();

            if (attackerGasLimit != null) {
                int gasCost = state.getGasCost();
                if (gasCost != 0) {
                    sb_gas.append(tabSpace).append(SMVConstants.State).append(" = ").append(state.getStateName(verbose))
                            .append(" : max(0,").append(SMVConstants.AttackerGasLimit).append(" - ").append(gasCost)
                            .append(");\n");
                }
            }

            for (StateTransition st : state.getStateTransitions()) {
                if (state == loopState) {
                    if (st.nextState.getParentFunctionType() == FunctionType.ModifierFunction) {
                        visited.addAll(st.nextState.getParent().getAllStates());
                    }
                }
                if (state != loopState)
                    sb.append(tabSpace)
                            .append(st.condition.toSMV(state, st.nextState, state.getContractNumber(0), verbose));

                if (!visited.contains(st.nextState)) {
                    states.add(st.nextState);
                    visited.add(st.nextState);
                }
            }
        }

        sb.append(tabSpace).append("TRUE: ").append(SMVConstants.State).append(";\n");
        sb.append("esac;\n");
        if (attackerGasLimit != null) {
            sb_gas.append(tabSpace).append("TRUE: ").append(SMVConstants.AttackerGasLimit).append(";\n");
            sb_gas.append("esac;\n");

            sb.append(sb_gas);
        }

        return sb;
    }

    private void collectStates(StringBuilder allStates, StringBuilder functionStates, boolean verbose) {
        StateI state;
        HashSet<StateI> visited = new HashSet<StateI>();
        HashSet<String> foundFunctions = new HashSet<>();
        Queue<StateI> states = new LinkedList<StateI>();

        states.add(startState);
        visited.add(startState);
        states.add(errorState);
        visited.add(errorState);
        if (attackerGasLimit != null) {
            states.add(gasErrorState);
            visited.add(gasErrorState);
        }
        functionStates.append(loopState.getStateName(verbose)).append(",");

        while (states.size() > 0) {
            state = states.remove();

            allStates.append(state.getStateName(verbose)).append(",");

            for (StateTransition st : state.getStateTransitions()) {
                if (state == loopState && !(st.nextState.getParentFunctionType() == FunctionType.ModifierFunction)) {
                    if (!foundFunctions.contains(st.nextState.getStateName(verbose))) {
                        functionStates.append(st.nextState.getStateName(verbose)).append(",");
                        foundFunctions.add(st.nextState.getStateName(verbose));
                    }
                }
                if (!visited.contains(st.nextState)) {
                    states.add(st.nextState);
                    visited.add(st.nextState);
                }
            }
        }
        allStates.setLength(allStates.length() - 1); // remove the ","
        functionStates.setLength(functionStates.length() - 1); // remove the ","
    }

    /**
     * unwrap modifier functions of each function into it
     * @param verbose
     * @throws CloneNotSupportedException
     */
    private void addModifierStates(boolean verbose) throws CloneNotSupportedException {
        HashMap<String, BlockI> functionModifierBlocks = new HashMap<>();

        getFunctionModifierBlocks(functionModifierBlocks);

        for (SmartContract sc : contracts) {
            for (Function f : sc.functions.values())
                for (int j = f.require_modifiers.size() - 1; j >= 0; j--) {
                    if (functionModifierBlocks.containsKey(f.require_modifiers.get(j))) {
                        applyModifierToFunction(functionModifierBlocks, f, f.require_modifiers.get(j));
                    }
                }
        }
    }

    /**
     * unwrap modifier function mf into function f
     * @param functionModifierBlocks
     * @param f
     * @param mf
     * @throws CloneNotSupportedException
     */
    private void applyModifierToFunction(HashMap<String, BlockI> functionModifierBlocks, Function f, String mf)
            throws CloneNotSupportedException {
        removeBlockCurrentEntryAndSuccessState(f);

        StateI functionCurrentStartState = f.block.getStartState();
        BlockI modifierBlock = functionModifierBlocks.get(mf);

        assignNewNumberToModifierBlockStates(f, modifierBlock);
        addParameterAndLocalVariableToModifierFunction(f, modifierBlock);

        StateI modifierFunctionStartState = modifierBlock.getStartState().clone();
        modifierFunctionStartState.setParent(f.block);
        f.block.addRawState(modifierFunctionStartState);
        f.block.setStartState(modifierFunctionStartState);

        updateLoopState(f, functionCurrentStartState);

        HashSet<StateI> mdVisited = new HashSet<>();
        Queue<StateI> mdStates = new LinkedList<StateI>();

        mdStates.add(modifierFunctionStartState);
        mdVisited.add(modifierFunctionStartState);

        cloneModifierBlockStatesToFunction(f, functionCurrentStartState, mdVisited, mdStates);
    }

    /**
     * Get a clone of all modifier block states and add them to function f
     * @param f
     * @param functionCurrentStartState
     * @param mdVisited
     * @param mdStates
     * @throws CloneNotSupportedException
     */
    private void cloneModifierBlockStatesToFunction(Function f, StateI functionCurrentStartState,
            HashSet<StateI> mdVisited, Queue<StateI> mdStates) throws CloneNotSupportedException {
        boolean isThisModifier = true;
        StateI mdState;
        while (mdStates.size() > 0) {
            mdState = mdStates.remove();
            ArrayList<StateTransition> tempStateTransitions = mdState.getStateTransitions();
            mdState.updateStateTransitions(new ArrayList<StateTransition>());
            for (StateTransition st : tempStateTransitions) {
                if (st.nextState == loopState) continue;
                StateI sttState = st.nextState.clone();
                sttState.setParent(f.block);

                if (!mdVisited.contains(sttState)) {
                    f.block.addRawState(sttState);
                    if (sttState.getStateType() == StateType.RequireState) {
                        if (!f.parent.requireConditions.contains(sttState)) {
                            f.parent.requireConditions.add((BranchingStateI) sttState);
                        }
                    }
                    if (sttState.getStateType() == StateType.IfState) {
                        if (!f.parent.ifConditions.contains(sttState)) {
                            f.parent.ifConditions.add((BranchingStateI) sttState);
                        }
                    }

                    if (sttState.getStateType() == StateType.FunctionModifierSymbolState && isThisModifier) {
                        isThisModifier = false;
                        manageModifierSymbolState(f, functionCurrentStartState, mdVisited, mdStates, sttState);
                    }

                    mdState.addState(new StateTransition(sttState, st.condition));

                    mdStates.add(sttState);
                    mdVisited.add(sttState);
                } else {
                    mdState.addState(new StateTransition(sttState, st.condition));
                }
            }
        }

        StateI successState = null;
        for (StateI state : f.block.getAllStates()) {
            if (state.getStateType() == StateType.FunctionSuccessState) {
                successState = state;
            }
        }
        for (StateI state : f.block.getAllStates()) {
            if (state.getStateTransitions().size() == 0) {
                if (state.getStateType() == StateType.FunctionSuccessState) {
                    f.block.setLastState(state);
                    state.addState(StateTransition.newTrue(loopState));
                } else if (state.getStateType() == StateType.FunctionFailedEndState) {
                    state.addState(StateTransition.newTrue(loopState));
                } else if (state.getStateType() != StateType.FailedRequiredState) {
                    state.addState(StateTransition.newTrue(successState));
                }
            }
        }
    }

    /**
     * assign a new number to each of the modifierBlock states
     * @param f
     * @param modifierBlock
     */
    private void assignNewNumberToModifierBlockStates(Function f, BlockI modifierBlock) {
        int counter = f.block.getAllStates().size() + 1;
        for (StateI state : modifierBlock.getAllStates()) {
            state.setNumber(counter);
            counter++;
        }
    }

    /**
     * copy the modifier function parameters and local variables into function
     * @param f
     * @param modifierBlock
     */
    private void addParameterAndLocalVariableToModifierFunction(Function f, BlockI modifierBlock) {
        for (Variable v : IterableUtils.chainedIterable(modifierBlock.getParent().parameters.values(),
                modifierBlock.getParent().internal_variables.values())) {
            Variable clonedVariable = new Variable(v.scope, f.identifier, v.typeName, v.identifier);
            clonedVariable.value = v.value;
            if (v.scope == VariableScope.ParameterVariable) {
                clonedVariable.parentFunction = f;
                clonedVariable.parentContract = v.parentContract;
                f.parameters.put(clonedVariable.identifier, clonedVariable);
            } else {
                f.addLocalVariable(clonedVariable);
            }
            for (StateI state : modifierBlock.getAllStates()) {
                if (state.changesVariable(v)) {
                    if (state.getStateType() == StateType.DataChangeState) {
                        ((DataChangeState) state).setUpdateVar(clonedVariable);
                    }
                }
            }
        }
    }

    /**
     * manage "_;" symbol in modifier function
     *
     * @param f
     * @param functionCurrentStartState
     * @param mdVisited
     * @param mdStates
     * @param sttState
     * @throws CloneNotSupportedException
     */
    private void manageModifierSymbolState(Function f, StateI functionCurrentStartState, HashSet<StateI> mdVisited,
            Queue<StateI> mdStates, StateI sttState) throws CloneNotSupportedException {
        ArrayList<StateTransition> FMSymbolStateTransitions = sttState.getStateTransitions();

        sttState.updateStateTransitions(new ArrayList<>());
        sttState.addState(StateTransition.newTrue(functionCurrentStartState));

        StateI nextOfSttState = FMSymbolStateTransitions.get(0).nextState.clone();
        nextOfSttState.setParent(f.block);
        if (!mdVisited.contains(nextOfSttState)) {
            f.block.addRawState(nextOfSttState);
            if (nextOfSttState.getStateType() == StateType.RequireState) {
                if (f.parent.requireConditions.contains(nextOfSttState))
                    f.parent.requireConditions.add((BranchingStateI) nextOfSttState);
            }
            if (sttState.getStateType() == StateType.IfState) {
                if (!f.parent.ifConditions.contains(nextOfSttState))
                    f.parent.ifConditions.add((BranchingStateI) nextOfSttState);
            }

            f.block.getLastState().updateStateTransitions(new ArrayList<StateTransition>());
            f.block.getLastState().addState(StateTransition.newTrue(nextOfSttState));

            mdStates.add(nextOfSttState);
            mdVisited.add(nextOfSttState);
        } else {
            f.block.getLastState().updateStateTransitions(new ArrayList<StateTransition>());
            f.block.getLastState().addState(StateTransition.newTrue(nextOfSttState));
        }
    }

    /**
     * updates the loopState after modifying a function block
     *
     * @param f
     * @param functionCurrentStartState
     */
    private void updateLoopState(Function f, StateI functionCurrentStartState) {
        ArrayList<StateTransition> loopStateStateTransitions = loopState.getStateTransitions();
        ArrayList<StateTransition> toBeRemovedST = new ArrayList<>();
        for (StateTransition loopStateST : loopStateStateTransitions) {
            if (loopStateST.nextState.equals(functionCurrentStartState)) {
                toBeRemovedST.add(loopStateST);
            }
        }
        for (StateTransition toBeRemoved : toBeRemovedST) {
            loopStateStateTransitions.remove(toBeRemoved);
        }
        loopStateStateTransitions.add(StateTransition.newTrue(f.block.getStartState()));
        loopState.updateStateTransitions(loopStateStateTransitions);
    }

    /**
     * Remove transitions to/from FunctionEntryState and FunctionSuccessState from function Block
     * Remove FunctionEntryState and FunctionSuccessState from function Block
     *
     * @param f
     */
    private void removeBlockCurrentEntryAndSuccessState(Function f) {
        if (f.block.getAllStates().size() < 5) {
            ArrayList<StateTransition> startStateTransitions = f.block.getStartState().getStateTransitions();
            f.block.getStartState().updateStateTransitions(new ArrayList<>());
            StateI emptyState = new EmptyState();
            emptyState.setParent(f.block);
            emptyState.setNumber(f.block.getAllStates().size() + 1);
            f.block.addRawState(emptyState);
            f.block.getStartState().addState(StateTransition.newTrue(emptyState));
            emptyState.updateStateTransitions(startStateTransitions);
        }
        // Remove transitions to/from FunctionEntryState and FunctionSuccessState from function Block
        for (StateI state : f.block.getAllStates()) {
            ArrayList<StateTransition> tempST = state.getStateTransitions();
            for (StateTransition sTransmission : state.getStateTransitions()) {
                if (sTransmission.nextState.getStateType() == StateType.FunctionEntryState) {
                    tempST.addAll(sTransmission.nextState.getStateTransitions());
                    tempST.remove(sTransmission);
                } else if (sTransmission.nextState.getStateType() == StateType.FunctionSuccessState) {
                    f.block.setLastState(state);
                    tempST.addAll(sTransmission.nextState.getStateTransitions());
                    tempST.remove(sTransmission);
                }
            }
            state.updateStateTransitions(tempST);
        }

        // Remove FunctionEntryState and FunctionSuccessState from function Block
        ArrayList<StateI> toBeRemovedBlockStates = new ArrayList<>();
        ArrayList<StateI> tempBlockStates = f.block.getAllStates();
        for (StateI state : f.block.getAllStates()) {
            if (state.getStateType() == StateType.FunctionEntryState
                    || state.getStateType() == StateType.FunctionSuccessState) {
                toBeRemovedBlockStates.add(state);
            }
        }
        tempBlockStates.removeAll(toBeRemovedBlockStates);
        f.block.updateAllStates(tempBlockStates);
    }

    /**
     * returns modifier function blocks
     *
     * @param functionModifierBlocks
     */
    private void getFunctionModifierBlocks(HashMap<String, BlockI> functionModifierBlocks) {
        for (SmartContract sc : contracts) {
            for (ModifierFunction mf : sc.modifiers.values()) {
                boolean isModifierBlockAlreadyFound = functionModifierBlocks.containsKey(mf.identifier);
                if (!isModifierBlockAlreadyFound) {
                    BlockI modifierBlock = mf.block;
                    modifierBlock.getLastState().updateStateTransitions(new ArrayList<StateTransition>());
                    modifierBlock.getLastState().updateStateTransitions(new ArrayList<StateTransition>());
                    functionModifierBlocks.put(mf.identifier, modifierBlock);
                }
            }
        }
    }

    private void collectQueryNames(StringBuilder queryNames, boolean verbose) {
        queryExpectedResult = new HashMap<>();

        boolean first = true;
        // DApp queries
        for (QueryI q : queries) {
            if (first) {
                first = false;
            } else {
                queryNames.append(",");
            }
            queryNames.append(q.getQueryName(verbose));
            queryExpectedResult.put(q.getQueryName(verbose), q);
        }

        // Smart Contract Queries
        for (SmartContract sc : contracts) {
            for (QueryI q : sc.contractQueries) {
                if (first) {
                    first = false;
                } else {
                    queryNames.append(",");
                }
                queryNames.append(q.getQueryName(verbose));
                queryExpectedResult.put(q.getQueryName(verbose), q);
            }
            for (QueryI q : sc.stateQueries) {
                if (first) {
                    first = false;
                } else {
                    queryNames.append(",");
                }
                queryNames.append(q.getQueryName(verbose));
                queryExpectedResult.put(q.getQueryName(verbose), q);
            }
        }
    }

    public StringBuilder toDOTString(StateI startState, boolean secondLoop, boolean verbose) {
        StringBuilder sb = new StringBuilder();
        StateI state;
        HashSet<StateI> visited = new HashSet<StateI>();
        Queue<StateI> states = new LinkedList<StateI>();
        boolean first = true;

        states.add(startState);
        visited.add(startState);
        if (secondLoop) {
            sb.append(loopState.getStateName(verbose)).append("_2[").append(loopState.getDOTAttributes())
                    .append("];\n");
        }

        while (states.size() > 0) {
            state = states.remove();

            sb.append(state.getStateName(verbose)).append("[").append(state.getDOTAttributes()).append("];\n");
            for (StateTransition st : state.getStateTransitions()) {
                sb.append(state.getStateName(verbose)).append(" -> ");
                if (secondLoop && st.nextState == loopState) {
                    if (first) {
                        first = false;
                        sb.append(st.nextState.getStateName(verbose));
                    } else {
                        sb.append(st.nextState.getStateName(verbose)).append("_2");
                    }
                } else {
                    sb.append(st.nextState.getStateName(verbose));
                }
                sb.append("[label=\"").append(st.condition.toString()).append("\"];\n");

                if (!visited.contains(st.nextState)) {
                    states.add(st.nextState);
                    visited.add(st.nextState);
                }
            }
        }

        return sb;
    }

    public static String HTML_TEMPLATE_PRE = "<!DOCTYPE html><html> <head> <title>Solidity Safety Graph</title> <script src=\"https://d3js.org/d3.v4.min.js\"></script> <script src=\"https://unpkg.com/viz.js@1.8.0/viz.js\" type=\"javascript/worker\"></script> <script src=\"https://unpkg.com/d3-graphviz@1.4.0/build/d3-graphviz.min.js\"></script> <style type=\"text/css\"> #graphContainer{width: 100%; height: 100%; margin: 0px;}html, body{height: 100%; width: 100%; margin: 0px;}</style> </head> <body> <div id=\"graphContainer\"></div><script type=\"text/javascript\">var container=document.getElementById('graphContainer'); var dot=`digraph{\nrankdir=LR;\nnode [shape=box,style=\"filled\"];\n";
    public static String HTML_TEMPLATE_SUF = "}`; function attributer(datum, index, nodes){var selection=d3.select(this); if (datum.tag=='svg'){var width=d3.select('#graphContainer').node().clientWidth; var height=d3.select('#graphContainer').node().clientHeight; var x='10'; var y='10'; var unit='px'; selection.attr('width', width + unit).attr('height', height + unit); datum.attributes.width=width + unit; datum.attributes.height=height + unit;}}d3.select('#graphContainer') .graphviz() .attributer(attributer) .fade(false) .renderDot(dot); </script> </body></html>";
}
