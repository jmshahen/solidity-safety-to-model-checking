package solidity_safety.constants;

public class SMVConstants {
    /**
     * Holds the string used to split the solidity code to help identify variables and change 
     * their names to the NusMV version.
     * 
     * TODO Move this code into the {@link solidity_safety.parser.SolidityParser}
     */
    public static String SplitVariables = "[ <>+!=*/-\\\\(\\\\)]";
    /**
     * The name to use in the NuSMV reduction which will store the current state that the model is in.
     * <br/>
     * <ul>
     * <li><b>Change this name if this name conflicts with the solidity file</b></li>
     * <li>Valid NuSMV variable names are located here: page 130 http://nusmv.fbk.eu/NuSMV/userman/v26/nusmv.pdf</li>
     * <li>"__" cannot be the starting prefix for a variable name</li>
     * </ul>
     */
    public static String State = "_State";
    /** The prefix to use in the NuSMV reduction which will be added to state variables to create a temporary 
     * variable which functions can effect, and then will be copied over to the real state variables 
     * if the function successfully runs.
     * <br/>
     * <ul>
     * <li><b>Change this name if this name conflicts with the solidity file</b></li>
     * <li>Valid NuSMV variable names are located here: page 130 http://nusmv.fbk.eu/NuSMV/userman/v26/nusmv.pdf</li>
     * <li>"__" cannot be the starting prefix for a variable name</li>
     * </ul>
     */
    public static String TempVariable = "_Temp_";
    /**
     * Internal NuSMV variable which takes the value of the query name, so that we can back reference the NuSMV
     * output concerning queries with the queries made in Solidity Safety file.
     * <br/>
     * <ul>
     * <li><b>Change this name if this name conflicts with the solidity file</b></li>
     * <li>Valid NuSMV variable names are located here: page 130 http://nusmv.fbk.eu/NuSMV/userman/v26/nusmv.pdf</li>
     * <li>"__" cannot be the starting prefix for a variable name</li>
     * </ul>
     */
    public static String QueryName = "_QueryName";
    /**
     * [Optional] Internal NuSMV variable which holds the amount of gas an attacker is willing to spend on their attack.
     * <br>
     * Each solidity code will decrement the gas value until it equals 0 (<code>max(0, gaslimit - &lt;gas_value&gt;)</code>).
     * <br>
     * The State Machine will go to the Gas Error state if the <code>gaslimit == 0</code>.
     */
    public static String AttackerGasLimit = "_GasLimit";
    /** Configurable minimum value for the NuSMV INT variable.
     * <br/>
     * This is required for NuSMV to perform arithmetic without entering a invalid state.
     */
    public static int NuSMV_MIN_INT = -200;
    /** Configurable maximum value for the NuSMV INT variable.
     * <br/>
     * This is required for NuSMV to perform arithmetic without entering a invalid state.
     */
    public static int NuSMV_MAX_INT = 200;
    /** Configurable minimum value for the NuSMV UINT variable.
     * <br/>
     * This is required for NuSMV to perform arithmetic without entering a invalid state.
     */
    public static int NuSMV_MIN_UINT = 0;
    /** Configurable maximum value for the NuSMV UINT variable.
     * <br/>
     * This is required for NuSMV to perform arithmetic without entering a invalid state.
     */
    public static int NuSMV_MAX_UINT = 200;
}
