pragma solidity >=0.4.22 <0.6.0;

contract ModifierFunction3 {
    int locked;
    int a1;
    int a2;

    /*!QUERY: a1 > 10 ;REACHABLE*/
    /*!QUERY: a2 > 10 ;REACHABLE*/
    /*!QUERY: locked < 0 ;UNREACHABLE*/
    /*!QUERY: locked > 0 ;UNREACHABLE*/
    /*!QUERY: locked = 0 ;REACHABLE*/

    modifier noReentrancy() {
        require(locked == 0, "Prevents functions from being called at the same time, might exists a race condition.");
        locked = locked + 1;
        _;
        locked = locked - 1;
    }

    function f1() public noReentrancy {
        a1 = a1 + 1;
    }

    function f2() public noReentrancy {
        a2 = a2 + 1;
    }
}