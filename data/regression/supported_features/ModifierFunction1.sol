pragma solidity >=0.4.22 <0.6.0;

contract ModifierFunction1 {
    uint a = 0;
    uint b = 0;

    /*!QUERY: a = 7;REACHABLE*/

    /*!QUERY: a >= 8;UNREACHABLE*/

    /*!QUERY: b = 5;REACHABLE*/

    /*!QUERY: b > 5;UNREACHABLE*/

    constructor() public {
    }

    modifier modFunc{
        require(a < 7, "two functions using the same modifier function");
        _;
    }

    function rsTest() public modFunc{
        if(a > 5){
            a=a+1;
        }else{
            a=a+1;
        }
    }

    function rsTest2() public modFunc{
        if(a > 5){
            b=5;
        }
    }
}