pragma solidity >=0.4.0 <0.7.0;

contract SimpleFunction1 {
    constructor() public {
    }

    function func1() public {
        // Empty function
    }
    function func2() private view returns (uint) {
        /*!QUERY: TRUE ; REACHABLE*/
        return 1;
    }
    function func3() public payable {
    }
    function func4(uint b) public view returns(bool){
        /*!QUERY: TRUE ; REACHABLE*/
        return b == 12;
    }

    function() external payable {
        // this is a sink; it will take value if someone accidentally sends money to this contract
        /*!QUERY: TRUE ; REACHABLE*/
    }
}