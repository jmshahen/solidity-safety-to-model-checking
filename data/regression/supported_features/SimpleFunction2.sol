/*!UINT_MAX: 50;*/
/*!UINT_MIN: 0;*/
/*!INT_MAX: 200;*/
/*!INT_MIN: -200;*/

pragma solidity >=0.4.0 <0.7.0;

contract SimpleFunction2 {
    /*!QUERY: ba = 20; REACHABLE*/
    /*!QUERY: baa = 21; REACHABLE*/
    /*!QUERY: baaa = 22; REACHABLE*/
    /*!QUERY: a = 10; REACHABLE*/
    /*!QUERY: aa = 11; REACHABLE*/
    /*!QUERY: aaa = 12; REACHABLE*/
    /*!QUERY: a = 20; REACHABLE*/
    /*!QUERY: aa = 31; REACHABLE*/
    /*!QUERY: aaa = 42; REACHABLE*/

    /*!QUERY: a = 11; UNREACHABLE*/
    /*!QUERY: a = 12; UNREACHABLE*/
    /*!QUERY: aa = 10; UNREACHABLE*/
    /*!QUERY: aa = 12; UNREACHABLE*/
    /*!QUERY: aaa = 10; UNREACHABLE*/
    /*!QUERY: aaa = 11; UNREACHABLE*/


    int var1 = -10;
    uint a = 10;
    uint aa = 11;
    uint aaa = 12;
    uint ba = 20;
    uint baa = 21;
    uint baaa = 22;
    uint total=0;

    function func1() public {
        // Empty function
    }
    function func2a() private view returns (uint) {
        uint a1 = 1;
        uint b2 = 2;

        a = a + 10;
        a1 = a1 + ba;
        /*!QUERY: TRUE ; REACHABLE*/

        return total + a + a1 + b2;
    }
    function func2aa() private view returns (uint) {
        uint a12 = 1;
        uint b22 = 2;

        aa = aa + 20;
        a12 = aa + ba + baa;
        /*!QUERY: TRUE ; REACHABLE*/

        return total + aa + a12 + b22;
    }
    function func2aaa() private view returns (uint) {
        uint a13 = 1;
        uint b23 = 2;

        aaa = aaa + 30;
        //a13= baa + aa + a + aaa + baa + ba;
        /*!QUERY: TRUE ; REACHABLE*/

        return total + aaa + a13 + b23;
    }
    function func3() public payable {
        total += 1;
    }
    function func4(uint b) public view returns(bool){
        return b == total;
    }

    function() external payable {
        // this is a sink; it will take value if someone accidentally sends money to this contract
    }
}