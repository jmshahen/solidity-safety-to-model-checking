pragma solidity ^0.4.0;

contract ModifierFunction7 {
    uint a = 0;
    uint b = 0;
    uint c = 0;

    /*!QUERY: a > 7;UNREACHABLE*/
    /*!QUERY: a <= 7;REACHABLE*/
    /*!QUERY: a = 7;REACHABLE*/

    /*!QUERY: b > 5;UNREACHABLE*/
    /*!QUERY: b = 5;REACHABLE*/
    /*!QUERY: b <= 5;REACHABLE*/

    /*!QUERY: c > 4;UNREACHABLE*/
    /*!QUERY: c = 4;REACHABLE*/
    /*!QUERY: c <= 4;REACHABLE*/


    modifier modFunc3{
        require(a < 7, "a should not be greater than 6");
        a = a + 1;
        _;
    }

    modifier modFunc2{
        if (b < 5) {
            b = b + 1;
            _;
        }
    }

    modifier modFunc1{
        if (c < 4) {
            _;
        }
    }

    // a function having 3 modifiers
    function mfTest1() public modFunc3 modFunc2 modFunc1 {
        c = c + 1;
    }

}