pragma solidity >=0.4.22 <0.6.0;

contract RequireStatement2 {
    uint a = 0;
    uint b = 0;
    uint c = 0;
    uint d = 0;
    enum State {Created, Locked, Inactive, Error, ErrorReset}
    State public state;

    /*!QUERY: a >= 7;UNREACHABLE*/
    /*!QUERY: b >= 1;UNREACHABLE*/
    /*!QUERY: c >= 6;UNREACHABLE*/
    /*!QUERY: d > 25;UNREACHABLE*/

    /*!QUERY: a < 7;REACHABLE*/
    /*!QUERY: a = 6;REACHABLE*/
    /*!QUERY: b < 1;REACHABLE*/
    /*!QUERY: c < 6;REACHABLE*/
    /*!QUERY: d = 25;REACHABLE*/

    /*!QUERY: state = State.Created;REACHABLE*/
    /*!QUERY: state = State.Inactive;REACHABLE*/
    /*!QUERY: state = State.Locked;REACHABLE*/
    /*!QUERY: state = State.Error;UNREACHABLE*/
    /*!QUERY: state = State.ErrorReset;UNREACHABLE*/

    constructor() public {
    }

    function rsTest() public {
        if(a > 5){
            a=a+1;
        }
        else{
            a=a+1;
        }
        require(a < 7, "a message");
    }

    function increment_b() public {
        b = b + 1;
        require(b < 1, "This should never be true");
        /*!QUERY: TRUE; UNREACHABLE */
    }
    function increment_c() public {
        c = c + 1;
        require(c < 6, "a message");
        d = d + 5;
    }

    function nextState() public {
        if (state == State.Created) {
            state = State.Locked;
        } else if (state == State.Inactive) {
            state = State.Created;
        }

        if(state == State.ErrorReset) {
            state = State.Error;
        }
    }

    function unlockState() public {
        require(state == State.Locked, "a message");
        state = State.Inactive;
    }

    function resetError() public {
        require(state == State.Error, "a message");
        state = State.ErrorReset;
    }

}