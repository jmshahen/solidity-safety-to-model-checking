pragma solidity >=0.4.22 <0.6.0;

contract RequireStatement3 {
    uint a = 0;
    uint b = 0;

    /*!QUERY: a < 7;REACHABLE*/
    /*!QUERY: b = 20;REACHABLE*/

    /*!QUERY: a = 10;UNREACHABLE*/
    /*!QUERY: b = 30;UNREACHABLE*/


    constructor() public {
    }

    function rsTest() public {
        if (a > 5) {
            a = a + 1;
            require(a < 10, "a message");
        }
        else {
            a = a + 1;
        }
    }

    function increment_b() public {
        int c = 10;
        if(a <= 5){
            b = 20;
            require(c == 10, "Always succeeds");
        }
        else{
            b = 30;
            require(c != 10, "Always fails");
        }
    }
}
