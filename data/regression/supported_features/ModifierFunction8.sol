pragma solidity ^0.4.0;

contract ModifierFunction8 {
    uint lock = 0;
    uint callCount = 0;
    uint maxCount = 5;
    uint b = 0;

    /*!QUERY: lock > 0; UNREACHABLE*/
    /*!QUERY: callCount > 0; REACHABLE*/
    /*!QUERY: callCount > maxCount; UNREACHABLE*/
    /*!QUERY: b > 0; UNREACHABLE*/


    modifier modFunc1{
        require(lock > 0, "lock should be greater than 0");
        _;
    }

    modifier modFunc2{
        require(callCount < maxCount,"");
        lock = lock + 1;
        _;
        lock = lock - 1;
    }


    // a function having 2 modifiers where the ordering of them determines 
    // if a function can be called or not
    // This function can NEVER be called
    function mfTest1() public modFunc1 modFunc2 {
        b = b + 1;
        /*!QUERY: TRUE ; UNREACHABLE*/
    }

    // a function having 2 modifiers where the ordering of them determines 
    // if a function can be called or not
    // This function can be called at most maxCount Times
    function mfTest2() public modFunc2 modFunc1 {
        callCount = callCount + 1;
        /*!QUERY: TRUE ; REACHABLE*/
    }
}
