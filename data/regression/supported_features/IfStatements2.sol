pragma solidity >=0.4.0 <0.7.0;

contract IfStatements2 {
    uint result;
    /*!QUERY: result = 0;REACHABLE*/
    /*!QUERY: result = 1;REACHABLE*/
    /*!QUERY: result = 2;REACHABLE*/
    /*!QUERY: result = 3;REACHABLE*/
    /*!QUERY: result = 4;REACHABLE*/
    /*!QUERY: result = 5;REACHABLE*/
    /*!QUERY: result = 6;REACHABLE*/
    /*!QUERY: result = 7;REACHABLE*/
    /*!QUERY: result = 8;REACHABLE*/
    /*!QUERY: result = 9;REACHABLE*/
    /*!QUERY: result = 10;REACHABLE*/
    /*!QUERY: result = 11;REACHABLE*/
    /*!QUERY: result = 12;UNREACHABLE*/

    function ifelse() public {
        if( result == 0) {   // if else statement
            result = 1;
        } else {
            result = 2;
        }
    }
    function ifalone() public {
        if( result == 2) {   // if else statement
            result = 3;
        }
    }
    function ifelseifelse() public {
        if(result == 3) {   // if else statement
            result = 4;
        } else if(result == 4){
            result = 5;
        } else {
            result = 6;
        }
    }
    function ifelseif() public {
        if(result == 6) {   // if else statement
            result = 7;
        } else if(result == 7){
            result = 8;
        }
    }
    function ifelsenested() public {
        if(result < 10) {   // if else statement
            if(result == 8) {   // if else statement
                result = 9;
            } else {
                result = 10;
            }
        } else {
            result = 11;
        }
    }

    function ifnotreachable() public {
        if(result == 12) {
            result = 12;
        }
    }
}