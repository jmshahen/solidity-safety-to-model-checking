pragma solidity >=0.4.0 <0.7.0;

contract SimpleStateMachine2 {
    uint state_a;
    int state_b = -12;
    // address owner;

    constructor() public {
        // owner = msg.sender;
    }

    function noBranches() public pure returns (uint){
        uint a; // default value is 0
        uint b = 3+a;
        a = 3;
        uint c = a+b;
        /*!QUERY: TRUE ; REACHABLE*/
        return c;
    }

    function noBranchesWithInput(uint a) public pure returns (uint){
        uint b = 3+a;
        uint c = a+b;
        /*!QUERY: TRUE ; REACHABLE*/
        return c;
    }

    function singleRequire() public pure returns (uint){
        uint a; // default value is 0
        uint b = 3+a; // 3=3+0
        a = 3;
        uint c = a+b; // 6=3+3

        require(c > 5, "This will never fail");
        /*!QUERY: TRUE ; REACHABLE*/

        return c;
    }
    function singleRequireWithInput(uint a) public pure returns (uint){
        uint b = 3+a;
        uint c = a+b; // c = 3 + 2*a

        require(c > 5, "This can fail, but won't always fail");

        /*!QUERY: TRUE ; REACHABLE*/

        return c;
    }
}