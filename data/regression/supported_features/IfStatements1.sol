pragma solidity >=0.4.0 <0.7.0;

contract IfStatements1 {
    uint result;
    /*!QUERY: result = 0;REACHABLE*/
    /*!QUERY: result = 1;REACHABLE*/
    /*!QUERY: result = 2;REACHABLE*/
    /*!QUERY: result = 3;UNREACHABLE*/
    /*!QUERY: result = 4;UNREACHABLE*/
    /*!QUERY: result = 5;UNREACHABLE*/
    /*!QUERY: result = 6;UNREACHABLE*/
    /*!QUERY: result = 7;UNREACHABLE*/
    /*!QUERY: result = 8;UNREACHABLE*/
    /*!QUERY: result = 9;UNREACHABLE*/
    /*!QUERY: result = 10;UNREACHABLE*/
    /*!QUERY: result = 11;UNREACHABLE*/

    function ifelse() public {
        if( result == 0) {   // if else statement
            result = 10;
            result = 1;
        } else {
            result = 2;
        }
    }
}