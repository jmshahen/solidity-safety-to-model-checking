pragma solidity >=0.4.0 <0.7.0;

contract SimpleStateVariables {
    /*!QUERY: ba = 20; REACHABLE*/
    /*!QUERY: baa = 21; REACHABLE*/
    /*!QUERY: baaa = 22; REACHABLE*/
    /*!QUERY: a = 10; REACHABLE*/
    /*!QUERY: aa = 11; REACHABLE*/
    /*!QUERY: aaa = 12; REACHABLE*/


    /*!QUERY: ba != 20; UNREACHABLE*/
    /*!QUERY: baa != 21; UNREACHABLE*/
    /*!QUERY: baaa != 22; UNREACHABLE*/
    /*!QUERY: a != 10; UNREACHABLE*/
    /*!QUERY: aa != 11; UNREACHABLE*/
    /*!QUERY: aaa != 12; UNREACHABLE*/
    
    uint a = 10;
    uint aa = 11;
    uint aaa = 12;
    uint b;
    uint ba = 20;
    uint baa = 21;
    uint baaa = 22;
    int c  = -100;
}