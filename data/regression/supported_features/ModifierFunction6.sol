pragma solidity ^0.4.0;

contract ModifierFunction6 {
    uint a = 0;
    uint b = 0;

    /*!QUERY: a > 7;UNREACHABLE*/
    /*!QUERY: a <= 7;REACHABLE*/
    /*!QUERY: a = 7;UNREACHABLE*/

    /*!QUERY: b > 5;UNREACHABLE*/
    /*!QUERY: b = 5;REACHABLE*/
    /*!QUERY: b <= 5;REACHABLE*/


    modifier modFunc1{
        require(a < 7, "a should not be greater than 6");
        a = a + 1;
        _;
    }

    modifier modFunc2{
        require(b < 5, "b should not be greater than 4");
        _;
    }

    // a function having 2 modifiers
    function mfTest1() public modFunc2 modFunc1 {
        b = b + 1;
    }
}
