pragma solidity >=0.4.22 <0.6.0;

contract EnumDataType {
    uint a;
    enum State {Created, Locked, Inactive, Error}
    enum OtherEnum {s, ss, Hello, Bob, sss}
    OtherEnum public other;
    /*!QUERY: other = OtherEnum.Hello;REACHABLE*/
    /*!QUERY: other = OtherEnum.Bob;UNREACHABLE*/
    State public state;
    /*!QUERY: state = State.Inactive;REACHABLE*/
    /*!QUERY: state = State.Error;UNREACHABLE*/
    /*!QUERY: a > 10;UNREACHABLE*/

    constructor() public {
        // state = State.Created; // NOT REQUIRED DUE TO DEFAULT VALUE
        a = 0;
    }

    function nextState() public {
        if (state == State.Created) {
            state = State.Locked;
        } else if (state == State.Inactive) {
            state = State.Created;
        }
    }

    function unlockState(OtherEnum _oe) public {
        other = OtherEnum.Hello;
        if (state == State.Locked) {
            state = State.Inactive;
            /*!QUERY: TRUE;REACHABLE*/
        }
    }
}
