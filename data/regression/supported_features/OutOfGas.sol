pragma solidity >=0.4.0 <0.7.0;
/*!AttackGasLimit: 1;*/

contract OutOfGas {
    constructor() public {
    }

    function func1() public {
        // Empty function
    }
    function func2() private view returns (uint) {
        /*!QUERY: TRUE ; UNREACHABLE*/
        return 1;
    }
    function func3() public payable {
    }
    function func4(uint b) public view returns(bool){
        /*!QUERY: TRUE ; UNREACHABLE*/
        return b == 12;
    }

    function() external payable {
        // this is a sink; it will take value if someone accidentally sends money to this contract
        /*!QUERY: TRUE ; UNREACHABLE*/
    }
}