pragma solidity >=0.4.0 <0.7.0;

contract SimpleStateMachine1 {
    enum State {Created, Locked, Inactive, Error}
    function noBranches() external pure returns (uint){
        State state;
        uint a = 3;
        // default value is 0
        uint b = 3 + a;
        uint c = a + b;
        return c;
    }

    function noBranchesWithInput(uint a1) external pure returns (uint){
        uint b1;
        b1 = 3+a1;
        uint c1;
        c1 = a1+b1;
        return c1;
    }

    function singleRequire() external pure returns (uint){
        uint a2; // default value is 0
        uint b2;
        b2 = 3+a2; // 3=3+0
        a2 = 3;
        uint c2;
        c2 = a2+b2; // 6=3+3

        /*!QUERY: b2 = 3;REACHABLE*/

        require(c2 > 5, "This will never fail");

        return c2;
    }

    function singleRequireWithInput(uint a3) external pure returns (uint){
        uint b3;
        b3 = 2*a3;
        uint c3 = a3+b3; // c = a + 2*a

        require(c3 > 5, "This can fail");
        /*!QUERY: c3 > 5; REACHABLE */
        /*!QUERY: c3 = 5; UNREACHABLE */

        return c3;
    }

}
