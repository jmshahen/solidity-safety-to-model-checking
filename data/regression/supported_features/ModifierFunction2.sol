pragma solidity >=0.4.22 <0.6.0;

contract ModifierFunction2{
    enum State {Created, Locked, Inactive, Error, ErrorReset}
    State public state;

    /*!QUERY: state = State.Created;REACHABLE*/
    /*!QUERY: state = State.Inactive;REACHABLE*/
    /*!QUERY: state = State.Locked;REACHABLE*/
    /*!QUERY: state = State.Error;UNREACHABLE*/
    /*!QUERY: state = State.ErrorReset;UNREACHABLE*/

    constructor() public {
    }

    function nextState() public {
        if (state == State.Created) {
            state = State.Locked;
        } else if (state == State.Inactive) {
            state = State.Created;
        }

        if(state == State.ErrorReset) {
            state = State.Error;
        }
    }

    modifier lockStateModifier(){
        require(state == State.Locked, "Can only call when the state is Locked");
        _;
    }

    modifier ErrorStateModifier() {
        require(state == State.Error, "Can only call when the state is Error");
        _;
    }

    function unlockState() public lockStateModifier{
        state = State.Inactive;
    }

    function resetError() public ErrorStateModifier{
        state = State.ErrorReset;
    }

}