pragma solidity ^0.4.0;

contract ModifierFunction4 {
    int value;
    bool registeredAddress;

    /*!QUERY: registeredAddress = TRUE ;REACHABLE*/
    /*!QUERY: value = 100 ;REACHABLE*/
    /*!QUERY: value = 101 ;UNREACHABLE*/

    modifier costs(int price){
        int a;
        if (value < price) {
            _;
        }
        a = price * 2;
        value = a;
    }

    function register() public costs(price) {
        if (value > 5) {
            registeredAddress = true;
        }
        value = value + 1;
    }

}
