pragma solidity ^0.4.0;

contract ModifierFunction5 {
    uint a = 0;

    /*!QUERY: a > 0;UNREACHABLE*/
    /*!QUERY: a = 0;REACHABLE*/


    modifier modFunc1{
        require(a < 7, "a should not be greater than 6");
        _;
    }

    modifier modFunc2{
        require(a >= 7, "a should be greater than 6");
        _;
    }

    // a function having 2 modifiers where they have opposite conditions and 
    // thus if a function has them it can never be called
    function mfTest1() public modFunc1 modFunc2 {
        a = a + 1;
    }


}
