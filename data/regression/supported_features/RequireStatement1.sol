pragma solidity >=0.4.22 <0.6.0;

contract RequireStatement1 {
    uint a = 0;
    uint b = 0;
    uint c = 0;
    uint d = 0;
    uint e = 0;

    /*!QUERY: a = 7;REACHABLE*/
    /*!QUERY: b = 1;REACHABLE*/
    /*!QUERY: c = 6;REACHABLE*/
    /*!QUERY: d = 0;REACHABLE*/
    /*!QUERY: d = 3;REACHABLE*/
    /*!QUERY: d = 50;REACHABLE*/

    /*!QUERY: a >= 8;UNREACHABLE*/
    /*!QUERY: b >= 2;UNREACHABLE*/
    /*!QUERY: c >= 7;UNREACHABLE*/
    /*!QUERY: d = 1;UNREACHABLE*/
    /*!QUERY: d = 2;UNREACHABLE*/
    /*!QUERY: d = 5;UNREACHABLE*/
    /*!QUERY: d = 51;UNREACHABLE*/
    /*!QUERY: d = 53;UNREACHABLE*/


    constructor() public {
    }

    function rsTest() public {
        require(a < 7, "a message");
        if(a > 5){
            a=a+1;
        }
        else{
            a=a+1;
        }
    }

    function increment_b() public {
        require(b < 1, "a message");
        b = b + 1;
    }
    function increment_c() public {
        require(c < 6, "a message");
        c = c + 1;
    }

    function mulRequire1() public {
        require(a == 1, "a message");
        require(b == 1, "a message");
        require(c == 1, "a message");
        require(d == 0, "a message");
        d = 50;
    }

    function mulRequire2() public {
        require(a < 7, "a message");
        d = d + 1;
        require(c < 6, "a message");
        d = d + 1;
        require(c < 6, "a message");
        d = d + 1;
        require(d < 7, "a message");
    }
}
