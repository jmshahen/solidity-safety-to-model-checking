// Comes from reachable/reachable02.sol but forces the UNIT MAX and MIN values to be incorrect
/*!UINT_MAX: 0;*/
/*!UINT_MIN: 10;*/
pragma solidity >=0.4.0 <0.7.0;

contract SimpleFunction1 {
    uint a;
    uint b = 5;
    uint c;
    /*!QUERY: c = 18;UNREACHABLE*/
    /*!QUERY: c = 17;UNREACHABLE*/

    constructor() public {
        a = 3;
    }

    function func1() public {
        c = a + b;
        b = b - 1;
        a = 2;
        /*!QUERY: c < a & c < b; REACHABLE */
        // The Above should be UNREACHABLE, but due to the rollover nature of int/uint this is an error
    }
    function func2() public {
        b = b + a;
    }
    function func3() public {
        a = 10;
    }
}