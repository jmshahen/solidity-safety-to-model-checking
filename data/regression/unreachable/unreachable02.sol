pragma solidity >=0.4.0 <0.7.0;

contract SimpleStorage {
    uint storedData;
    uint sensitiveInformation;

    constructor(uint info) public {
        sensitiveInformation = info;
    }

    function set(uint x) public {
        storedData = x;
    }

    function get() public view returns (uint) {
        return storedData;
    }
}