pragma solidity >=0.4.0 <0.7.0;

contract SimpleStorage {
    uint storedData = 15;
    /*!QUERY: storedData = 13;*/
    /*!QUERY: storedData = -1;*/

    function set(uint x) public {
        uint b = x * 2;
        storedData = b;
    }

    function get() public view returns (uint) {
        return storedData;
    }
}