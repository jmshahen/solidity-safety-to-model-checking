pragma solidity >=0.4.0 <0.7.0;

contract SameLocalParameterNames {
    /*!QUERY: a = 10; REACHABLE*/
    /*!QUERY: aa = 11; REACHABLE*/
    /*!QUERY: aaa = 12; REACHABLE*/
    /*!QUERY: a = 20; REACHABLE*/
    /*!QUERY: aa = 31; REACHABLE*/
    /*!QUERY: aaa = 42; REACHABLE*/

    /*!QUERY: a = 11; UNREACHABLE*/
    /*!QUERY: a = 12; UNREACHABLE*/
    /*!QUERY: aa = 10; UNREACHABLE*/
    /*!QUERY: aa = 12; UNREACHABLE*/
    /*!QUERY: aaa = 10; UNREACHABLE*/
    /*!QUERY: aaa = 11; UNREACHABLE*/

    uint a = 10;
    uint aa = 11;
    uint aaa = 12;
    uint total=0;

    constructor() public {
        uint a1 = 10;
        total = a1 - 10;
    }

    function func1() public {
        // Empty function
    }
    function func2a(uint p) private view returns (uint) {
        uint a1 = 1;
        uint b2 = 2;

        a = a + 10;

        return total + a + a1 + b2;
    }
    function func2aa(uint p) private view returns (uint) {
        uint a1 = 1;
        uint b2 = 2;

        aa = aa + 20;

        return total + aa + a1 + b2;
    }
    function func2aaa(uint p) private view returns (uint) {
        uint a1 = 1;
        uint b2 = 2;

        aaa = aaa + 30;

        return total + aaa + a1 + b2;
    }
    function func3(uint p) public payable {
        total += 1;
    }
    function func4(uint b) public view returns(bool){
        return b == total;
    }

    function() external payable {
        // this is a sink; it will take value if someone accidentally sends money to this contract
    }
}