pragma solidity >=0.4.0 <0.7.0;

contract SimpleStateVariables2 {
    /*!QUERY: a = 10; REACHABLE*/
    /*!QUERY: aa = 10; REACHABLE*/
    /*!QUERY: aaa = 10; REACHABLE*/

    /*!QUERY: a = 11; UNREACHABLE*/
    /*!QUERY: a = 12; UNREACHABLE*/
    /*!QUERY: aa = 10; UNREACHABLE*/
    /*!QUERY: aa = 12; UNREACHABLE*/
    /*!QUERY: aaa = 10; UNREACHABLE*/
    /*!QUERY: aaa = 11; UNREACHABLE*/
    
    uint a = 10;
    uint aa = 11;
    uint aaa = 12;
    uint b;
    int c  = -100;
    address addr_a;
    address addr_b = address(0x123);
    address addr_bb = address(0x124);
    address bb_addr = address(0x125);
    address bb_rdda = address(0x126);
}