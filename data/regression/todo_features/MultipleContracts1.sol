pragma solidity >=0.4.0 <0.7.0;

contract MultipleContracts1 {
    uint total=0;
    OtherContract b;
    address owner;

    constructor() public {
        owner = msg.sender;
    }

    function func1() public {
        // Empty function
    }
    function func2() private view returns (uint) {
        uint a = 1;
        uint b = 2;
        return total + a + b;
    }
    function func3() public payable {
        total += msg.value;
    }
    function func4(uint b) public view returns(bool){
        return b == total;
    }

    function() external payable {
        // this is a sink; it will take value if someone accidentally sends money to this contract
    }
}

contract OtherContract {
    bool a;
}