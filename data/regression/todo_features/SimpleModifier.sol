pragma solidity >=0.4.0 <0.7.0;

contract SimpleFunction1 {
    
    modifier above5(uint a) {
        require(a > 5, "Must be above 5");
        _;
    }

    function func4(uint b) public pure above5(b) returns(bool){
        return b == 12;
    }
}