/*!UINT_MAX: 0;*/
/*!UINT_MIN: 10;*/
pragma solidity >=0.4.0 <0.7.0;

// The definition for the range for UINT is backwards and should cause an error
// This should not be automatically corrected, because this might be indicative of an error.
contract IncorrectUINTDefinition{
    uint a;

    constructor() public {
        a = 3;
    }

    function func1() public {
        a = 2;
    }
}
