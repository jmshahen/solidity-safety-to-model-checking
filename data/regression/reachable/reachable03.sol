pragma solidity >=0.4.0 <0.7.0;

contract SimpleStorage {
    uint storedData;
    uint securedData;
    /*!QUERY: securedData = 1; */
    /*!QUERY: securedData > 1; */
    /*!QUERY: storedData < 20; */

    function set(uint x) public {
        uint temp = 20;
        if(x > temp) {
            storedData = x;
            temp = 2 * storedData;
            if(temp >= 100 & temp <= 200) {
                temp = 1;
                /*!QUERY: TRUE; */
            }
            securedData = temp;
        }
    }

    function get() public view returns (uint) {
        return storedData;
    }
}