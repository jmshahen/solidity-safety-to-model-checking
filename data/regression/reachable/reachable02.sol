pragma solidity >=0.4.0 <0.7.0;
contract SimpleFunction1 {
    uint a;
    uint b = 5; // QUERY: Can be ever equal 18? YES: func2() -> func3() -> func2()
    uint c;
    /*!QUERY: c = 18;REACHABLE*/
    /*!QUERY: c = 17;REACHABLE*/

    constructor() public {
        a = 3;
    }

    function func1() public {
        c = a + b;
        b = b - 1;
        a = 2;
        /*!QUERY: c < a & c < b; UNREACHABLE */
    }
    function func2() public {
        b = b + a;
    }
    function func3() public {
        a = 10;
    }
}