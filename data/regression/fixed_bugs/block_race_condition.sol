pragma solidity >=0.4.22 <0.6.0;

contract BlockRaceConidition {
    uint b = 0;
    uint c = 0;
    uint b2 = 0;
    uint c2 = 0;

    uint temp = 0;

    /*!QUERY: b >= 1;UNREACHABLE*/
    /*!QUERY: c >= 1;UNREACHABLE*/
    /*!QUERY: b2 >= 1;UNREACHABLE*/
    /*!QUERY: c2 >= 1;UNREACHABLE*/

    function increment_b() public {
        b = b + 1;
        require(b < 1, "a message");
    }

    function increment_c() public {
        c = c + 1;
        if(c < 1) {
        }else {
            c = 0;
        }
    }
    function increment_b2() public {
        b2 = b2 + 1;
        temp = temp + 1;
        require(b2 < 1, "a message");
    }

    function increment_c2() public {
        c2 = c2 + 1;
        temp = temp + 1;
        if(c2 < 1) {
        }else {
            c2 = 0;
        }
    }
}